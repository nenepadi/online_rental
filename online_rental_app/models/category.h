#ifndef CATEGORY_H
#define CATEGORY_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class CategoryObject;
class QJsonArray;


class T_MODEL_EXPORT Category : public TAbstractModel
{
public:
    Category();
    Category(const Category &other);
    Category(const CategoryObject &object);
    ~Category();

    int catId() const;
    QString categoryName() const;
    void setCategoryName(const QString &categoryName);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    Category &operator=(const Category &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static Category create(const QString &categoryName);
    static Category create(const QVariantMap &values);
    static Category get(int catId);
    static Category get(int catId, int lockRevision);
    static int count();
    static QList<Category> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<CategoryObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(Category)
Q_DECLARE_METATYPE(QList<Category>)

#endif // CATEGORY_H
