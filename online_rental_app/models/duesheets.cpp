#include <TreeFrogModel>
#include "duesheets.h"
#include "duesheetsobject.h"

DueSheets::DueSheets()
    : TAbstractModel(), d(new DueSheetsObject)
{
    d->id = 0;
    d->lock_revision = 0;
}

DueSheets::DueSheets(const DueSheets &other)
    : TAbstractModel(), d(new DueSheetsObject(*other.d))
{ }

DueSheets::DueSheets(const DueSheetsObject &object)
    : TAbstractModel(), d(new DueSheetsObject(object))
{ }

DueSheets::~DueSheets()
{
    // If the reference count becomes 0,
    // the shared data object 'DueSheetsObject' is deleted.
}

int DueSheets::id() const
{
    return d->id;
}

QString DueSheets::orderedItem() const
{
    return d->ordered_item;
}

void DueSheets::setOrderedItem(const QString &orderedItem)
{
    d->ordered_item = orderedItem;
}

QString DueSheets::orderedBy() const
{
    return d->ordered_by;
}

void DueSheets::setOrderedBy(const QString &orderedBy)
{
    d->ordered_by = orderedBy;
}

QString DueSheets::processedDate() const
{
    return d->processed_date;
}

void DueSheets::setProcessedDate(const QString &processedDate)
{
    d->processed_date = processedDate;
}

QString DueSheets::dueDate() const
{
    return d->due_date;
}

void DueSheets::setDueDate(const QString &dueDate)
{
    d->due_date = dueDate;
}

bool DueSheets::checkIn() const
{
    return d->check_in;
}

void DueSheets::setCheckIn(const bool &checkIn)
{
    d->check_in = checkIn;
}

QString DueSheets::processor() const
{
    return d->processor;
}

void DueSheets::setProcessor(const QString &processor)
{
    d->processor = processor;
}

QString DueSheets::dayFineLastActivate() const
{
    return d->day_fine_last_activate;
}

void DueSheets::setDayFineLastActivate(const QString &dayFineLastActivate)
{
    d->day_fine_last_activate = dayFineLastActivate;
}

QString DueSheets::createdAt() const
{
    return d->created_at;
}

QString DueSheets::updatedAt() const
{
    return d->updated_at;
}

int DueSheets::lockRevision() const
{
    return d->lock_revision;
}

DueSheets &DueSheets::operator=(const DueSheets &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

DueSheets DueSheets::create(const QString &orderedItem, const QString &orderedBy, const QString &processedDate, const QString &dueDate, const bool &checkIn, const QString &processor, const QString &dayFineLastActivate)
{
    DueSheetsObject obj;
    obj.ordered_item = orderedItem;
    obj.ordered_by = orderedBy;
    obj.processed_date = processedDate;
    obj.due_date = dueDate;
    obj.check_in = checkIn;
    obj.processor = processor;
    obj.day_fine_last_activate = dayFineLastActivate;
    if (!obj.create()) {
        return DueSheets();
    }
    return DueSheets(obj);
}

DueSheets DueSheets::create(const QVariantMap &values)
{
    DueSheets model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

DueSheets DueSheets::get(int id)
{
    TSqlORMapper<DueSheetsObject> mapper;
    return DueSheets(mapper.findByPrimaryKey(id));
}

DueSheets DueSheets::get(int id, int lockRevision)
{
    TSqlORMapper<DueSheetsObject> mapper;
    TCriteria cri;
    cri.add(DueSheetsObject::Id, id);
    cri.add(DueSheetsObject::LockRevision, lockRevision);
    return DueSheets(mapper.findFirst(cri));
}

int DueSheets::count()
{
    TSqlORMapper<DueSheetsObject> mapper;
    return mapper.findCount();
}

QList<DueSheets> DueSheets::getAll()
{
    return tfGetModelListByCriteria<DueSheets, DueSheetsObject>(TCriteria());
}

QJsonArray DueSheets::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<DueSheetsObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<DueSheetsObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(DueSheets(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *DueSheets::modelData()
{
    return d.data();
}

const TModelObject *DueSheets::modelData() const
{
    return d.data();
}
