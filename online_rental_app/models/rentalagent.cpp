#include <TreeFrogModel>
#include "rentalagent.h"
#include "rentalagentobject.h"

RentalAgent::RentalAgent()
    : TAbstractUser(), TAbstractModel(), d(new RentalAgentObject)
{
    d->agentid = 0;
    d->lock_revision = 0;
}

RentalAgent::RentalAgent(const RentalAgent &other)
    : TAbstractUser(), TAbstractModel(), d(new RentalAgentObject(*other.d))
{ }

RentalAgent::RentalAgent(const RentalAgentObject &object)
    : TAbstractUser(), TAbstractModel(), d(new RentalAgentObject(object))
{ }


RentalAgent::~RentalAgent()
{
    // If the reference count becomes 0,
    // the shared data object 'RentalAgentObject' is deleted.
}

int RentalAgent::agentid() const
{
    return d->agentid;
}

QString RentalAgent::firstname() const
{
    return d->firstname;
}

void RentalAgent::setFirstname(const QString &firstname)
{
    d->firstname = firstname;
}

QString RentalAgent::lastname() const
{
    return d->lastname;
}

void RentalAgent::setLastname(const QString &lastname)
{
    d->lastname = lastname;
}

QString RentalAgent::email() const
{
    return d->email;
}

void RentalAgent::setEmail(const QString &email)
{
    d->email = email;
}

QString RentalAgent::password() const
{
    return d->password;
}

void RentalAgent::setPassword(const QString &password)
{
    d->password = password;
}

QString RentalAgent::location() const
{
    return d->location;
}

void RentalAgent::setLocation(const QString &location)
{
    d->location = location;
}

QString RentalAgent::role() const
{
    return d->role;
}

void RentalAgent::setRole(const QString &role)
{
    d->role = role;
}

QString RentalAgent::createdAt() const
{
    return d->created_at;
}

QString RentalAgent::updatedAt() const
{
    return d->updated_at;
}

int RentalAgent::lockRevision() const
{
    return d->lock_revision;
}

RentalAgent &RentalAgent::operator=(const RentalAgent &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

RentalAgent RentalAgent::authenticate(const QString &email, const QString &password)
{
    if (email.isEmpty() || password.isEmpty())
        return RentalAgent();

    TSqlORMapper<RentalAgentObject> mapper;
    RentalAgentObject obj = mapper.findFirst(TCriteria(RentalAgentObject::Email, email));
    if (obj.isNull() || obj.password != password) {
        obj.clear();
    }
    return RentalAgent(obj);
}

RentalAgent RentalAgent::create(const QString &firstname, const QString &lastname, const QString &email, const QString &password, const QString &location, const QString &role)
{
    RentalAgentObject obj;
    obj.firstname = firstname;
    obj.lastname = lastname;
    obj.email = email;
    obj.password = password;
    obj.location = location;
    obj.role = role;
    if (!obj.create()) {
        return RentalAgent();
    }
    return RentalAgent(obj);
}

RentalAgent RentalAgent::create(const QVariantMap &values)
{
    RentalAgent model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

RentalAgent RentalAgent::get(int agentid)
{
    TSqlORMapper<RentalAgentObject> mapper;
    return RentalAgent(mapper.findByPrimaryKey(agentid));
}

RentalAgent RentalAgent::get(int agentid, int lockRevision)
{
    TSqlORMapper<RentalAgentObject> mapper;
    TCriteria cri;
    cri.add(RentalAgentObject::Agentid, agentid);
    cri.add(RentalAgentObject::LockRevision, lockRevision);
    return RentalAgent(mapper.findFirst(cri));
}

int RentalAgent::count()
{
    TSqlORMapper<RentalAgentObject> mapper;
    return mapper.findCount();
}

QList<RentalAgent> RentalAgent::getAll()
{
    return tfGetModelListByCriteria<RentalAgent, RentalAgentObject>();
}

QJsonArray RentalAgent::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<RentalAgentObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<RentalAgentObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(RentalAgent(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *RentalAgent::modelData()
{
    return d.data();
}

const TModelObject *RentalAgent::modelData() const
{
    return d.data();
}
