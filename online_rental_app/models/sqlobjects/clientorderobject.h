#ifndef CLIENTORDEROBJECT_H
#define CLIENTORDEROBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT ClientOrderObject : public TSqlObject, public QSharedData
{
public:
    int id;
    QString movie;
    QString user;
    QString location;
    bool fulfilled;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        Id = 0,
        Movie,
        User,
        Location,
        Fulfilled,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return Id; }
    int autoValueIndex() const { return Id; }
    QString tableName() const { return QLatin1String("client_order"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int id READ getid WRITE setid)
    T_DEFINE_PROPERTY(int, id)
    Q_PROPERTY(QString movie READ getmovie WRITE setmovie)
    T_DEFINE_PROPERTY(QString, movie)
    Q_PROPERTY(QString user READ getuser WRITE setuser)
    T_DEFINE_PROPERTY(QString, user)
    Q_PROPERTY(QString location READ getlocation WRITE setlocation)
    T_DEFINE_PROPERTY(QString, location)
    Q_PROPERTY(bool fulfilled READ getfulfilled WRITE setfulfilled)
    T_DEFINE_PROPERTY(bool, fulfilled)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // CLIENTORDEROBJECT_H
