#ifndef DUESHEETSOBJECT_H
#define DUESHEETSOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT DueSheetsObject : public TSqlObject, public QSharedData
{
public:
    int id;
    QString ordered_item;
    QString ordered_by;
    QString processed_date;
    QString due_date;
    bool check_in;
    QString processor;
    QString day_fine_last_activate;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        Id = 0,
        OrderedItem,
        OrderedBy,
        ProcessedDate,
        DueDate,
        CheckIn,
        Processor,
        DayFineLastActivate,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return Id; }
    int autoValueIndex() const { return Id; }
    QString tableName() const { return QLatin1String("due_sheets"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int id READ getid WRITE setid)
    T_DEFINE_PROPERTY(int, id)
    Q_PROPERTY(QString ordered_item READ getordered_item WRITE setordered_item)
    T_DEFINE_PROPERTY(QString, ordered_item)
    Q_PROPERTY(QString ordered_by READ getordered_by WRITE setordered_by)
    T_DEFINE_PROPERTY(QString, ordered_by)
    Q_PROPERTY(QString processed_date READ getprocessed_date WRITE setprocessed_date)
    T_DEFINE_PROPERTY(QString, processed_date)
    Q_PROPERTY(QString due_date READ getdue_date WRITE setdue_date)
    T_DEFINE_PROPERTY(QString, due_date)
    Q_PROPERTY(bool check_in READ getcheck_in WRITE setcheck_in)
    T_DEFINE_PROPERTY(bool, check_in)
    Q_PROPERTY(QString processor READ getprocessor WRITE setprocessor)
    T_DEFINE_PROPERTY(QString, processor)
    Q_PROPERTY(QString day_fine_last_activate READ getday_fine_last_activate WRITE setday_fine_last_activate)
    T_DEFINE_PROPERTY(QString, day_fine_last_activate)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // DUESHEETSOBJECT_H
