#ifndef RENTALAGENTOBJECT_H
#define RENTALAGENTOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT RentalAgentObject : public TSqlObject, public QSharedData
{
public:
    int agentid;
    QString firstname;
    QString lastname;
    QString email;
    QString password;
    QString location;
    QString role;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        Agentid = 0,
        Firstname,
        Lastname,
        Email,
        Password,
        Location,
        Role,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return Agentid; }
    int autoValueIndex() const { return Agentid; }
    QString tableName() const { return QLatin1String("rental_agent"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int agentid READ getagentid WRITE setagentid)
    T_DEFINE_PROPERTY(int, agentid)
    Q_PROPERTY(QString firstname READ getfirstname WRITE setfirstname)
    T_DEFINE_PROPERTY(QString, firstname)
    Q_PROPERTY(QString lastname READ getlastname WRITE setlastname)
    T_DEFINE_PROPERTY(QString, lastname)
    Q_PROPERTY(QString email READ getemail WRITE setemail)
    T_DEFINE_PROPERTY(QString, email)
    Q_PROPERTY(QString password READ getpassword WRITE setpassword)
    T_DEFINE_PROPERTY(QString, password)
    Q_PROPERTY(QString location READ getlocation WRITE setlocation)
    T_DEFINE_PROPERTY(QString, location)
    Q_PROPERTY(QString role READ getrole WRITE setrole)
    T_DEFINE_PROPERTY(QString, role)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // RENTALAGENTOBJECT_H
