#ifndef CLIENTOBJECT_H
#define CLIENTOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT ClientObject : public TSqlObject, public QSharedData
{
public:
    int clientid;
    QString firstname;
    QString lastname;
    QString username;
    QString email;
    QString password;
    QString pick_location;
    bool is_active;
    int entitled_number_movies;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        Clientid = 0,
        Firstname,
        Lastname,
        Username,
        Email,
        Password,
        PickLocation,
        IsActive,
        EntitledNumberMovies,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return Clientid; }
    int autoValueIndex() const { return Clientid; }
    QString tableName() const { return QLatin1String("client"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int clientid READ getclientid WRITE setclientid)
    T_DEFINE_PROPERTY(int, clientid)
    Q_PROPERTY(QString firstname READ getfirstname WRITE setfirstname)
    T_DEFINE_PROPERTY(QString, firstname)
    Q_PROPERTY(QString lastname READ getlastname WRITE setlastname)
    T_DEFINE_PROPERTY(QString, lastname)
    Q_PROPERTY(QString username READ getusername WRITE setusername)
    T_DEFINE_PROPERTY(QString, username)
    Q_PROPERTY(QString email READ getemail WRITE setemail)
    T_DEFINE_PROPERTY(QString, email)
    Q_PROPERTY(QString password READ getpassword WRITE setpassword)
    T_DEFINE_PROPERTY(QString, password)
    Q_PROPERTY(QString pick_location READ getpick_location WRITE setpick_location)
    T_DEFINE_PROPERTY(QString, pick_location)
    Q_PROPERTY(bool is_active READ getis_active WRITE setis_active)
    T_DEFINE_PROPERTY(bool, is_active)
    Q_PROPERTY(int entitled_number_movies READ getentitled_number_movies WRITE setentitled_number_movies)
    T_DEFINE_PROPERTY(int, entitled_number_movies)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // CLIENTOBJECT_H
