#ifndef ITEMOBJECT_H
#define ITEMOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT ItemObject : public TSqlObject, public QSharedData
{
public:
    int id;
    QString item_title;
    QString category;
    QString year_of_production;
    QString description;
    int quantity;
    QString lead_actor;
    QString lead_actress;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        Id = 0,
        ItemTitle,
        Category,
        YearOfProduction,
        Description,
        Quantity,
        LeadActor,
        LeadActress,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return Id; }
    int autoValueIndex() const { return Id; }
    QString tableName() const { return QLatin1String("item"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int id READ getid WRITE setid)
    T_DEFINE_PROPERTY(int, id)
    Q_PROPERTY(QString item_title READ getitem_title WRITE setitem_title)
    T_DEFINE_PROPERTY(QString, item_title)
    Q_PROPERTY(QString category READ getcategory WRITE setcategory)
    T_DEFINE_PROPERTY(QString, category)
    Q_PROPERTY(QString year_of_production READ getyear_of_production WRITE setyear_of_production)
    T_DEFINE_PROPERTY(QString, year_of_production)
    Q_PROPERTY(QString description READ getdescription WRITE setdescription)
    T_DEFINE_PROPERTY(QString, description)
    Q_PROPERTY(int quantity READ getquantity WRITE setquantity)
    T_DEFINE_PROPERTY(int, quantity)
    Q_PROPERTY(QString lead_actor READ getlead_actor WRITE setlead_actor)
    T_DEFINE_PROPERTY(QString, lead_actor)
    Q_PROPERTY(QString lead_actress READ getlead_actress WRITE setlead_actress)
    T_DEFINE_PROPERTY(QString, lead_actress)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // ITEMOBJECT_H
