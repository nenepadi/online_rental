#ifndef POSSIBLELOCATIONSOBJECT_H
#define POSSIBLELOCATIONSOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT PossibleLocationsObject : public TSqlObject, public QSharedData
{
public:
    int id;
    QString location_name;
    double latitude;
    double longitude;

    enum PropertyIndex {
        Id = 0,
        LocationName,
        Latitude,
        Longitude,
    };

    int primaryKeyIndex() const { return Id; }
    int autoValueIndex() const { return Id; }
    QString tableName() const { return QLatin1String("possible_locations"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int id READ getid WRITE setid)
    T_DEFINE_PROPERTY(int, id)
    Q_PROPERTY(QString location_name READ getlocation_name WRITE setlocation_name)
    T_DEFINE_PROPERTY(QString, location_name)
    Q_PROPERTY(double latitude READ getlatitude WRITE setlatitude)
    T_DEFINE_PROPERTY(double, latitude)
    Q_PROPERTY(double longitude READ getlongitude WRITE setlongitude)
    T_DEFINE_PROPERTY(double, longitude)
};

#endif // POSSIBLELOCATIONSOBJECT_H
