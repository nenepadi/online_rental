#ifndef CATEGORYOBJECT_H
#define CATEGORYOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT CategoryObject : public TSqlObject, public QSharedData
{
public:
    int cat_id;
    QString category_name;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        CatId = 0,
        CategoryName,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return CatId; }
    int autoValueIndex() const { return CatId; }
    QString tableName() const { return QLatin1String("category"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int cat_id READ getcat_id WRITE setcat_id)
    T_DEFINE_PROPERTY(int, cat_id)
    Q_PROPERTY(QString category_name READ getcategory_name WRITE setcategory_name)
    T_DEFINE_PROPERTY(QString, category_name)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // CATEGORYOBJECT_H
