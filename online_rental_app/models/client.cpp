#include <TreeFrogModel>
#include "client.h"
#include "clientobject.h"

Client::Client()
    : TAbstractUser(), TAbstractModel(), d(new ClientObject)
{
    d->clientid = 0;
    d->entitled_number_movies = 0;
    d->lock_revision = 0;
}

Client::Client(const Client &other)
    : TAbstractUser(), TAbstractModel(), d(new ClientObject(*other.d))
{ }

Client::Client(const ClientObject &object)
    : TAbstractUser(), TAbstractModel(), d(new ClientObject(object))
{ }


Client::~Client()
{
    // If the reference count becomes 0,
    // the shared data object 'ClientObject' is deleted.
}

int Client::clientid() const
{
    return d->clientid;
}

QString Client::firstname() const
{
    return d->firstname;
}

void Client::setFirstname(const QString &firstname)
{
    d->firstname = firstname;
}

QString Client::lastname() const
{
    return d->lastname;
}

void Client::setLastname(const QString &lastname)
{
    d->lastname = lastname;
}

QString Client::username() const
{
    return d->username;
}

void Client::setUsername(const QString &username)
{
    d->username = username;
}

QString Client::email() const
{
    return d->email;
}

void Client::setEmail(const QString &email)
{
    d->email = email;
}

QString Client::password() const
{
    return d->password;
}

void Client::setPassword(const QString &password)
{
    d->password = password;
}

QString Client::pickLocation() const
{
    return d->pick_location;
}

void Client::setPickLocation(const QString &pickLocation)
{
    d->pick_location = pickLocation;
}

bool Client::isActive() const
{
    return d->is_active;
}

void Client::setIsActive(const bool &isActive)
{
    d->is_active = isActive;
}

int Client::entitledNumberMovies() const
{
    return d->entitled_number_movies;
}

void Client::setEntitledNumberMovies(int entitledNumberMovies)
{
    d->entitled_number_movies = entitledNumberMovies;
}

QString Client::createdAt() const
{
    return d->created_at;
}

QString Client::updatedAt() const
{
    return d->updated_at;
}

int Client::lockRevision() const
{
    return d->lock_revision;
}

Client &Client::operator=(const Client &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

Client Client::authenticate(const QString &email, const QString &password)
{
    if (email.isEmpty() || password.isEmpty())
        return Client();

    TSqlORMapper<ClientObject> mapper;
    ClientObject obj = mapper.findFirst(TCriteria(ClientObject::Email, email));
    if (obj.isNull() || obj.password != password) {
        obj.clear();
    }
    return Client(obj);
}

Client Client::create(const QString &firstname, const QString &lastname, const QString &username, const QString &email, const QString &password, const QString &pickLocation, const bool &isActive, int entitledNumberMovies)
{
    ClientObject obj;
    obj.firstname = firstname;
    obj.lastname = lastname;
    obj.username = username;
    obj.email = email;
    obj.password = password;
    obj.pick_location = pickLocation;
    obj.is_active = isActive;
    obj.entitled_number_movies = entitledNumberMovies;
    if (!obj.create()) {
        return Client();
    }
    return Client(obj);
}

Client Client::create(const QVariantMap &values)
{
    Client model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

Client Client::get(int clientid)
{
    TSqlORMapper<ClientObject> mapper;
    return Client(mapper.findByPrimaryKey(clientid));
}

Client Client::get(int clientid, int lockRevision)
{
    TSqlORMapper<ClientObject> mapper;
    TCriteria cri;
    cri.add(ClientObject::Clientid, clientid);
    cri.add(ClientObject::LockRevision, lockRevision);
    return Client(mapper.findFirst(cri));
}

int Client::count()
{
    TSqlORMapper<ClientObject> mapper;
    return mapper.findCount();
}

QList<Client> Client::getAll()
{
    return tfGetModelListByCriteria<Client, ClientObject>();
}

QJsonArray Client::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<ClientObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<ClientObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(Client(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *Client::modelData()
{
    return d.data();
}

const TModelObject *Client::modelData() const
{
    return d.data();
}
