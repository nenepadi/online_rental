#include <TreeFrogModel>
#include "item.h"
#include "itemobject.h"

Item::Item()
    : TAbstractModel(), d(new ItemObject)
{
    d->id = 0;
    d->quantity = 0;
    d->lock_revision = 0;
}

Item::Item(const Item &other)
    : TAbstractModel(), d(new ItemObject(*other.d))
{ }

Item::Item(const ItemObject &object)
    : TAbstractModel(), d(new ItemObject(object))
{ }

Item::~Item()
{
    // If the reference count becomes 0,
    // the shared data object 'ItemObject' is deleted.
}

int Item::id() const
{
    return d->id;
}

QString Item::itemTitle() const
{
    return d->item_title;
}

void Item::setItemTitle(const QString &itemTitle)
{
    d->item_title = itemTitle;
}

QString Item::category() const
{
    return d->category;
}

void Item::setCategory(const QString &category)
{
    d->category = category;
}

QString Item::yearOfProduction() const
{
    return d->year_of_production;
}

void Item::setYearOfProduction(const QString &yearOfProduction)
{
    d->year_of_production = yearOfProduction;
}

QString Item::description() const
{
    return d->description;
}

void Item::setDescription(const QString &description)
{
    d->description = description;
}

int Item::quantity() const
{
    return d->quantity;
}

void Item::setQuantity(int quantity)
{
    d->quantity = quantity;
}

QString Item::leadActor() const
{
    return d->lead_actor;
}

void Item::setLeadActor(const QString &leadActor)
{
    d->lead_actor = leadActor;
}

QString Item::leadActress() const
{
    return d->lead_actress;
}

void Item::setLeadActress(const QString &leadActress)
{
    d->lead_actress = leadActress;
}

QString Item::createdAt() const
{
    return d->created_at;
}

QString Item::updatedAt() const
{
    return d->updated_at;
}

int Item::lockRevision() const
{
    return d->lock_revision;
}

Item &Item::operator=(const Item &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

Item Item::create(const QString &itemTitle, const QString &category, const QString &yearOfProduction, const QString &description, int quantity, const QString &leadActor, const QString &leadActress)
{
    ItemObject obj;
    obj.item_title = itemTitle;
    obj.category = category;
    obj.year_of_production = yearOfProduction;
    obj.description = description;
    obj.quantity = quantity;
    obj.lead_actor = leadActor;
    obj.lead_actress = leadActress;
    if (!obj.create()) {
        return Item();
    }
    return Item(obj);
}

Item Item::create(const QVariantMap &values)
{
    Item model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

Item Item::get(int id)
{
    TSqlORMapper<ItemObject> mapper;
    return Item(mapper.findByPrimaryKey(id));
}

Item Item::get(int id, int lockRevision)
{
    TSqlORMapper<ItemObject> mapper;
    TCriteria cri;
    cri.add(ItemObject::Id, id);
    cri.add(ItemObject::LockRevision, lockRevision);
    return Item(mapper.findFirst(cri));
}

int Item::count()
{
    TSqlORMapper<ItemObject> mapper;
    return mapper.findCount();
}

QList<Item> Item::getAll()
{
    return tfGetModelListByCriteria<Item, ItemObject>(TCriteria());
}

QJsonArray Item::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<ItemObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<ItemObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(Item(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *Item::modelData()
{
    return d.data();
}

const TModelObject *Item::modelData() const
{
    return d.data();
}
