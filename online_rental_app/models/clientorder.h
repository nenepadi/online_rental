#ifndef CLIENTORDER_H
#define CLIENTORDER_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class ClientOrderObject;
class QJsonArray;


class T_MODEL_EXPORT ClientOrder : public TAbstractModel
{
public:
    ClientOrder();
    ClientOrder(const ClientOrder &other);
    ClientOrder(const ClientOrderObject &object);
    ~ClientOrder();

    int id() const;
    QString movie() const;
    void setMovie(const QString &movie);
    QString user() const;
    void setUser(const QString &user);
    QString location() const;
    void setLocation(const QString &location);
    bool fulfilled() const;
    void setFulfilled(const bool &fulfilled);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    ClientOrder &operator=(const ClientOrder &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static ClientOrder create(const QString &movie, const QString &user, const QString &location, const bool &fulfilled);
    static ClientOrder create(const QVariantMap &values);
    static ClientOrder get(int id);
    static ClientOrder get(int id, int lockRevision);
    static int count();
    static QList<ClientOrder> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<ClientOrderObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(ClientOrder)
Q_DECLARE_METATYPE(QList<ClientOrder>)

#endif // CLIENTORDER_H
