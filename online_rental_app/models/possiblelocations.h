#ifndef POSSIBLELOCATIONS_H
#define POSSIBLELOCATIONS_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class PossibleLocationsObject;
class QJsonArray;


class T_MODEL_EXPORT PossibleLocations : public TAbstractModel
{
public:
    PossibleLocations();
    PossibleLocations(const PossibleLocations &other);
    PossibleLocations(const PossibleLocationsObject &object);
    ~PossibleLocations();

    int id() const;
    QString locationName() const;
    void setLocationName(const QString &locationName);
    double latitude() const;
    void setLatitude(double latitude);
    double longitude() const;
    void setLongitude(double longitude);
    PossibleLocations &operator=(const PossibleLocations &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static PossibleLocations create(const QString &locationName, double latitude, double longitude);
    static PossibleLocations create(const QVariantMap &values);
    static PossibleLocations get(int id);
    static int count();
    static QList<PossibleLocations> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<PossibleLocationsObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(PossibleLocations)
Q_DECLARE_METATYPE(QList<PossibleLocations>)

#endif // POSSIBLELOCATIONS_H
