#include <TreeFrogModel>
#include "possiblelocations.h"
#include "possiblelocationsobject.h"

PossibleLocations::PossibleLocations()
    : TAbstractModel(), d(new PossibleLocationsObject)
{
    d->id = 0;
    d->latitude = 0;
    d->longitude = 0;
}

PossibleLocations::PossibleLocations(const PossibleLocations &other)
    : TAbstractModel(), d(new PossibleLocationsObject(*other.d))
{ }

PossibleLocations::PossibleLocations(const PossibleLocationsObject &object)
    : TAbstractModel(), d(new PossibleLocationsObject(object))
{ }

PossibleLocations::~PossibleLocations()
{
    // If the reference count becomes 0,
    // the shared data object 'PossibleLocationsObject' is deleted.
}

int PossibleLocations::id() const
{
    return d->id;
}

QString PossibleLocations::locationName() const
{
    return d->location_name;
}

void PossibleLocations::setLocationName(const QString &locationName)
{
    d->location_name = locationName;
}

double PossibleLocations::latitude() const
{
    return d->latitude;
}

void PossibleLocations::setLatitude(double latitude)
{
    d->latitude = latitude;
}

double PossibleLocations::longitude() const
{
    return d->longitude;
}

void PossibleLocations::setLongitude(double longitude)
{
    d->longitude = longitude;
}

PossibleLocations &PossibleLocations::operator=(const PossibleLocations &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

PossibleLocations PossibleLocations::create(const QString &locationName, double latitude, double longitude)
{
    PossibleLocationsObject obj;
    obj.location_name = locationName;
    obj.latitude = latitude;
    obj.longitude = longitude;
    if (!obj.create()) {
        return PossibleLocations();
    }
    return PossibleLocations(obj);
}

PossibleLocations PossibleLocations::create(const QVariantMap &values)
{
    PossibleLocations model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

PossibleLocations PossibleLocations::get(int id)
{
    TSqlORMapper<PossibleLocationsObject> mapper;
    return PossibleLocations(mapper.findByPrimaryKey(id));
}

int PossibleLocations::count()
{
    TSqlORMapper<PossibleLocationsObject> mapper;
    return mapper.findCount();
}

QList<PossibleLocations> PossibleLocations::getAll()
{
    return tfGetModelListByCriteria<PossibleLocations, PossibleLocationsObject>(TCriteria());
}

QJsonArray PossibleLocations::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<PossibleLocationsObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<PossibleLocationsObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(PossibleLocations(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *PossibleLocations::modelData()
{
    return d.data();
}

const TModelObject *PossibleLocations::modelData() const
{
    return d.data();
}
