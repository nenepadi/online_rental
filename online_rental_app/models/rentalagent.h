#ifndef RENTALAGENT_H
#define RENTALAGENT_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractUser>
#include <TAbstractModel>

class TModelObject;
class RentalAgentObject;
class QJsonArray;


class T_MODEL_EXPORT RentalAgent : public TAbstractUser, public TAbstractModel
{
public:
    RentalAgent();
    RentalAgent(const RentalAgent &other);
    RentalAgent(const RentalAgentObject &object);
    ~RentalAgent();

    int agentid() const;
    QString firstname() const;
    void setFirstname(const QString &firstname);
    QString lastname() const;
    void setLastname(const QString &lastname);
    QString email() const;
    void setEmail(const QString &email);
    QString password() const;
    void setPassword(const QString &password);
    QString location() const;
    void setLocation(const QString &location);
    QString role() const;
    void setRole(const QString &role);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    QString identityKey() const { return email(); }
    RentalAgent &operator=(const RentalAgent &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static RentalAgent authenticate(const QString &email, const QString &password);
    static RentalAgent create(const QString &firstname, const QString &lastname, const QString &email, const QString &password, const QString &location, const QString &role);
    static RentalAgent create(const QVariantMap &values);
    static RentalAgent get(int agentid);
    static RentalAgent get(int agentid, int lockRevision);
    static int count();
    static QList<RentalAgent> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<RentalAgentObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(RentalAgent)
Q_DECLARE_METATYPE(QList<RentalAgent>)

#endif // RENTALAGENT_H
