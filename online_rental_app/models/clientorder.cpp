#include <TreeFrogModel>
#include "clientorder.h"
#include "clientorderobject.h"

ClientOrder::ClientOrder()
    : TAbstractModel(), d(new ClientOrderObject)
{
    d->id = 0;
    d->lock_revision = 0;
}

ClientOrder::ClientOrder(const ClientOrder &other)
    : TAbstractModel(), d(new ClientOrderObject(*other.d))
{ }

ClientOrder::ClientOrder(const ClientOrderObject &object)
    : TAbstractModel(), d(new ClientOrderObject(object))
{ }

ClientOrder::~ClientOrder()
{
    // If the reference count becomes 0,
    // the shared data object 'ClientOrderObject' is deleted.
}

int ClientOrder::id() const
{
    return d->id;
}

QString ClientOrder::movie() const
{
    return d->movie;
}

void ClientOrder::setMovie(const QString &movie)
{
    d->movie = movie;
}

QString ClientOrder::user() const
{
    return d->user;
}

void ClientOrder::setUser(const QString &user)
{
    d->user = user;
}

QString ClientOrder::location() const
{
    return d->location;
}

void ClientOrder::setLocation(const QString &location)
{
    d->location = location;
}

bool ClientOrder::fulfilled() const
{
    return d->fulfilled;
}

void ClientOrder::setFulfilled(const bool &fulfilled)
{
    d->fulfilled = fulfilled;
}

QString ClientOrder::createdAt() const
{
    return d->created_at;
}

QString ClientOrder::updatedAt() const
{
    return d->updated_at;
}

int ClientOrder::lockRevision() const
{
    return d->lock_revision;
}

ClientOrder &ClientOrder::operator=(const ClientOrder &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

ClientOrder ClientOrder::create(const QString &movie, const QString &user, const QString &location, const bool &fulfilled)
{
    ClientOrderObject obj;
    obj.movie = movie;
    obj.user = user;
    obj.location = location;
    obj.fulfilled = fulfilled;
    if (!obj.create()) {
        return ClientOrder();
    }
    return ClientOrder(obj);
}

ClientOrder ClientOrder::create(const QVariantMap &values)
{
    ClientOrder model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

ClientOrder ClientOrder::get(int id)
{
    TSqlORMapper<ClientOrderObject> mapper;
    return ClientOrder(mapper.findByPrimaryKey(id));
}

ClientOrder ClientOrder::get(int id, int lockRevision)
{
    TSqlORMapper<ClientOrderObject> mapper;
    TCriteria cri;
    cri.add(ClientOrderObject::Id, id);
    cri.add(ClientOrderObject::LockRevision, lockRevision);
    return ClientOrder(mapper.findFirst(cri));
}

int ClientOrder::count()
{
    TSqlORMapper<ClientOrderObject> mapper;
    return mapper.findCount();
}

QList<ClientOrder> ClientOrder::getAll()
{
    return tfGetModelListByCriteria<ClientOrder, ClientOrderObject>(TCriteria());
}

QJsonArray ClientOrder::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<ClientOrderObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<ClientOrderObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(ClientOrder(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *ClientOrder::modelData()
{
    return d.data();
}

const TModelObject *ClientOrder::modelData() const
{
    return d.data();
}
