#ifndef CLIENT_H
#define CLIENT_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractUser>
#include <TAbstractModel>

class TModelObject;
class ClientObject;
class QJsonArray;


class T_MODEL_EXPORT Client : public TAbstractUser, public TAbstractModel
{
public:
    Client();
    Client(const Client &other);
    Client(const ClientObject &object);
    ~Client();

    int clientid() const;
    QString firstname() const;
    void setFirstname(const QString &firstname);
    QString lastname() const;
    void setLastname(const QString &lastname);
    QString username() const;
    void setUsername(const QString &username);
    QString email() const;
    void setEmail(const QString &email);
    QString password() const;
    void setPassword(const QString &password);
    QString pickLocation() const;
    void setPickLocation(const QString &pickLocation);
    bool isActive() const;
    void setIsActive(const bool &isActive);
    int entitledNumberMovies() const;
    void setEntitledNumberMovies(int entitledNumberMovies);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    QString identityKey() const { return email(); }
    Client &operator=(const Client &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static Client authenticate(const QString &email, const QString &password);
    static Client create(const QString &firstname, const QString &lastname, const QString &username, const QString &email, const QString &password, const QString &pickLocation, const bool &isActive, int entitledNumberMovies);
    static Client create(const QVariantMap &values);
    static Client get(int clientid);
    static Client get(int clientid, int lockRevision);
    static int count();
    static QList<Client> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<ClientObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(Client)
Q_DECLARE_METATYPE(QList<Client>)

#endif // CLIENT_H
