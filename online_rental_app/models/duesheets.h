#ifndef DUESHEETS_H
#define DUESHEETS_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class DueSheetsObject;
class QJsonArray;


class T_MODEL_EXPORT DueSheets : public TAbstractModel
{
public:
    DueSheets();
    DueSheets(const DueSheets &other);
    DueSheets(const DueSheetsObject &object);
    ~DueSheets();

    int id() const;
    QString orderedItem() const;
    void setOrderedItem(const QString &orderedItem);
    QString orderedBy() const;
    void setOrderedBy(const QString &orderedBy);
    QString processedDate() const;
    void setProcessedDate(const QString &processedDate);
    QString dueDate() const;
    void setDueDate(const QString &dueDate);
    bool checkIn() const;
    void setCheckIn(const bool &checkIn);
    QString processor() const;
    void setProcessor(const QString &processor);
    QString dayFineLastActivate() const;
    void setDayFineLastActivate(const QString &dayFineLastActivate);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    DueSheets &operator=(const DueSheets &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static DueSheets create(const QString &orderedItem, const QString &orderedBy, const QString &processedDate, const QString &dueDate, const bool &checkIn, const QString &processor, const QString &dayFineLastActivate);
    static DueSheets create(const QVariantMap &values);
    static DueSheets get(int id);
    static DueSheets get(int id, int lockRevision);
    static int count();
    static QList<DueSheets> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<DueSheetsObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(DueSheets)
Q_DECLARE_METATYPE(QList<DueSheets>)

#endif // DUESHEETS_H
