#ifndef ITEM_H
#define ITEM_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class ItemObject;
class QJsonArray;


class T_MODEL_EXPORT Item : public TAbstractModel
{
public:
    Item();
    Item(const Item &other);
    Item(const ItemObject &object);
    ~Item();

    int id() const;
    QString itemTitle() const;
    void setItemTitle(const QString &itemTitle);
    QString category() const;
    void setCategory(const QString &category);
    QString yearOfProduction() const;
    void setYearOfProduction(const QString &yearOfProduction);
    QString description() const;
    void setDescription(const QString &description);
    int quantity() const;
    void setQuantity(int quantity);
    QString leadActor() const;
    void setLeadActor(const QString &leadActor);
    QString leadActress() const;
    void setLeadActress(const QString &leadActress);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    Item &operator=(const Item &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static Item create(const QString &itemTitle, const QString &category, const QString &yearOfProduction, const QString &description, int quantity, const QString &leadActor, const QString &leadActress);
    static Item create(const QVariantMap &values);
    static Item get(int id);
    static Item get(int id, int lockRevision);
    static int count();
    static QList<Item> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<ItemObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(Item)
Q_DECLARE_METATYPE(QList<Item>)

#endif // ITEM_H
