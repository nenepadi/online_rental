#include <TreeFrogModel>
#include "category.h"
#include "categoryobject.h"

Category::Category()
    : TAbstractModel(), d(new CategoryObject)
{
    d->cat_id = 0;
    d->lock_revision = 0;
}

Category::Category(const Category &other)
    : TAbstractModel(), d(new CategoryObject(*other.d))
{ }

Category::Category(const CategoryObject &object)
    : TAbstractModel(), d(new CategoryObject(object))
{ }

Category::~Category()
{
    // If the reference count becomes 0,
    // the shared data object 'CategoryObject' is deleted.
}

int Category::catId() const
{
    return d->cat_id;
}

QString Category::categoryName() const
{
    return d->category_name;
}

void Category::setCategoryName(const QString &categoryName)
{
    d->category_name = categoryName;
}

QString Category::createdAt() const
{
    return d->created_at;
}

QString Category::updatedAt() const
{
    return d->updated_at;
}

int Category::lockRevision() const
{
    return d->lock_revision;
}

Category &Category::operator=(const Category &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

Category Category::create(const QString &categoryName)
{
    CategoryObject obj;
    obj.category_name = categoryName;
    if (!obj.create()) {
        return Category();
    }
    return Category(obj);
}

Category Category::create(const QVariantMap &values)
{
    Category model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

Category Category::get(int catId)
{
    TSqlORMapper<CategoryObject> mapper;
    return Category(mapper.findByPrimaryKey(catId));
}

Category Category::get(int catId, int lockRevision)
{
    TSqlORMapper<CategoryObject> mapper;
    TCriteria cri;
    cri.add(CategoryObject::CatId, catId);
    cri.add(CategoryObject::LockRevision, lockRevision);
    return Category(mapper.findFirst(cri));
}

int Category::count()
{
    TSqlORMapper<CategoryObject> mapper;
    return mapper.findCount();
}

QList<Category> Category::getAll()
{
    return tfGetModelListByCriteria<Category, CategoryObject>(TCriteria());
}

QJsonArray Category::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<CategoryObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<CategoryObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(Category(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *Category::modelData()
{
    return d.data();
}

const TModelObject *Category::modelData() const
{
    return d.data();
}
