#ifndef HOMECONTROLLER_H
#define HOMECONTROLLER_H

#include "applicationcontroller.h"
#include "rentalagent.h"
#include "possiblelocations.h"
#include <QFile>

using namespace std;


class T_CONTROLLER_EXPORT HomeController : public ApplicationController
{
    Q_OBJECT
public:
    HomeController() { }
    HomeController(const HomeController &other);

public slots:
    void index();
};

T_DECLARE_CONTROLLER(HomeController, homecontroller)

#endif // HOMECONTROLLER_H
