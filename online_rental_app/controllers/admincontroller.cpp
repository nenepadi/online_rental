#include "admincontroller.h"

AdminController::AdminController(const AdminController &)
    : ApplicationController()
{
    setLayout("admin");
}

void AdminController::index()
{
    int clientCount = Client::count();
    int orderCount = ClientOrder::count();
    int staffCount = RentalAgent::count();
    int salesCount = DueSheets::count();

    texport(clientCount);
    texport(orderCount);
    texport(staffCount);
    texport(salesCount);
    render("index");
}


//Staff actions
void AdminController::all_staff()
{
    QList<RentalAgent> staffList = RentalAgent::getAll();
    texport(staffList);
    render("allStaff");
}

void AdminController::staff_entry()
{
    QStringList locations = alocations();
    texport(locations);
    renderStaffEntry();
}

void AdminController::create_staff()
{
    if(httpRequest().method() != Tf::Post) { return; }

    QVariantMap form = httpRequest().formItems("agent");
    RentalAgent agent = RentalAgent::create(form);
    if (!agent.isNull()) {
        QString notice = "Created successfully.";
        tflash(notice);
        redirect(urla("all_staff"));
    } else {
        QString error = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Failed to create!!!</div>";
        texport(error);
        renderStaffEntry(form);
    }
}

void AdminController::renderStaffEntry(const QVariantMap &agent)
{
    texport(agent);
    render("newStaff");
}

void AdminController::remove_staff(const QString &pk)
{
    if(httpRequest().method() != Tf::Post){
        return;
    }

    RentalAgent agent = RentalAgent::get(pk.toInt());
    agent.remove();
    redirect(urla("all_staff"));
}

void AdminController::edit_staff(const QString &pk)
{
   RentalAgent agent = RentalAgent::get(pk.toInt());
   if(!agent.isNull()){
       QStringList locations = alocations();
       texport(locations);
       renderStaffEdit(agent.toVariantMap());
   } else {
       redirect(urla("staff_entry"));
   }

}

void AdminController::save_staff(const QString &pk)
{
    if(httpRequest().method() != Tf::Post){
        return;
    }

    QString error;
    RentalAgent agent = RentalAgent::get(pk.toInt());
    if (agent.isNull()) {
        error = "Original data not found. It may have been updated/removed by another transaction.";
        tflash(error);
        redirect(urla("edit_staff", pk));
        return;
    }

    QVariantMap form = httpRequest().formItems("agent");
    agent.setProperties(form);
    if(agent.save()) {
        QString notice = "Updated successfully.";
        tflash(notice);
        redirect(urla("all_staff"));
    } else {
        error = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Failed to update!!!</div>";
        texport(error);
        renderStaffEdit(form);
    }
}

void AdminController::renderStaffEdit(const QVariantMap &agent)
{
    texport(agent);
    render("newStaff");
}


//Product actions
void AdminController::all_items()
{
    QList<Item> itemList = Item::getAll();
    texport(itemList);
    render("allItem");
}

void AdminController::item_entry()
{
    QList<Category> catList = Category::getAll();
    texport(catList);
    renderItemEntry();
}

void AdminController::create_item()
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QVariantMap form = httpRequest().formItems("item");
    Item item = Item::create(form);
    if (!item.isNull()) {
        QString notice = "Created successfully.";
        tflash(notice);
        redirect(urla("all_items"));
    } else {
        QString error = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Failed to create!!!</div>";
        texport(error);
        renderItemEntry(form);
    }
}

void AdminController::renderItemEntry(const QVariantMap &item)
{
    texport(item);
    render("newItem");
}

void AdminController::edit_item(const QString &pk)
{
    Item item = Item::get(pk.toInt());
    if (!item.isNull()) {
        QList<Category> catList = Category::getAll();
        texport(catList);
        renderItemEdit(item.toVariantMap());
    } else {
        redirect(urla("item_entry"));
    }
}

void AdminController::save_item(const QString &pk)
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QString error;
    Item item = Item::get(pk.toInt());
    if (item.isNull()) {
        error = "Original data not found. It may have been updated/removed by another transaction.";
        tflash(error);
        redirect(urla("edit_item", pk));
        return;
    }

    QVariantMap form = httpRequest().formItems("item");
    item.setProperties(form);
    if (item.save()) {
        QString notice = "Updated successfully.";
        tflash(notice);
        redirect(urla("all_items"));
    } else {
        error = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Failed to update!!!</div>";
        texport(error);
        renderItemEdit(form);
    }
}

void AdminController::renderItemEdit(const QVariantMap &item)
{
    texport(item);
    render("newItem");
}

void AdminController::remove_item(const QString &pk)
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    Item item = Item::get(pk.toInt());
    item.remove();
    redirect(urla("all_items"));
}

void AdminController::category_create()
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QString catName = httpRequest().formItemValue("categoryName");
    Category category = Category::create(catName);
    if(!category.isNull()){
        QString notice = "Created successfully";
        tflash(notice);
        redirect(urla("categoryListings"));
    } else{
        QString error = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Failed to create!!!</div>";
        texport(error);
        render("categoryListings");
    }
}

void AdminController::category_remove(const QString &pk)
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    Category catlog = Category::get(pk.toInt());
    catlog.remove();
    redirect(urla("categoryListings"));
}

void AdminController::categoryListings()
{
    QList<Category> allCategories = Category::getAll();
    texport(allCategories);
    render();
}

void AdminController::category_edit(const QString &pk)
{
    Category catlog = Category::get(pk.toInt());
    if (!catlog.isNull()) {
        texport(catlog);
        QList<Category> allCategories = Category::getAll();
        texport(allCategories);
        render("categoryListings");
    } else {
        redirect(urla("categoryListings"));
    }
}

void AdminController::category_save(const QString &pk)
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QString error;
    Category catlog = Category::get(pk.toInt());
    if (catlog.isNull()) {
        error = "Original data not found. It may have been updated/removed by another transaction.";
        tflash(error);
        redirect(urla("category_edit", pk));
        return;
    }

    QString catName = httpRequest().formItemValue("categoryName");
    catlog.setCategoryName(catName);
    if (catlog.save()) {
        QString notice = "Updated successfully.";
        tflash(notice);
        redirect(urla("categoryListings"));
    } else {
        error = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Failed to update!!!</div>";
        texport(error);
        render("categoryListings");
    }

}


//Client actions
void AdminController::all_clients()
{
    QList<Client> clients = Client::getAll();
    texport(clients);
    render("allClients");
}


//Reports
void AdminController::reports()
{
    QList<Item> items = Item::getAll();
    QList<Client> clients = Client::getAll();
    QList<ClientOrder> clientorders = ClientOrder::getAll();
    QList<DueSheets> duesheets = DueSheets::getAll();
    QList<RentalAgent> agents = RentalAgent::getAll();

    texport(items);
    texport(clients);
    texport(clientorders);
    texport(duesheets);
    texport(agents);

    render();
}


//Helper functions
QStringList AdminController::alocations()
{
    QList<PossibleLocations> all_locations = PossibleLocations::getAll();
    QStringList locations;
    foreach (PossibleLocations loc, all_locations) {
        locations.append(loc.locationName());
    }
    return locations;
}

int AdminController::getAdmin()
{
    QString login_name = identityKeyOfLoginUser();
    int iden;
    QList<RentalAgent> all_staff = RentalAgent::getAll();
    foreach(RentalAgent ag, all_staff){
        if(ag.email() == login_name){
            iden = ag.agentid();
            break;
        }
    }
    return iden;
}


void AdminController::setAccessRules()
{
    RentalAgent admin = RentalAgent::get(getAdmin());
    setDenyDefault(true);
    QStringList actions;
    actions << "index" << "all_staff" << "staff_entry" << "remove_staff" << "edit_staff"
            << "all_items" << "item_entry" << "remove_item" << "edit_item" << "all_clients"
            << "categoryListings" << "category_create" << "category_edit" << "category_save"
            << "category_remove" << "save_item" << "create_staff" << "save_staff" << "create_item"
            << "reports";
    if(admin.role() == "administrator"){
        setAllowUser(identityKeyOfLoginUser(), actions);
    } else{
        setDenyUser(identityKeyOfLoginUser(), actions);
    }
}

bool AdminController::preFilter()
{
    RentalAgent admin = RentalAgent::get(getAdmin());

    if (!isUserLoggedIn()) {
        redirect(url("Account", "staffLoginForm"));
        return false;
    } else if(!validateAccess(&admin)) {
        renderErrorResponse(403);
        return false;
    }

    texport(admin);

    return true;
}


// Don't remove below this line
T_REGISTER_CONTROLLER(admincontroller)
