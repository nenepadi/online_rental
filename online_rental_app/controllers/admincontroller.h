#ifndef ADMINCONTROLLER_H
#define ADMINCONTROLLER_H

#include "applicationcontroller.h"
#include "rentalagent.h"
#include "item.h"
#include "category.h"
#include "client.h"
#include "duesheets.h"
#include "clientorder.h"
#include "possiblelocations.h"


class T_CONTROLLER_EXPORT AdminController : public ApplicationController
{
    Q_OBJECT
public:
    AdminController() { }
    AdminController(const AdminController &other);
    int getAdmin();

private:
    void renderItemEntry(const QVariantMap &agent = QVariantMap());
    void renderItemEdit(const QVariantMap &agent = QVariantMap());
    void renderStaffEntry(const QVariantMap &agent = QVariantMap());
    void renderStaffEdit(const QVariantMap &agent = QVariantMap());

public slots:
    void index();

    //Staff actions
    void all_staff();
    void staff_entry();
    void create_staff();
    void edit_staff(const QString &pk);
    void remove_staff(const QString &pk);
    void save_staff(const QString &pk);

    //Product actions
    void all_items();
    void item_entry();
    void create_item();
    void edit_item(const QString &pk);
    void save_item(const QString &pk);
    void remove_item(const QString &pk);

    void categoryListings();
    void category_edit(const QString &pk);
    void category_create();
    void category_save(const QString &pk);
    void category_remove(const QString &pk);

    //Client actions
    void all_clients();

    //Reports
    //void weeklyReports();
    void reports();

    //Helper functions
    QStringList alocations();
    //void dReports(const QDate &date);

protected:
    bool preFilter();
    void setAccessRules();

};

T_DECLARE_CONTROLLER(AdminController, admincontroller)

#endif // ADMINCONTROLLER_H
