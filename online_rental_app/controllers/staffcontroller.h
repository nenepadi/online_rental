#ifndef STAFFCONTROLLER_H
#define STAFFCONTROLLER_H

#include "applicationcontroller.h"
#include "rentalagent.h"
#include "client.h"
#include "clientorder.h"
#include <QDate>
#include "item.h"
#include "duesheets.h"

class T_CONTROLLER_EXPORT StaffController : public ApplicationController
{
    Q_OBJECT
public:
    StaffController() { }
    StaffController(const StaffController &other);

private:
    void renderClientEntry(const QVariantMap &client = QVariantMap());
    void renderClientEdit(const QVariantMap &client = QVariantMap());
    int getLoggedStaff();
    QList<Client> findByLocation(const QString location);

public slots:
    void staff_dash();

    void client_listings(const QString &location);
    void client_entry();
    void create_client();
    void edit_client(const QString &pk);
    void save_client(const QString &pk);
    void remove_client(const QString &pk);

    //Transaction fulfillments
    void orderListings(const QString &location);
    void orderProcessForm(const QString &pk);
    void orderProcessed(const QString &pk);
    void viewDueSheet();
    void activateFine(const QString &pk);
    void checkIn(const QString &pk);
    void checkInProcess(const QString &pk);


protected:
    bool preFilter();
    void setAccessRules();

};

T_DECLARE_CONTROLLER(StaffController, staffcontroller)

#endif // STAFFCONTROLLER_H
