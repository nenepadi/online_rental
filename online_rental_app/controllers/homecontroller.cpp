#include "homecontroller.h"


HomeController::HomeController(const HomeController &)
    : ApplicationController()
{
    setLayout("home");
}

void HomeController::index()
{
    QList<RentalAgent> agents = RentalAgent::getAll();
    QList<PossibleLocations> allLocations = PossibleLocations::getAll();
    QVariantList activeLocations;

    foreach (PossibleLocations loc, allLocations) {
        foreach(RentalAgent agent, agents){
            if(agent.location() == loc.locationName()){
                activeLocations.append(loc.toVariantMap());
            }
        }
    }

    QJsonDocument doc = QJsonDocument::fromVariant(activeLocations);

    QFile jsonFile("public/locations.json");
    jsonFile.open(QFile::WriteOnly);
    jsonFile.write("{ \"locations\" : " + doc.toJson(QJsonDocument::Compact) + "}");

    render();
}


// Don't remove below this line
T_REGISTER_CONTROLLER(homecontroller)
