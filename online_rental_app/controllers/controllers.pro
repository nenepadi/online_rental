TARGET = controller
TEMPLATE = lib
CONFIG += shared
QT += network sql xml
QT -= gui
DEFINES += TF_DLL
DESTDIR = ../lib
INCLUDEPATH += ../helpers ../models
DEPENDPATH  += ../helpers ../models
LIBS += -L../lib -lhelper -lmodel

include(../appbase.pri)

HEADERS += applicationcontroller.h
SOURCES += applicationcontroller.cpp
HEADERS += staffcontroller.h
SOURCES += staffcontroller.cpp
HEADERS += homecontroller.h
SOURCES += homecontroller.cpp
HEADERS += accountcontroller.h
SOURCES += accountcontroller.cpp
HEADERS +=
SOURCES +=
HEADERS += shopfrontcontroller.h
SOURCES += shopfrontcontroller.cpp
HEADERS += admincontroller.h
SOURCES += admincontroller.cpp
