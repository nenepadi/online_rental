#ifndef ACCOUNTCONTROLLER_H
#define ACCOUNTCONTROLLER_H

#include "applicationcontroller.h"
#include "rentalagent.h"
#include "client.h"
#include "possiblelocations.h"

class T_CONTROLLER_EXPORT AccountController : public ApplicationController
{
    Q_OBJECT
public:
    AccountController() { }
    AccountController(const AccountController &other);

public slots:
    //client specific actions
    void clientLoginForm();
    void clientLogin();
    void clientLogout();
    void clientProfile();
    void editProfile();
    void changeProfileForm();
    void changePassword();
    void changePickLocation();
    void changeLocation();

    //staff specific actions
    void staffLoginForm();
    void staffLogin();
    void staffLogout();
    void lock_screen();
    void lockScreenForm();
    void passwordForm();
    void passwordChange();

    //helper functions
    void form(QString myView);
    void logout_redirect(QString myUrl);
    int getLoggedStaff();
    int getLoggedUser();


protected:
    //bool preFilter();
};

T_DECLARE_CONTROLLER(AccountController, accountcontroller)

#endif // ACCOUNTCONTROLLER_H
