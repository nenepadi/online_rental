#include "accountcontroller.h"

RentalAgent agent;
Client client;
QString errorMessage = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Login <strong>failed!!!</strong></div>";

AccountController::AccountController(const AccountController &)
    : ApplicationController()
{ }


//staff specific functions
void AccountController::staffLoginForm()
{
    form("staffLoginForm");
}

void AccountController::staffLogin()
{
    QString email = httpRequest().formItemValue("email");
    QString password = httpRequest().formItemValue("password");

    agent = RentalAgent::authenticate(email, password);
    if(!agent.isNull() && agent.role() == "staff"){
        userLogin(&agent);
        redirect(QUrl("/Staff/staff_dash"));
    }
    else if(!agent.isNull() && agent.role() == "administrator"){
        userLogin(&agent);
        redirect(QUrl("/Admin/index"));
    }
    else{
        texport(errorMessage);
        render("staffLoginForm");
    }
}

void AccountController::staffLogout()
{
    logout_redirect("Account/staffLoginForm");
}

void AccountController::lockScreenForm()
{
    RentalAgent staff = RentalAgent::get(getLoggedStaff());
    texport(staff);
    render("lockScreen");
}

void AccountController::lock_screen()
{
    RentalAgent staff = RentalAgent::get(getLoggedStaff());

    QString email = staff.email();
    QString password = httpRequest().formItemValue("password");

    agent = RentalAgent::authenticate(email, password);
    if(!agent.isNull() && agent.role() == "staff"){
        userLogin(&agent);
        redirect(QUrl("/Staff/staff_dash"));
    }
    else if(!agent.isNull() && agent.role() == "administrator"){
        userLogin(&agent);
        redirect(QUrl("/Admin/index"));
    }
    else{
        texport(errorMessage);
        render("lockScreenForm");
    }
}

void AccountController::passwordForm()
{
    RentalAgent agent = RentalAgent::get(getLoggedStaff());
    texport(agent);
    render();
}

void AccountController::passwordChange()
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    RentalAgent agent = RentalAgent::get(getLoggedStaff());
    QString old_pass = httpRequest().formItemValue("opass");
    QString new_pass = httpRequest().formItemValue("npass");
    QString rt_pass = httpRequest().formItemValue("rpass");

    if(old_pass == agent.password() && new_pass == rt_pass && (new_pass != " " && rt_pass != " ")){
        agent.setPassword(new_pass);
        if(agent.update()){
            (agent.role() == "administrator") ? redirect(url("Admin", "index")) : redirect(url("Staff", "staff_dash"));
        } else{
            QString error = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Something is wrong. Are you sure you are a staff!!!</div>";
            texport(error);
            render("passwordForm");
        }
    } else{
        QString error = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Make sure new password and retype password match. Also make sure you type the correct old password</div>";
        texport(error);
        render("passwordForm");
    }
}

// client specific actions ...
void AccountController::clientLoginForm()
{
    form("clientLoginForm");
}

void AccountController::clientLogin()
{
    QString email = httpRequest().formItemValue("email");
    QString password = httpRequest().formItemValue("password");

    client = Client::authenticate(email, password);
    if (!client.isNull() && client.isActive()) {
        userLogin(&client);
        redirect(QUrl("/Shopfront/index"));
    } else {
        texport(errorMessage);
        render("clientLoginForm");
    }
}

void AccountController::clientLogout()
{
    logout_redirect("Home/index");
}

void AccountController::clientProfile()
{
    Client user = Client::get(getLoggedUser());
    QList<PossibleLocations> loc = PossibleLocations::getAll();
    PossibleLocations location;
    foreach(PossibleLocations lo, loc){
        if(lo.locationName() == user.pickLocation()){
            location = lo;
        }
    }

    texport(user);
    texport(location);
    render("clientProfile");
}

void AccountController::editProfile()
{
    if(httpRequest().method() != Tf::Post){
        return;
    }

    QString error;
    Client user = Client::get(getLoggedUser());
    if (user.isNull()) {
        error = "Original data not found. It may have been updated/removed by another transaction.";
        tflash(error);
        redirect(urla("clientProfile"));
        return;
    }

    QString nfname, nlname, nuname;
    nfname = httpRequest().formItemValue("nfname");
    nlname = httpRequest().formItemValue("nlname");
    nuname = httpRequest().formItemValue("nuname");

    if(nfname != " " && nlname != " " && nuname != " "){
        user.setFirstname(nfname);
        user.setLastname(nlname);
        user.setUsername(nuname);
        if(user.update()){
            QString notice = "Updated successfully.";
            tflash(notice);
            redirect(urla("clientProfile"));
        } else{
            error = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Failed to update!!!</div>";
            texport(error);
            render("changeProfileForm");
        }
    }
//    user.setProperties(form);
//    if(user.save()) {
//        QString notice = "Updated successfully.";
//        tflash(notice);
//        redirect(urla("clientProfile"));
//    } else {
//        error = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Failed to update!!!</div>";
//        texport(error);
//        renderEdit(form);
//    }
}

void AccountController::changeProfileForm()
{
    Client user = Client::get(getLoggedUser());

    if(!user.isNull()){
        texport(user);
        render();
    } else {
        redirect(urla("clientProfile"));
    }
}

void AccountController::changePassword()
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    Client user = Client::get(getLoggedUser());
    QString old_pass = httpRequest().formItemValue("oldpass");
    QString new_pass = httpRequest().formItemValue("newpass");
    QString rt_pass = httpRequest().formItemValue("rtpass");

    if(old_pass == user.password() && new_pass == rt_pass && (new_pass != " " && rt_pass != " ")){
        user.setPassword(new_pass);
        if(user.update()){
            redirect(urla("clientProfile"));
        } else{
            redirect(urla("clientProfile"));
        }
    } else{
        render("clientProfile");
    }
}

void AccountController::changePickLocation()
{
    Client user = Client::get(getLoggedUser());
    texport(user);
    render();
}

void AccountController::changeLocation()
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    Client user = Client::get(getLoggedUser());
    QString nlocation = httpRequest().formItemValue("nlocation");

    if(nlocation != " "){
        user.setPickLocation(nlocation);
        if(user.update()){
            redirect(urla("clientProfile"));
        } else{
            redirect(urla("clientProfile"));
        }
    } else{
        render("changePickLocation");
    }
}


//helper functions ...
void AccountController::form(QString myView)
{
    userLogout();
    render(myView);
}

int AccountController::getLoggedStaff()
{
    QString login_name = identityKeyOfLoginUser();
    int iden;
    QList<RentalAgent> all_staff = RentalAgent::getAll();
    foreach(RentalAgent ag, all_staff){
        if(ag.email() == login_name){
            iden = ag.agentid();
            break;
        }
    }
    return iden;
}

int AccountController::getLoggedUser()
{
    QString login_name = identityKeyOfLoginUser();
    int iden;
    QList<Client> users = Client::getAll();
    foreach(Client user, users){
        if(user.email() == login_name){
            iden = user.clientid();
            break;
        }
    }
    return iden;
}

void AccountController::logout_redirect(QString myUrl)
{
    userLogout();
    redirect(url(myUrl));
}


// Don't remove below this line
T_REGISTER_CONTROLLER(accountcontroller)
