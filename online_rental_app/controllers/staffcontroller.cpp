#include "staffcontroller.h"

StaffController::StaffController(const StaffController &) : ApplicationController()
{
    setLayout("staff");
}

void StaffController::staff_dash()
{
    RentalAgent staff = RentalAgent::get(getLoggedStaff());

    //Clients
    QList<Client> mClients = findByLocation(staff.location());
    int clientCount = mClients.count();

    //Orders
    QList<ClientOrder> gOrderList = ClientOrder::getAll();
    QList<ClientOrder> mOrderList, ufOrderList;
    foreach(ClientOrder co, gOrderList){
        if(co.location() == staff.location()){
            mOrderList.append(co);
        }
    }
    foreach(ClientOrder c, mOrderList){
        if(!c.fulfilled()){
            ufOrderList.append(c);
        }
    }

    int orderCount = mOrderList.count();



    //Sales
    QList<DueSheets> dueTrans = DueSheets::getAll();
    QList<DueSheets> mDues;
    foreach(DueSheets ds, dueTrans){
        if(ds.processor() == staff.email()){
            mDues.append(ds);
        }
    }
    int salesCount = mDues.count();

    QList<DueSheets> dues;
    foreach (DueSheets ds, dueTrans) {
        if(QDate::fromString(ds.dueDate(), "yyyy-MM-dd")  <= QDate::currentDate() && !ds.checkIn() && ds.processor() == staff.email()){
            dues.append(ds);
        }
    }

    texport(dues);
    texport(ufOrderList);
    texport(clientCount);
    texport(salesCount);
    texport(orderCount);
    render("staffDash");
}

void StaffController::client_listings(const QString &location)
{
    QList<Client> clientByLocationList = findByLocation(location);
    texport(clientByLocationList);
    render("clientListings");
}

void StaffController::renderClientEntry(const QVariantMap &client)
{
    texport(client);
    render("clientEntry");
}

void StaffController::renderClientEdit(const QVariantMap &client)
{
    texport(client);
    render("clientEntry");
}

void StaffController::client_entry()
{
    renderClientEntry();
}

void StaffController::edit_client(const QString &pk)
{
    Client client = Client::get(pk.toInt());
    if (!client.isNull()) {
        renderClientEdit(client.toVariantMap());
    } else {
        redirect(urla("client_entry"));
    }
}

void StaffController::save_client(const QString &pk)
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QString error;
    Client client = Client::get(pk.toInt());
    if (client.isNull()) {
        error = "Original data not found. It may have been updated/removed by another transaction.";
        tflash(error);
        redirect(urla("edit_client", pk));
        return;
    }

    QVariantMap form = httpRequest().formItems("client");
    client.setProperties(form);
    RentalAgent agent = RentalAgent::get(getLoggedStaff());
    if (client.save()) {
        QString notice = "Updated successfully.";
        tflash(notice);
        redirect(urla("client_listings", agent.location()));
    } else {
        error = "Failed to update.";
        texport(error);
        renderClientEdit(form);
    }
}

void StaffController::create_client()
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QVariantMap form = httpRequest().formItems("client");
    Client client = Client::create(form);
    RentalAgent agent = RentalAgent::get(getLoggedStaff());
    if (!client.isNull()) {
        QString notice = "Created successfully.";
        tflash(notice);
        redirect(urla("client_listings", agent.location()));
    } else {
        QString error = "Failed to create.";
        texport(error);
        renderClientEntry(form);
    }
}

void StaffController::remove_client(const QString &pk)
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    RentalAgent loginStaff = RentalAgent::get(getLoggedStaff());
    Client client = Client::get(pk.toInt());
    client.remove();
    redirect(urla("client_listings", loginStaff.location()));
}

void StaffController::orderListings(const QString &location)
{
    QList<ClientOrder> gOrderList = ClientOrder::getAll();
    QList<ClientOrder> ufOrderList, fOrderList;
    foreach(ClientOrder co, gOrderList){
        if(co.location() == location){
            if(!co.fulfilled()){
                ufOrderList.append(co);
            } else{
                fOrderList.append(co);
            }
        }
    }

    texport(fOrderList);
    texport(ufOrderList);

    render();
}

void StaffController::orderProcessForm(const QString &pk)
{
    RentalAgent agent = RentalAgent::get(getLoggedStaff());
    QList<ClientOrder> gOrderList = ClientOrder::getAll();
    QList<ClientOrder> ufOrderList;
    foreach(ClientOrder co, gOrderList){
        if(co.location() == agent.location()){
            if(!co.fulfilled()){
                ufOrderList.append(co);
            }
        }
    }
    texport(ufOrderList);

    QDate date = QDate::currentDate();
    texport(date);

    ClientOrder cOrder = ClientOrder::get(pk.toInt());
    QVariantMap clientOrder = cOrder.toVariantMap();
    texport(clientOrder);
    render("orderListings");
}

void StaffController::orderProcessed(const QString &pk)
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QString error;
    ClientOrder cOrder = ClientOrder::get(pk.toInt());
    if (cOrder.isNull()) {
        error = "Original data not found. It may have been updated/removed by another transaction.";
        tflash(error);
        redirect(urla("orderProcessForm", pk));
        return;
    }

    QDate today = QDate::currentDate();

    QVariantMap clientOrder = cOrder.toVariantMap();
    QString processedDate = httpRequest().formItemValue("processedDate", today.toString());
    QString dueDate = httpRequest().formItemValue("dueDate", today.addDays(2).toString());


    QList<Item> aItems = Item::getAll();
    QList<Client> aClients = Client::getAll();
    bool isUpdateDone = false;

    foreach(Item i, aItems){
        if(i.itemTitle() == clientOrder["movie"].toString()){
            i.setQuantity(i.quantity() - 1);
            //if(i.update()) isUpdateDone = true;
            isUpdateDone = ((i.update()) ? true : false);
            break;
        }
    }

    foreach(Client c, aClients){
        if(c.email() == clientOrder["user"].toString() && isUpdateDone){
            c.setEntitledNumberMovies(c.entitledNumberMovies() - 1);
            //if(c.update() && isUpdateDone) isUpdateDone = true;
            isUpdateDone = ((c.update()) ? true : false);
            break;
        }
    }

    cOrder.setFulfilled(true);
    //if(cOrder.update() && isUpdateDone) isUpdateDone = true;
    isUpdateDone = ((cOrder.update() && isUpdateDone) ? true : false);

    RentalAgent staff = RentalAgent::get(getLoggedStaff());
    DueSheets dueSheet = DueSheets::create(clientOrder["movie"].toString(),
            clientOrder["user"].toString(), processedDate, dueDate, false, staff.email(), " ");
    
    if(!dueSheet.isNull() && isUpdateDone){
        QString notice = "Updated successfully.";
        tflash(notice);
        redirect(urla("orderListings", staff.location()));
    } else{
        error = "Failed to update.";
        texport(error);
        render("orderListings");
    }
}

void StaffController::viewDueSheet()
{
    QList<DueSheets> dueTrans = DueSheets::getAll();
    RentalAgent staff = RentalAgent::get(getLoggedStaff());
    QList<DueSheets> dues;
    foreach (DueSheets ds, dueTrans) {
        if(QDate::fromString(ds.dueDate(), "yyyy-MM-dd")  <= QDate::currentDate() && !ds.checkIn() && ds.processor() == staff.email()){
            dues.append(ds);
        }
    }

    texport(dues);
    render("dueSheet");
}

void StaffController::activateFine(const QString &pk)
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    bool isUpdateDone = false;

    DueSheets dueSheet = DueSheets::get(pk.toInt());

    QList<Client> allClients = Client::getAll();
    foreach(Client cl, allClients){
        if(cl.email() == dueSheet.orderedBy()){
            cl.setEntitledNumberMovies(cl.entitledNumberMovies() - 1);
            //if(cl.update()) isUpdateDone = true;
            isUpdateDone = ((cl.update()) ? true : false);
        }
    }

    dueSheet.setDayFineLastActivate(QDate::currentDate().toString("yyyy-MM-dd"));
    if(dueSheet.update() && isUpdateDone){
        QString notice = "Updated successfully.";
        tflash(notice);
        redirect(urla("viewDueSheet"));
    } else{
        QString error = "Failed to update.";
        texport(error);
        render("dueSheet");
    }

}

void StaffController::checkIn(const QString &pk)
{
    DueSheets dueSheet = DueSheets::get(pk.toInt());

    QList<DueSheets> dueTrans = DueSheets::getAll();
    QList<DueSheets> cdues;
    foreach (DueSheets ds, dueTrans) {
        if(QDate::fromString(ds.dueDate(), "yyyy-MM-dd")  <= QDate::currentDate() && !ds.checkIn()){
            if(ds.id() == dueSheet.id()) continue;
            cdues.append(ds);
        }
    }
    texport(cdues);
    texport(dueSheet);
    render("dueSheet");
}

void StaffController::checkInProcess(const QString &pk)
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    bool isUpdateDone = false;

    DueSheets dueSheet = DueSheets::get(pk.toInt());
    dueSheet.setCheckIn(true);

    isUpdateDone = ((dueSheet.update()) ? true : false);

    QList<Item> allItems = Item::getAll();
    foreach(Item it, allItems){
        if(it.itemTitle() == dueSheet.orderedItem() && isUpdateDone){
            it.setQuantity(it.quantity() + 1);
            //if(it.update()) isUpDateDone = true;
            isUpdateDone = ((it.update()) ? true : false);
            break;
        }
    }

    if(dueSheet.update() && isUpdateDone){
        QString notice = "Updated successfully.";
        tflash(notice);
        redirect(urla("viewDueSheet"));
    } else{
        QString error = "Failed to update.";
        texport(error);
        render("dueSheet");
    }
}

//---------------------------------------------------------//

QList<Client> StaffController::findByLocation(const QString location)
{
    QList<Client> clientList = Client::getAll();
    QList<Client> clientByLocationList;
    foreach(Client cl, clientList){
        if(cl.pickLocation() == location){
            clientByLocationList.append(cl);
        }
    }

    return clientByLocationList;
}

int StaffController::getLoggedStaff()
{
    QString login_name = identityKeyOfLoginUser();
    int iden;
    QList<RentalAgent> all_staff = RentalAgent::getAll();
    foreach(RentalAgent ag, all_staff){
        if(ag.email() == login_name){
            iden = ag.agentid();
            break;
        }
    }
    return iden;
}

void StaffController::setAccessRules()
{
    QList<RentalAgent> staffs = RentalAgent::getAll();
    setDenyDefault(true);
    QStringList actions;
    actions << "staff_dash" << "client_listings" << "client_entry" << "remove_client" << "edit_client"
            << "orderListings" << "orderProcessed" << "orderProcessForm" << "viewDueSheet"
            << "activateFine" << "checkIn" << "checkInProcess" << "create_client"
            << "save_client";
    foreach(RentalAgent staff, staffs){
        if(staff.role() == "administrator" || staff.role() == "staff"){
            setAllowUser(identityKeyOfLoginUser(), actions);
        } else{
            setDenyUser(identityKeyOfLoginUser(), actions);
        }
    }
}

bool StaffController::preFilter()
{
    RentalAgent agent = RentalAgent::get(getLoggedStaff());
    if (!isUserLoggedIn()) {
        redirect(url("Account", "staffLoginForm"));
        return false;
    } else if(!validateAccess(&agent)) {
        renderErrorResponse(403);
        return false;
    }

    texport(agent);
    return true;
}


// Don't remove below this line
T_REGISTER_CONTROLLER(staffcontroller)
