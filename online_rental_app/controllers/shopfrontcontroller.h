#ifndef SHOPFRONTCONTROLLER_H
#define SHOPFRONTCONTROLLER_H

#include "applicationcontroller.h"
#include "item.h"
#include "category.h"
#include "client.h"
#include "clientorder.h"


class T_CONTROLLER_EXPORT ShopfrontController : public ApplicationController
{
    Q_OBJECT
public:
    ShopfrontController() { }
    ShopfrontController(const ShopfrontController &other);
    int getLoggedUser();

public slots:
    void index();
    void getByCategory(const QString &category);
    void itemDetails(const QString &id);
    void placeOrder(const QString &movie);

protected:
    bool preFilter();

};

T_DECLARE_CONTROLLER(ShopfrontController, shopfrontcontroller)

#endif // SHOPFRONTCONTROLLER_H
