#include "shopfrontcontroller.h"


ShopfrontController::ShopfrontController(const ShopfrontController &)
    : ApplicationController()
{
    setLayout("shopfront");
}

void ShopfrontController::index()
{
    QList<Category> categoryList = Category::getAll();
    texport(categoryList);

    QList<Item> itemList = Item::getAll();
    texport(itemList);
    render();
}

void ShopfrontController::getByCategory(const QString &category)
{
    QList<Category> categoryList = Category::getAll();
    texport(categoryList);

    QList<Item> itemList = Item::getAll();
    QList<Item> itemByCategoryList;
    foreach(Item i, itemList){
        if(i.category() == category){
            itemByCategoryList.append(i);
        }
    }
    texport(itemByCategoryList);
    render("index");
}

void ShopfrontController::itemDetails(const QString &id)
{
    Item movie = Item::get(id.toInt());
    texport(movie);
    render("details");
}

void ShopfrontController::placeOrder(const QString &movie)
{
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    bool zeroItem = false;
    bool zeroUser = false;

    QList<Item> items = Item::getAll();
    foreach(Item item, items){
        if(item.itemTitle() == movie && item.quantity() <= 0) zeroItem = true;
    }

    Client client = Client::get(getLoggedUser());
    if(client.entitledNumberMovies() <= 0) zeroUser = true;

    if(!zeroItem && !zeroUser){
        ClientOrder clientOrder = ClientOrder::create(
                    movie, client.email(), client.pickLocation(), false);
        if(!clientOrder.isNull()){
            QString notice = "<div class='alert alert-success fade in'><button data-dismiss='alert' class='close close-sm' type='button'><i class='fa fa-times'></i></button>Order was placed successfully</div>";
            tflash(notice);
            redirect(urla("index"));
        } else{
            QString error = "<div class='alert alert-danger fade in'><button data-dismiss='alert' class='close close-sm' type='button'><i class='fa fa-times'></i></button>Order placement failed.</div>";
            texport(error);
            render("details");
        }
    } else if(!zeroItem && zeroUser){
        QString error = "<div class='alert alert-info fade in'><button data-dismiss='alert' class='close close-sm' type='button'><i class='fa fa-times'></i></button>Your <b>entitlement is due</b>, you have to <b>renew your premium</b> at the nearest rental house!!!</div>";
        texport(error);
        render("details");
    } else{
        QString error = "<div class='alert alert-info fade in'><button data-dismiss='alert' class='close close-sm' type='button'><i class='fa fa-times'></i></button>The movie has been <b>rented out</b>. Try again after 24hours.</div>";
        texport(error);
        render("details");
    }
}

int ShopfrontController::getLoggedUser()
{
    QString login_name = identityKeyOfLoginUser();
    int iden;
    QList<Client> clients = Client::getAll();
    foreach (Client client, clients) {
        if(client.email() == login_name){
            iden = client.clientid();
            break;
        }
    }
    return iden;
}

bool ShopfrontController::preFilter()
{
    if (!isUserLoggedIn()) {
        redirect(url("Account", "clientLoginForm"));
        return false;
    }
    Client client = Client::get(getLoggedUser());
    texport(client);
    return true;
}


// Don't remove below this line
T_REGISTER_CONTROLLER(shopfrontcontroller)
