#include <QtCore>
#include <TreeFrogView>
#include "client.h" 
#include "possiblelocations.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT account_clientProfileView : public TActionView
{
  Q_OBJECT
public:
  account_clientProfileView() : TActionView() { }
  account_clientProfileView(const account_clientProfileView &) : TActionView() { }
  QString toString();
};

QString account_clientProfileView::toString()
{
  responsebody.reserve(9481);
  responsebody += tr("<!DOCTYPE html>\n");
      tfetch(Client, user);
  tfetch(PossibleLocations, location);
  responsebody += tr("<html lang=\"en\">\n	<head>\n		<meta charset=\"utf-8\">\n		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\n    		<title>Online Rentals:Shopfront - ");
  responsebody += THttpUtility::htmlEscape(controller()->name());
  responsebody += tr("</title>\n\n		<!-- Bootstrap core CSS -->\n	    	<link href=\"/flatlab/css/bootstrap.min.css\" rel=\"stylesheet\">\n	    	<link href=\"/flatlab/css/bootstrap-reset.css\" rel=\"stylesheet\">\n	    	<!--external css-->\n	    	<link href=\"/flatlab/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />\n	    	<!--right slidebar-->\n	    	<link href=\"/flatlab/css/slidebars.css\" rel=\"stylesheet\">\n	    	<!-- Custom styles for this template -->\n	    	<link href=\"/flatlab/css/style.css\" rel=\"stylesheet\">\n	    	<link href=\"/flatlab/css/style-responsive.css\" rel=\"stylesheet\" />\n	    	<script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.4/mapbox.js'></script>\n		<link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.4/mapbox.css' rel='stylesheet' />\n\n	    	<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->\n	    	<!--[if lt IE 9]>\n	      		<script src=\"/flatlab/js/html5shiv.js\"></script>\n	      		<script src=\"/flatlab/js/respond.min.js\"></script>\n	    	<![endif]-->\n	    	<style type=\"text/css\">\n			#map-canvas {\n				height: 550px;\n				margin: 0;\n				padding: 2px;\n				border: solid 2px #ddd;\n				border-radius: 10px;\n			}\n		</style>\n	</head>\n\n  	<body class=\"full-width\">\n  		<section id=\"container\" class=\"\">\n  			<!--header start-->\n      			<header class=\"header white-bg\">\n          			<!--logo start-->\n          			<a href=\"\" class=\"logo\" >Online<span>Rentals</span></a>\n          			<!--logo end-->\n          			<!--nav start-->\n          			<div class=\"top-nav \">\n              			<ul class=\"nav pull-right top-menu\">\n                  				<li>\n                      				<input type=\"text\" class=\"form-control search\" placeholder=\"Search\">\n                  				</li>\n                  				<!-- user login dropdown start-->\n                  				<li class=\"dropdown\">\n                      				<a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">\n                          				<img alt=\"\" src=\"/flatlab/img/avatar1_small1.jpg\">\n                          				<span class=\"username\">");
  responsebody += THttpUtility::htmlEscape(user.username());
  responsebody += tr("</span>\n                          				<b class=\"caret\"></b>\n                      				</a>\n                      				<ul class=\"dropdown-menu\">\n                          				<div class=\"log-arrow-up\"></div>\n                          				<li>");
  responsebody += QVariant(linkTo("<i class='fa fa-suitcase'></i> Profile", urla("clientProfile"))).toString();
  responsebody += tr("</li>\n                          				<li>");
  responsebody += QVariant(linkTo("<i class='fa fa-sign-out'></i> Logout", urla("clientLogout"))).toString();
  responsebody += tr("</li>\n                      				</ul>\n                  				</li>\n                  				<!-- user login dropdown end -->\n              			</ul>\n          			</div>\n          			<!--nav end-->\n      			</header>\n      			<!--header end-->\n      			\n      			<!--main content start-->\n      			<section id=\"main-content\">\n          			<section class=\"wrapper\">\n              			<!-- page start-->\n              			<div class=\"row\">\n                  				<aside class=\"profile-nav col-lg-3\">\n                      				<section class=\"panel\">\n                          				<div class=\"user-heading round\">\n                              					<a href=\"#\">\n                                  					<img src=\"/flatlab/img/profile-avatar.jpg\" alt=\"\">\n                              					</a>\n                              					<h1>");
  responsebody += THttpUtility::htmlEscape(user.firstname() + " " + user.lastname());
  responsebody += tr("</h1>\n                              					<p>");
  responsebody += THttpUtility::htmlEscape(user.email());
  responsebody += tr("</p>\n                          				</div>\n\n                          				<ul class=\"nav nav-pills nav-stacked\">\n                              					<li class=\"active\"><a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("clientProfile"));
  responsebody += tr("\"> <i class=\"fa fa-user\"></i> Profile</a></li>\n                              					<li><a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("changeProfileForm"));
  responsebody += tr("\"> <i class=\"fa fa-edit\"></i> Edit profile</a></li>\n                              					<li><a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("changePickLocation"));
  responsebody += tr("\"> <i class=\"fa fa-map-marker\"></i> Change Pick Location</a></li>          				\n                          				</ul>\n							</section>\n                  				</aside>\n                  				<aside class=\"profile-info col-lg-9\">\n                      				<section class=\"panel\">\n							      <div class=\"bio-graph-heading\">\n							             Your details all in one place. We guarantee a secure storage of your details and have no intention of using it for any secondary purpose.\n							      </div>\n							      <div class=\"panel-body bio-graph-info\">\n								      <h1>User Details</h1>\n								  	<div class=\"row\">\n										<div class=\"bio-row\">\n											<p><span>First Name </span>: ");
  responsebody += THttpUtility::htmlEscape(user.firstname());
  responsebody += tr("</p>\n										</div>\n										<div class=\"bio-row\">\n											<p><span>Last Name </span>: ");
  responsebody += THttpUtility::htmlEscape(user.lastname());
  responsebody += tr("</p>\n										</div>\n										<div class=\"bio-row\">\n											<p><span>Email </span>:  ");
  responsebody += THttpUtility::htmlEscape(user.email());
  responsebody += tr("</p>\n										</div>\n										<div class=\"bio-row\">\n											<p><span>Pick location </span>:  ");
  responsebody += THttpUtility::htmlEscape(user.pickLocation());
  responsebody += tr("</p>\n										</div>\n										<div class=\"bio-row\">\n											<p><span>Entitlements </span>:  ");
  responsebody += THttpUtility::htmlEscape(user.entitledNumberMovies());
  responsebody += tr("</p>\n										</div>\n										<div class=\"bio-row\">\n											<p><span>Are you active? </span>:  ");
  responsebody += THttpUtility::htmlEscape(user.isActive());
  responsebody += tr("</p>\n										</div>\n									</div>\n								</div>\n							</section>\n							<section>\n                    					<div class=\"panel panel-primary\">\n                        						<div class=\"panel-heading\">Your current pick location</div>\n                        						<div class=\"panel-body\">\n                            						<div id=\"map-canvas\"></div>\n                            						<p id = \"lat\" style=\"display: none;\">");
  responsebody += THttpUtility::htmlEscape(location.latitude());
  responsebody += tr("</p>\n                            						<p id = \"lng\" style=\"display: none;\">");
  responsebody += THttpUtility::htmlEscape(location.longitude());
  responsebody += tr("</p>\n                        						</div>\n                    					</div>\n                					</section>\n                  				</aside>\n              			</div>\n              			<!-- page end-->\n          			</section>\n      			</section>\n      			<!--main content end-->\n\n      			<!--footer start-->\n      			<footer class=\"site-footer\">\n          			<div class=\"text-center\">\n              			&copy;2015 Aseda Designs.\n              			<a href=\"#\" class=\"go-top\">\n                  				<i class=\"fa fa-angle-up\"></i>\n              			</a>\n          			</div>\n      			</footer>\n      			<!--footer end-->\n  		</section>\n\n    		<!-- js placed at the end of the document so the pages load faster -->\n    		<script src=\"/flatlab/js/jquery.js\"></script>\n	    	<script src=\"/flatlab/js/bootstrap.min.js\"></script>\n	    	<script class=\"include\" type=\"text/javascript\" src=\"/flatlab/js/jquery.dcjqaccordion.2.7.js\"></script>\n	    	<script src=\"/flatlab/js/jquery.scrollTo.min.js\"></script>\n	    	<script src=\"/flatlab/js/jquery.nicescroll.js\" type=\"text/javascript\"></script>\n	    	<script src=\"/flatlab/js/respond.min.js\" ></script>\n\n	  	<!--right slidebar-->\n	  	<script src=\"/flatlab/js/slidebars.min.js\"></script>\n\n	    	<!--common script for all pages-->\n	    	<script src=\"/flatlab/js/common-scripts.js\"></script>\n\n	  	<script>\n	  	console.log(document.getElementById(\"lng\").innerHTML);\n			L.mapbox.accessToken = 'pk.eyJ1IjoibmVuZXBhZGkiLCJhIjoiWDl5dk0tYyJ9.A3iSN0zfrpApthOewLcKCw';\n			var map = L.mapbox.map('map-canvas', 'nenepadi.kp6jnh0h').setView([document.getElementById(\"lat\").innerHTML, document.getElementById(\"lng\").innerHTML], 7);\n			var fixedMarker = L.marker(new L.LatLng(document.getElementById(\"lat\").innerHTML, document.getElementById(\"lng\").innerHTML), {\n    				icon: L.mapbox.marker.icon({\n        					'marker-color': '#ff8888',\n        					'marker-symbol': 'cinema'\n    				})\n			}).addTo(map);\n		</script>\n  	</body>\n</html>");

  return responsebody;
}

Q_DECLARE_METATYPE(account_clientProfileView)
T_REGISTER_VIEW(account_clientProfileView)

#include "account_clientProfileView.moc"
