#include <QtCore>
#include <TreeFrogView>
#include "rentalagent.h" 
#include "client.h" 
#include "clientorder.h" 
#include "item.h" 
#include "duesheets.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT admin_reportsView : public TActionView
{
  Q_OBJECT
public:
  admin_reportsView() : TActionView() { }
  admin_reportsView(const admin_reportsView &) : TActionView() { }
  QString toString();
};

QString admin_reportsView::toString()
{
  responsebody.reserve(15821);
            tfetch(QList<RentalAgent>, agents);
  tfetch(QList<Client>, clients);
  tfetch(QList<ClientOrder>, clientorders);
  tfetch(QList<Item>, items);
  tfetch(QList<DueSheets>, duesheets);
  responsebody += tr("\n<script type=\"text/javascript\" src=\"/flatlab/assets/htmltable_export/tableExport.js\"></script>\n<script type=\"text/javascript\" src=\"/flatlab/assets/htmltable_export/jquery.base64.js\"></script>\n<script type=\"text/javascript\" src=\"/flatlab/assets/htmltable_export/html2canvas.js\"></script>\n\n<script type=\"text/javaScript\">\n	$(document).ready(function(){\n		$('.table').dataTable();\n	});\n</script>\n\n<section class=\"wrapper\">\n	<div class=\"row\">\n		<div class=\"col-sm-12\">\n			<section class=\"panel\">\n				<header class=\"panel-heading\">\n					<b>All Clients</b>\n					<span class=\"tools pull-right\"><a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a></span>\n					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n					<div class=\"btn-group pull-right\">\n						<button class=\"btn btn-warning btn-sm dropdown-toggle\" data-toggle=\"dropdown\">\n							<i class=\"fa fa-bars\"></i> Export Table Data\n						</button>\n						<ul class=\"dropdown-menu \" role=\"menu\">\n							<li><a href=\"#\" onClick =\"$('#clients').tableExport({type:'csv',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/csv.png' width='24px'> CSV</a></li>\n							\n							<li class=\"divider\"></li>\n\n							<li><a href=\"#\" onClick =\"$('#clients').tableExport({type:'excel',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/xls.png' width='24px'> XLS</a></li>\n							<li><a href=\"#\" onClick =\"$('#clients').tableExport({type:'powerpoint',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/ppt.png' width='24px'> PowerPoint</a></li>\n\n							<li class=\"divider\"></li>\n\n							<li><a href=\"#\" onClick =\"$('#clients').tableExport({type:'png',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/png.png' width='24px'> PNG</a></li>\n						</ul>\n					</div>\n				</header>\n				<div class=\"panel-body\">\n					<section>\n						<table class=\"table table-bordered table-striped table-condensed\" id=\"clients\">\n							<thead>\n								<tr>\n									<th>First name</th>\n									<th>Last name</th>\n									<th>Username</th>\n									<th>Email</th>\n									<th>Pick location</th>\n									<th>Entitlement</th>\n									<th>Active?</th>\n								</tr>\n							</thead>\n							<tbody>\n								");
  foreach(Client cl, clients){;
  responsebody += tr("									<tr>\n										<td>");
  responsebody += THttpUtility::htmlEscape(cl.firstname());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(cl.lastname());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(cl.username());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(cl.email());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(cl.pickLocation());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(cl.entitledNumberMovies());
  responsebody += tr("</td>\n										");
  if(cl.isActive()){;
  responsebody += tr("		                  							<td class=\"center\">Yes</td>\n		                  						");
  } else{;
  responsebody += tr("		                  							<td class=\"center\">No</td>\n		                  						");
  };
  responsebody += tr("									</tr>\n								");
  };
  responsebody += tr("							</tbody>\n							<tfoot>\n								<tr>\n									<th>First name</th>\n									<th>Last name</th>\n									<th>Username</th>\n									<th>Email</th>\n									<th>Pick location</th>\n									<th>Entitlement</th>\n									<th>Active?</th>\n								</tr>\n							</tfoot>\n						</table>\n					</section>\n				</div>\n			</section>\n\n			<section class=\"panel\">\n				<header class=\"panel-heading\">\n					<b>All Agents</b>\n             			<span class=\"tools pull-right\"><a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a></span>\n             			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n					<div class=\"btn-group pull-right\">\n						<button class=\"btn btn-warning btn-sm dropdown-toggle\" data-toggle=\"dropdown\">\n							<i class=\"fa fa-bars\"></i> Export Table Data\n						</button>\n						<ul class=\"dropdown-menu \" role=\"menu\">\n							<li><a href=\"#\" onClick =\"$('#agents').tableExport({type:'csv',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/csv.png' width='24px'> CSV</a></li>\n							\n							<li class=\"divider\"></li>\n\n							<li><a href=\"#\" onClick =\"$('#agents').tableExport({type:'excel',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/xls.png' width='24px'> XLS</a></li>\n							<li><a href=\"#\" onClick =\"$('#agents').tableExport({type:'powerpoint',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/ppt.png' width='24px'> PowerPoint</a></li>\n\n							<li class=\"divider\"></li>\n\n							<li><a href=\"#\" onClick =\"$('#agents').tableExport({type:'png',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/png.png' width='24px'> PNG</a></li>\n						</ul>\n					</div>\n				</header>\n				<div class=\"panel-body\">\n					<section>\n						<table class=\"table table-bordered table-striped table-condensed\" id=\"agents\">\n							<thead>\n								<tr>\n									<th>First name</th>\n									<th>Last name</th>\n									<th>Email</th>\n									<th>Location</th>\n									<th>Role</th>\n								</tr>\n							</thead>\n							<tbody>\n								");
  foreach(RentalAgent ag, agents){;
  responsebody += tr("									<tr>\n										<td>");
  responsebody += THttpUtility::htmlEscape(ag.firstname());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(ag.lastname());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(ag.email());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(ag.location());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(ag.role());
  responsebody += tr("</td>\n									</tr>\n								");
  };
  responsebody += tr("							</tbody>\n							<tfoot>\n								<tr>\n									<th>First name</th>\n									<th>Last name</th>\n									<th>Email</th>\n									<th>Pick location</th>\n									<th>Role</th>\n								</tr>\n							</tfoot>\n						</table>\n					</section>\n				</div>\n			</section>\n\n			<section class=\"panel\">\n				<header class=\"panel-heading\">\n					<b>All Items</b>\n             			<span class=\"tools pull-right\"><a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a></span>\n             			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n					<div class=\"btn-group pull-right\">\n						<button class=\"btn btn-warning btn-sm dropdown-toggle\" data-toggle=\"dropdown\">\n							<i class=\"fa fa-bars\"></i> Export Table Data\n						</button>\n						<ul class=\"dropdown-menu \" role=\"menu\">\n							<li><a href=\"#\" onClick =\"$('#items').tableExport({type:'csv',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/csv.png' width='24px'> CSV</a></li>\n							\n							<li class=\"divider\"></li>\n\n							<li><a href=\"#\" onClick =\"$('#items').tableExport({type:'excel',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/xls.png' width='24px'> XLS</a></li>\n							<li><a href=\"#\" onClick =\"$('#items').tableExport({type:'powerpoint',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/ppt.png' width='24px'> PowerPoint</a></li>\n\n							<li class=\"divider\"></li>\n\n							<li><a href=\"#\" onClick =\"$('#items').tableExport({type:'png',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/png.png' width='24px'> PNG</a></li>\n						</ul>\n					</div>\n				</header>\n				<div class=\"panel-body\">\n					<section>\n						<table class=\"table table-bordered table-striped table-condensed\" id=\"items\">\n							<thead>\n								<tr>\n									<th>Movie title</th>\n									<th>Category</th>\n									<th>Remaining quantity</th>\n									<th>Updated at</th>\n								</tr>\n							</thead>\n							<tbody>\n								");
  foreach(Item it, items){;
  responsebody += tr("									<tr>\n										<td>");
  responsebody += THttpUtility::htmlEscape(it.itemTitle());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(it.category());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(it.quantity());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(it.updatedAt());
  responsebody += tr("</td>\n									</tr>\n								");
  };
  responsebody += tr("							</tbody>\n							<tfoot>\n								<tr>\n									<th>Movie title</th>\n									<th>Category</th>\n									<th>Remaining quantity</th>\n									<th>Updated at</th>\n								</tr>\n							</tfoot>\n						</table>\n					</section>\n				</div>\n			</section>\n\n			<section class=\"panel\">\n				<header class=\"panel-heading\">\n					<b>All Orders</b>\n             			<span class=\"tools pull-right\"><a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a></span>\n             			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n					<div class=\"btn-group pull-right\">\n						<button class=\"btn btn-warning btn-sm dropdown-toggle\" data-toggle=\"dropdown\">\n							<i class=\"fa fa-bars\"></i> Export Table Data\n						</button>\n						<ul class=\"dropdown-menu \" role=\"menu\">\n							<li><a href=\"#\" onClick =\"$('#orders').tableExport({type:'csv',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/csv.png' width='24px'> CSV</a></li>\n							\n							<li class=\"divider\"></li>\n\n							<li><a href=\"#\" onClick =\"$('#orders').tableExport({type:'excel',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/xls.png' width='24px'> XLS</a></li>\n							<li><a href=\"#\" onClick =\"$('#orders').tableExport({type:'powerpoint',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/ppt.png' width='24px'> PowerPoint</a></li>\n\n							<li class=\"divider\"></li>\n\n							<li><a href=\"#\" onClick =\"$('#orders').tableExport({type:'png',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/png.png' width='24px'> PNG</a></li>\n						</ul>\n					</div>\n				</header>\n				<div class=\"panel-body\">\n					<section>\n						<table class=\"table table-bordered table-striped table-condensed\" id=\"orders\">\n							<thead>\n								<tr>\n									<th>Ordered item</th>\n									<th>Ordered by</th>\n									<th>Location</th>\n									<th>Fullfilled?</th>\n									<th>Time ordered</th>\n									<th>Time fulfilled</th>\n								</tr>\n							</thead>\n							<tbody>\n								");
  foreach(ClientOrder co, clientorders){;
  responsebody += tr("									<tr>\n										<td>");
  responsebody += THttpUtility::htmlEscape(co.movie());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(co.user());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(co.location());
  responsebody += tr("</td>\n										");
  if(co.fulfilled()){;
  responsebody += tr("		                  							<td>Yes</td>\n		                  						");
  } else{;
  responsebody += tr("		                  							<td>No</td>\n		                  						");
  };
  responsebody += tr("		                  						<td>");
  responsebody += THttpUtility::htmlEscape(co.createdAt());
  responsebody += tr("</td>\n		                  						<td>");
  responsebody += THttpUtility::htmlEscape(co.updatedAt());
  responsebody += tr("</td>\n									</tr>\n								");
  };
  responsebody += tr("							</tbody>\n							<tfoot>\n								<tr>\n									<th>Ordered item</th>\n									<th>Ordered by</th>\n									<th>Location</th>\n									<th>Fullfilled?</th>\n									<th>Time ordered</th>\n									<th>Time fulfilled</th>\n								</tr>\n							</tfoot>\n						</table>\n					</section>\n				</div>\n			</section>\n\n			<section class=\"panel\">\n				<header class=\"panel-heading\">\n					<b>All Item Due for Collection</b>\n             			<span class=\"tools pull-right\"><a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a></span>\n             			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n					<div class=\"btn-group pull-right\">\n						<button class=\"btn btn-warning btn-sm dropdown-toggle\" data-toggle=\"dropdown\">\n							<i class=\"fa fa-bars\"></i> Export Table Data\n						</button>\n						<ul class=\"dropdown-menu \" role=\"menu\">\n							<li><a href=\"#\" onClick =\"$('#dues').tableExport({type:'csv',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/csv.png' width='24px'> CSV</a></li>\n							\n							<li class=\"divider\"></li>\n\n							<li><a href=\"#\" onClick =\"$('#dues').tableExport({type:'excel',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/xls.png' width='24px'> XLS</a></li>\n							<li><a href=\"#\" onClick =\"$('#dues').tableExport({type:'powerpoint',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/ppt.png' width='24px'> PowerPoint</a></li>\n\n							<li class=\"divider\"></li>\n\n							<li><a href=\"#\" onClick =\"$('#dues').tableExport({type:'png',escape:'false'});\"> <img src='/flatlab/assets/htmltable_export/icons/png.png' width='24px'> PNG</a></li>\n						</ul>\n					</div>\n				</header>\n				<div class=\"panel-body\">\n					<section>\n						<table class=\"table table-bordered table-striped table-condensed\" id=\"dues\">\n							<thead>\n								<tr>\n									<th>Ordered item</th>\n									<th>Ordered by</th>\n									<th>Processed date</th>\n									<th>Processor</th>\n									<th>Due date</th>\n									<th>Checkin?</th>									\n									<th>Last fine activate</th>\n								</tr>\n							</thead>\n							<tbody>\n								");
  foreach(DueSheets ds, duesheets){;
  responsebody += tr("									<tr>\n										<td>");
  responsebody += THttpUtility::htmlEscape(ds.orderedItem());
  responsebody += tr("</td>\n										<td>");
  responsebody += THttpUtility::htmlEscape(ds.orderedBy());
  responsebody += tr("</td>\n		                  						<td>");
  responsebody += THttpUtility::htmlEscape(ds.processedDate());
  responsebody += tr("</td>\n		                  						<td>");
  responsebody += THttpUtility::htmlEscape(ds.processor());
  responsebody += tr("</td>\n		                  						<td>");
  responsebody += THttpUtility::htmlEscape(ds.dueDate());
  responsebody += tr("</td>\n		                  						");
  if(ds.checkIn()){;
  responsebody += tr("		                  							<td>Yes</td>\n		                  						");
  } else{;
  responsebody += tr("		                  							<td>No</td>\n		                  						");
  };
  responsebody += tr("		                  						");
  if( ds.dayFineLastActivate() != " "){;
  responsebody += tr("		                  							<td>");
  responsebody += THttpUtility::htmlEscape(ds.dayFineLastActivate());
  responsebody += tr("</td>\n		                  						");
  } else{;
  responsebody += tr("		                  							<td>N/A</td>\n		                  						");
  };
  responsebody += tr("									</tr>\n								");
  };
  responsebody += tr("							</tbody>\n							<tfoot>\n								<tr>\n									<th>Ordered item</th>\n									<th>Ordered by</th>\n									<th>Processed date</th>\n									<th>Processor</th>\n									<th>Due date</th>\n									<th>Checkin?</th>									\n									<th>Last fine activate</th>\n								</tr>\n							</tfoot>\n						</table>\n					</section>\n				</div>\n			</section>\n		</div>\n	</div>\n</section>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(admin_reportsView)
T_REGISTER_VIEW(admin_reportsView)

#include "admin_reportsView.moc"
