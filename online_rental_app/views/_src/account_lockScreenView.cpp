#include <QtCore>
#include <TreeFrogView>
#include "rentalagent.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT account_lockScreenView : public TActionView
{
  Q_OBJECT
public:
  account_lockScreenView() : TActionView() { }
  account_lockScreenView(const account_lockScreenView &) : TActionView() { }
  QString toString();
};

QString account_lockScreenView::toString()
{
  responsebody.reserve(2951);
  responsebody += tr("<!DOCTYPE html>\n");
    tfetch(RentalAgent, staff);
  responsebody += tr("<html lang=\"en\">\n    	<head>\n        	<meta charset=\"utf-8\">\n        	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\n        	<title>Lock Screen</title>\n\n        	<!-- Bootstrap core CSS -->\n        	<link href=\"/flatlab/css/bootstrap.min.css\" rel=\"stylesheet\">\n        	<link href=\"/flatlab/css/bootstrap-reset.css\" rel=\"stylesheet\">\n        	<!--external css-->\n        	<link href=\"/flatlab/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />\n        	<!-- Custom styles for this template -->\n        	<link href=\"/flatlab/css/style.css\" rel=\"stylesheet\">\n        	<link href=\"/flatlab/css/style-responsive.css\" rel=\"stylesheet\" />\n\n        	<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->\n        	<!--[if lt IE 9]>\n        		<script src=\"js/html5shiv.js\"></script>\n        		<script src=\"js/respond.min.js\"></script>\n        	<![endif]-->\n    	</head>\n\n    	<body class=\"lock-screen\" onload=\"startTime()\">\n        	<div class=\"lock-wrapper\">\n            		<div id=\"time\"></div>\n            		<div class=\"lock-box text-center\">\n                		<img src=\"/flatlab/img/follower-avatar.jpg\" alt=\"lock avatar\"/>\n                		<h1>");
  responsebody += THttpUtility::htmlEscape(staff.firstname() + " " + staff.lastname());
  responsebody += tr("</h1>\n                		<span class=\"locked\">Locked</span>\n                		");
  responsebody += QVariant(formTag(urla("lock_screen"), Tf::Post, "", a("class", "form-inline"))).toString();
  responsebody += tr("\n                    		<div class=\"form-group col-lg-12\">\n                        			<input type=\"password\" id=\"password\" name=\"password\" value=\"\" placeholder=\"Password\" class=\"form-control lock-input\">\n                        			<button class=\"btn btn-lock\" type=\"submit\">\n                            			<i class=\"fa fa-arrow-right\"></i>\n                        			</button>\n                    		</div>\n                		</form>\n            		</div>\n        	</div>\n        	\n        	<script>\n            		function startTime(){\n                		var today=new Date();\n                		var h=today.getHours();\n                		var m=today.getMinutes();\n                		var s=today.getSeconds();\n                		// add a zero in front of numbers<10\n                		m=checkTime(m);\n                		s=checkTime(s);\n                		document.getElementById('time').innerHTML=h+\":\"+m+\":\"+s;\n                		t=setTimeout(function(){startTime()},500);\n            		}\n\n            		function checkTime(i){\n                		if (i<10){\n                    		i=\"0\" + i;\n                		}\n                		return i;\n            		}\n        	</script>\n    	</body>\n</html>");

  return responsebody;
}

Q_DECLARE_METATYPE(account_lockScreenView)
T_REGISTER_VIEW(account_lockScreenView)

#include "account_lockScreenView.moc"
