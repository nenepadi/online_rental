#include <QtCore>
#include <TreeFrogView>
#include "rentalagent.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT admin_newStaffView : public TActionView
{
  Q_OBJECT
public:
  admin_newStaffView() : TActionView() { }
  admin_newStaffView(const admin_newStaffView &) : TActionView() { }
  QString toString();
};

QString admin_newStaffView::toString()
{
  responsebody.reserve(8252);
    tfetch(QStringList, locations);
  tfetch(QVariantMap, agent);
  responsebody += tr("<section class=\"wrapper\">\n    	<!-- page start-->\n    	<div class=\"row\">\n      		<div class=\"col-lg-12\">\n            		<section class=\"panel\">\n                		<header class=\"panel-heading\">\n	                		");
  if(controller()->activeAction() == "staff_entry"){;
  responsebody += tr("	                    		<b>Employ new staff</b>\n	                    	");
  } else{;
  responsebody += tr("	                    		<b>Edit staff #");
  responsebody += QVariant(agent["agentid"]).toString();
  responsebody += tr(" records</b>\n	                    	");
  };
  responsebody += tr("                    		<span class=\"tools pull-right\">\n                        			<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n                        			<a href=\"javascript:;\" class=\"fa fa-times\"></a>\n                    		</span>\n                		</header>\n                		<div class=\"panel-body\">\n                    		<div>");
  techoex(error);
  responsebody += tr("</div>\n                    		<div>");
  techoex(notice);
  responsebody += tr("</div>\n                    		<div class=\"form\">\n                    			");
  if(controller()->activeAction() == "staff_entry"){;
  responsebody += tr("                        				");
  responsebody += QVariant(formTag(urla("create_staff"), Tf::Post, "", a("class", "cmxform form-horizontal tasi-form employStaff"))).toString();
  responsebody += tr("\n                        			");
  } else{;
  responsebody += tr("                        				");
  responsebody += QVariant(formTag(urla("save_staff", agent["agentid"]), Tf::Post, "", a("class", "cmxform form-horizontal tasi-form employStaff"))).toString();
  responsebody += tr("\n                        			");
  };
  responsebody += tr("                            			<div class=\"form-group \">\n                                			<label for=\"firstname\" class=\"control-label col-lg-2\">First name</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"firstname\" name=\"agent[firstname]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(agent["firstname"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"lastname\" class=\"control-label col-lg-2\">Last name</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"lastname\" name=\"agent[lastname]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(agent["lastname"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"email\" class=\"control-label col-lg-2\">Email</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"email\" name=\"agent[email]\" type=\"email\" value=\"");
  responsebody += THttpUtility::htmlEscape(agent["email"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"password\" class=\"control-label col-lg-2\">Password</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"password\" name=\"agent[password]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(agent["password"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"location\" class=\"control-label col-lg-2\">Location</label>\n                                			<div class=\"col-lg-10\">\n	                                      			<select class=\"form-control m-bot15\" onChange=\"OnDropDownChage(this);\">\n	                                          			<option value = \"\" disabled>Select a location</option>\n	                                              		");
  for (int i = 0; i < locations.size(); ++i){;
  responsebody += tr("	                                              			");
  if(controller()->activeAction() == "staff_entry"){;
  responsebody += tr("	                                                				<option value = \"");
  responsebody += THttpUtility::htmlEscape(locations.at(i));
  responsebody += tr("\">");
  responsebody += THttpUtility::htmlEscape(locations.at(i));
  responsebody += tr("</option>\n	                                                			");
  } else{;
  responsebody += tr("	                                                				<option value = \"");
  responsebody += THttpUtility::htmlEscape(locations.at(i));
  responsebody += tr("\" \n	                                                				");
  if(agent["location"] == locations.at(i)){;
  responsebody += tr("	                                                					selected = \"selected\"\n	                                                				");
  };
  responsebody += tr("	                                                				>");
  responsebody += THttpUtility::htmlEscape(locations.at(i));
  responsebody += tr("</option>\n	                                                			");
  };
  responsebody += tr("	                                              		");
  };
  responsebody += tr("	                                            		</select>\n                                    				<input class=\" form-control\" id=\"location\" name=\"agent[location]\" type=\"hidden\" value=\"");
  responsebody += THttpUtility::htmlEscape(agent["location"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"role\" class=\"control-label col-lg-2\">Role</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"role\" name=\"agent[role]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(agent["role"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group\">\n                                			<div class=\"col-lg-offset-2 col-lg-10\">\n                                    				<button class=\"btn btn-danger\" type=\"submit\">\n                                    					");
  if(controller()->activeAction() == "staff_entry"){;
  responsebody += tr("                                    						Save\n                                    					");
  } else{;
  responsebody += tr("                                    						Update\n                                    					");
  };
  responsebody += tr("                                    				</button>\n                                    				<a class=\"btn btn-default\" type=\"button\" href=\"/Admin/all_staff\">Cancel</a>\n                                			</div>\n                            			</div>\n                        			</form>\n                    		</div>\n                		</div>\n            		</section>\n      		</div>\n    	</div>\n    	<!-- page end-->\n</section>\n\n<script src=\"/flatlab/js/jquery-ui-1.9.2.custom.min.js\"></script>\n<script src=\"/flatlab/js/form-validation-script.js\"></script>\n<script src=\"/flatlab/js/advanced-form-components.js\"></script>\n\n<script>\n	function OnDropDownChage(dropDown){\n		var selectedValue = dropDown.options[dropDown.selectedIndex].value;\n		document.getElementById(\"location\").value = selectedValue;\n	} \n</script>");

  return responsebody;
}

Q_DECLARE_METATYPE(admin_newStaffView)
T_REGISTER_VIEW(admin_newStaffView)

#include "admin_newStaffView.moc"
