#include <QtCore>
#include <TreeFrogView>
#include "item.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT admin_allItemView : public TActionView
{
  Q_OBJECT
public:
  admin_allItemView() : TActionView() { }
  admin_allItemView(const admin_allItemView &) : TActionView() { }
  QString toString();
};

QString admin_allItemView::toString()
{
  responsebody.reserve(3541);
    tfetch(QList<Item>, itemList);
  responsebody += tr("<section class=\"wrapper\">\n    	<!-- page start-->\n    	<div class=\"row\">\n       	<div class=\"col-sm-12\">\n            		<section class=\"panel\">\n              		<header class=\"panel-heading\">\n              			<b>All Items</b>\n              			<span class=\"tools pull-right\">\n                   				<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("item_entry"));
  responsebody += tr("\" id=\"create\" class=\"btn btn-shadow btn-success btn-xs\" data-toggle=\"tooltip\" title=\"Create new item\"><i class='fa fa-plus'></i></a>\n              				<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n              				<a href=\"javascript:;\" class=\"fa fa-times\"></a>\n            				</span>\n        			</header>\n              		<div class=\"panel-body\">\n              			<div class=\"adv-table\">\n              				<table  class=\"display table table-bordered table-striped\" id=\"dynamic-table\">\n              					<thead>\n              						<tr>\n              							<th>Title</th>\n									<th>Category</th>\n									<th>Year of Production</th>\n									<th  class=\"hidden-phone\">Copies In-Stock</th>\n									<th  class=\"hidden-phone\">Starring</th>\n                  							<th></th>\n              						</tr>\n              					</thead>\n              					<tbody>\n              						");
  foreach(Item i, itemList){;
  responsebody += tr("	              					<tr class=\"gradeA\">\n	                  						<td>");
  responsebody += THttpUtility::htmlEscape(i.itemTitle());
  responsebody += tr("</td>\n	                  						<td>");
  responsebody += THttpUtility::htmlEscape(i.category());
  responsebody += tr("</td>\n	                  						<td>");
  responsebody += THttpUtility::htmlEscape(i.yearOfProduction());
  responsebody += tr("</td>\n	                  						<td class=\"center hidden-phone\">");
  responsebody += THttpUtility::htmlEscape(i.quantity());
  responsebody += tr("</td>\n	                  						<td class=\"center hidden-phone\">");
  responsebody += THttpUtility::htmlEscape(i.leadActor());
  responsebody += tr(" &amp; ");
  responsebody += THttpUtility::htmlEscape(i.leadActress());
  responsebody += tr("</td>\n	                  						<td>\n	                  							<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("edit_item", i.id()));
  responsebody += tr("\" id=\"edit\" class=\"btn btn-primary btn-xs\" data-toggle=\"tooltip\" title=\"Edit item #");
  responsebody += THttpUtility::htmlEscape(i.id());
  responsebody += tr("\"><i class='fa fa-pencil'></i></a>\n										");
  responsebody += QVariant(linkTo("<i class='fa fa-trash-o'></i>", urla("remove_item", i.id()), Tf::Post, "confirm('Are you sure you want to delete?')", a("class", "btn btn-danger btn-xs"))).toString();
  responsebody += tr("\n									</td>\n	              					</tr>\n              						");
  };
  responsebody += tr("              					</tbody>\n              					<tfoot>\n              						<tr>\n                  							<th>Title</th>\n									<th>Category</th>\n									<th>Year of Production</th>\n									<th  class=\"hidden-phone\">Copies In-Stock</th>\n									<th  class=\"hidden-phone\">Starring</th>\n                  							<th></th>\n              						</tr>\n              					</tfoot>\n              				</table>\n              			</div>\n              		</div>\n            		</section>\n        	</div>\n    	</div>\n    	<!-- page end-->\n</section>");

  return responsebody;
}

Q_DECLARE_METATYPE(admin_allItemView)
T_REGISTER_VIEW(admin_allItemView)

#include "admin_allItemView.moc"
