#include <QtCore>
#include <TreeFrogView>
#include "item.h" 
#include "category.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT admin_newItemView : public TActionView
{
  Q_OBJECT
public:
  admin_newItemView() : TActionView() { }
  admin_newItemView(const admin_newItemView &) : TActionView() { }
  QString toString();
};

QString admin_newItemView::toString()
{
  responsebody.reserve(8719);
      tfetch(QVariantMap, item);
  tfetch(QList<Category>, catList);
  responsebody += tr("<section class=\"wrapper\">\n    	<!-- page start-->\n    	<div class=\"row\">\n        	<div class=\"col-lg-12\">\n            		<section class=\"panel\">\n                		<header class=\"panel-heading\">\n                			");
  if(controller()->activeAction() == "item_entry"){;
  responsebody += tr("                				<b>Add items</b>\n                			");
  } else{;
  responsebody += tr("                				<b>Edit Item #");
  responsebody += QVariant(item["id"]).toString();
  responsebody += tr("</b>\n                			");
  };
  responsebody += tr("                		</header>\n                		<div class=\"panel-body\">\n                			<div>");
  techoex(error);
  responsebody += tr("</div>\n                    		<div class=\"form\">\n                    			");
  if(controller()->activeAction() == "item_entry"){;
  responsebody += tr("                					");
  responsebody += QVariant(formTag(urla("create_item"), Tf::Post, "", a("class", "cmxform form-horizontal tasi-form createItem"))).toString();
  responsebody += tr("\n                				");
  } else{;
  responsebody += tr("                					");
  responsebody += QVariant(formTag(urla("save_item", item["id"]), Tf::Post, "", a("class", "cmxform form-horizontal tasi-form createItem"))).toString();
  responsebody += tr("\n                				");
  };
  responsebody += tr("                            			<div class=\"form-group \">\n                                			<label for=\"itemtitle\" class=\"control-label col-lg-2\">Item title</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"itemtitle\" name=\"item[itemTitle]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(item["itemTitle"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"itemcategory\" class=\"control-label col-lg-2\">Category</label>\n                                			<div class=\"col-lg-10\">\n                                				<select class=\"form-control m-bot15\" onChange=\"OnDropDownChage(this);\">\n                                					<option value = \"\" disabled>Select a category</option>\n                                    					");
  foreach(Category cat, catList){;
  responsebody += tr("                                    						");
  if(controller()->activeAction() == "item_entry"){;
  responsebody += tr("                                    							<option value = \"");
  responsebody += THttpUtility::htmlEscape(cat.categoryName());
  responsebody += tr("\">");
  responsebody += THttpUtility::htmlEscape(cat.categoryName());
  responsebody += tr("</option>\n                                    						");
  } else{;
  responsebody += tr("                                    							<option value = \"");
  responsebody += THttpUtility::htmlEscape(cat.categoryName());
  responsebody += tr("\" \n                                    							");
  if(item["category"] == cat.categoryName()){;
  responsebody += tr("	                                                					selected = \"selected\"\n	                                                				");
  };
  responsebody += tr("                              								>");
  responsebody += THttpUtility::htmlEscape(cat.categoryName());
  responsebody += tr("</option>\n                              							");
  };
  responsebody += tr("                                    					");
  };
  responsebody += tr("                                    				</select>\n                                    				<input class=\" form-control\" id=\"selCat\" name=\"item[category]\" type=\"hidden\" value=\"");
  responsebody += THttpUtility::htmlEscape(item["category"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"itemyear\" class=\"control-label col-lg-2\">Year of Production</label>\n                                			<div class=\"col-lg-8 input-append date dpMonths\">\n                                          			<input type=\"text\" class=\"form-control\" id=\"itemyear\" name=\"item[yearOfProduction]\" value=\"");
  responsebody += THttpUtility::htmlEscape(item["yearOfProduction"]);
  responsebody += tr("\" required/>\n                                              		<span class=\"input-group-btn add-on\">\n                                                			<button class=\"btn btn-danger\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>\n                                              		</span>\n                                    			</div>\n                                		</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"itemqty\" class=\"control-label col-lg-2\">Quantity</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"itemqty\" name=\"item[quantity]\" type=\"number\" value=\"");
  responsebody += THttpUtility::htmlEscape(item["quantity"]);
  responsebody += tr("\" />\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"description\" class=\"control-label col-lg-2\">Description</label>\n                                			<div class=\"col-lg-10\">\n                                    				<textarea class=\"form-control\" rows=\"2\" name=\"item[description]\" id=\"description\">");
  responsebody += THttpUtility::htmlEscape(item["description"]);
  responsebody += tr("</textarea>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"leadactor\" class=\"control-label col-lg-2\">Lead actor</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"leadactor\" name=\"item[leadActor]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(item["leadActor"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"leadactress\" class=\"control-label col-lg-2\">Lead actress</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"leadactress\" name=\"item[leadActress]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(item["leadActress"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group\">\n                                			<div class=\"col-lg-offset-2 col-lg-10\">\n                                    				<button class=\"btn btn-danger\" type=\"submit\">\n                                    					");
  if(controller()->activeAction() == "item_entry"){;
  responsebody += tr("                                    						Save\n                                    					");
  } else{;
  responsebody += tr("                                    						Update\n                                    					");
  };
  responsebody += tr("                                    				</button>\n                                    				<a class=\"btn btn-default\" type=\"button\" href=\"/Admin/all_items\">Cancel</a>\n                                			</div>\n                            			</div>\n                        			</form>\n                    		</div>\n                		</div>\n            		</section>\n        	</div>\n    	</div>\n    	<!-- page end-->\n</section>\n\n<script src=\"/flatlab/js/form-validation-script.js\"></script>\n<script type=\"text/javascript\" src=\"/flatlab/assets/bootstrap-datepicker/js/bootstrap-datepicker.js\"></script>\n<script src=\"/flatlab/js/advanced-form-components.js\"></script>\n\n<script>\n	function OnDropDownChage(dropDown){\n		var selectedValue = dropDown.options[dropDown.selectedIndex].value;\n		document.getElementById(\"selCat\").value = selectedValue;\n	} \n</script>");

  return responsebody;
}

Q_DECLARE_METATYPE(admin_newItemView)
T_REGISTER_VIEW(admin_newItemView)

#include "admin_newItemView.moc"
