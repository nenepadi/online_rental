#include <QtCore>
#include <TreeFrogView>
#include "rentalagent.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT admin_allStaffView : public TActionView
{
  Q_OBJECT
public:
  admin_allStaffView() : TActionView() { }
  admin_allStaffView(const admin_allStaffView &) : TActionView() { }
  QString toString();
};

QString admin_allStaffView::toString()
{
  responsebody.reserve(3199);
    tfetch(QList<RentalAgent>, staffList);
  responsebody += tr("<section class=\"wrapper\">\n	<!-- page start-->\n	<div class=\"row\">\n       	<div class=\"col-sm-12\">\n       		<section class=\"panel\">\n              		<header class=\"panel-heading\">\n              			<b>List of staff</b>\n              			<span class=\"tools pull-right\">\n              				<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("staff_entry"));
  responsebody += tr("\" id=\"create\" class=\"btn btn-shadow btn-success btn-xs\" data-toggle=\"tooltip\" title=\"Create new staff\"><i class='fa fa-plus'></i></a>\n                				<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n                				<a href=\"javascript:;\" class=\"fa fa-times\"></a>\n                			</span>\n              		</header>\n              		<div class=\"panel-body\">\n              			<div class=\"adv-table\">\n              				<table  class=\"display table table-bordered table-striped\" id=\"dynamic-table\">\n              					<thead>\n              						<tr>\n              							<th>Name</th>\n									<th>Email</th>\n									<th>Location</th>\n									<th  class=\"hidden-phone\">Role</th>\n                  							<th></th>\n              						</tr>\n              					</thead>\n              					<tbody>\n              						");
  foreach(RentalAgent ra, staffList){;
  responsebody += tr("	              					<tr class=\"gradeA\">\n	                  						<td>");
  responsebody += THttpUtility::htmlEscape(ra.firstname() + " " + ra.lastname());
  responsebody += tr("</td>\n	                  						<td>");
  responsebody += THttpUtility::htmlEscape(ra.email());
  responsebody += tr("</td>\n	                  						<td>");
  responsebody += THttpUtility::htmlEscape(ra.location());
  responsebody += tr("</td>\n	                  						<td class=\"center hidden-phone\">");
  responsebody += THttpUtility::htmlEscape(ra.role());
  responsebody += tr("</td>\n	                  						<td>\n	                  							<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("edit_staff", ra.agentid()));
  responsebody += tr("\" id=\"edit\" class=\"btn btn-primary btn-xs\" data-toggle=\"tooltip\" title=\"Edit staff #");
  responsebody += THttpUtility::htmlEscape(ra.agentid());
  responsebody += tr("\"><i class='fa fa-pencil'></i></a>\n										");
  responsebody += QVariant(linkTo("<i class='fa fa-trash-o'></i>", urla("remove_staff", ra.agentid()), Tf::Post, "confirm('Are you sure you want to delete?')", a("class", "delete btn btn-danger btn-xs"))).toString();
  responsebody += tr("\n									</td>\n	              					</tr>\n              						");
  };
  responsebody += tr("              					</tbody>\n              					<tfoot>\n              						<tr>\n                  							<th>Name</th>\n									<th>Email</th>\n									<th>Location</th>\n									<th  class=\"hidden-phone\">Role</th>\n                  							<th></th>\n              						</tr>\n              					</tfoot>\n              				</table>\n              			</div>\n              		</div>\n              	</section>\n        	</div>\n    	</div>\n    	<!-- page end-->\n</section>");

  return responsebody;
}

Q_DECLARE_METATYPE(admin_allStaffView)
T_REGISTER_VIEW(admin_allStaffView)

#include "admin_allStaffView.moc"
