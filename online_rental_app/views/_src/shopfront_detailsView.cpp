#include <QtCore>
#include <TreeFrogView>
#include "item.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT shopfront_detailsView : public TActionView
{
  Q_OBJECT
public:
  shopfront_detailsView() : TActionView() { }
  shopfront_detailsView(const shopfront_detailsView &) : TActionView() { }
  QString toString();
};

QString shopfront_detailsView::toString()
{
  responsebody.reserve(4863);
    tfetch(Item, movie);
  responsebody += tr("<section class=\"panel\">\n    <div class=\"panel-body\">\n        <div class=\"col-md-6\">\n            <div class=\"pro-img-details\">\n                <img src=\"http://placehold.it/370x372\" alt=\"\"/>\n            </div>\n        </div>\n        <div class=\"col-md-6\">\n            <h4 class=\"pro-d-title\">\n                <a href=\"#\" class=\"\">\n                    ");
  responsebody += THttpUtility::htmlEscape(movie.itemTitle());
  responsebody += tr("\n                </a>\n            </h4>\n            <p>\n                ");
  responsebody += THttpUtility::htmlEscape(movie.description());
  responsebody += tr("\n            </p>\n            <div class=\"product_meta\">\n                <span class=\"posted_in\"> <strong>Category:</strong> <a rel=\"tag\" href=\"/Shopfront/getByCategory/");
  responsebody += THttpUtility::htmlEscape(movie.category());
  responsebody += tr("\">");
  responsebody += THttpUtility::htmlEscape(movie.category());
  responsebody += tr("</a>.</span>\n                <span class=\"tagged_as\"><strong>Lead cast:</strong> <a rel=\"tag\" href=\"#\">");
  responsebody += THttpUtility::htmlEscape(movie.leadActor());
  responsebody += tr("</a>, <a rel=\"tag\" href=\"#\">");
  movie.leadActress();
  responsebody += tr("</a>.</span>\n            </div>\n            <div class=\"m-bot15\"> <strong>Year : </strong><span class=\"pro-price\">");
  responsebody += THttpUtility::htmlEscape(movie.yearOfProduction());
  responsebody += tr("</span></div>\n            <p>\n				");
  if(movie.quantity() > 0){;
  responsebody += tr("					");
  responsebody += QVariant(linkTo("<i class='fa fa-shopping-cart'></i> Rent now", urla("placeOrder", movie.itemTitle()), Tf::Post, "confirm('Do you want to place an order for this movie?')", a("class", "btn btn-round btn-danger"))).toString();
  responsebody += tr("\n				");
  } else{;
  responsebody += tr("					<a href = \"#\" class = \"btn btn-round btn-danger disabled\">Rented out</a>\n				");
  };
  responsebody += tr("            </p>\n            <div>");
  techoex(error);
  responsebody += tr("</div>\n        </div>\n    </div>\n</section>\n\n<section class=\"panel\">\n    <header class=\"panel-heading tab-bg-dark-navy-blue\">\n        <ul class=\"nav nav-tabs \">\n            <li class=\"active\">\n                <a data-toggle=\"tab\" href=\"#description\">\n                    Description\n                </a>\n            </li>\n            <li>\n                <a data-toggle=\"tab\" href=\"#reviews\">\n                    Reviews\n                </a>\n            </li>\n        </ul>\n    </header>\n    <div class=\"panel-body\">\n        <div class=\"tab-content tasi-tab\">\n            <div id=\"description\" class=\"tab-pane active\">\n                <h4 class=\"pro-d-head\">Product Description</h4>\n                <p> Praesent ac condimentum felis. Nulla at nisl orci, at dignissim dolor, The best product descriptions address your ideal buyer directly and personally. The best product descriptions address your ideal buyer directly and personally. </p>\n                <p> Praesent ac condimentum felis. Nulla at nisl orci, at dignissim dolor, The best product descriptions address your ideal buyer directly and personally. The best product descriptions address your ideal buyer directly and personally. The best product descriptions address your ideal buyer directly and personally. The best product descriptions address your ideal buyer directly and personally. </p>\n            </div>\n            <div id=\"reviews\" class=\"tab-pane\">\n                <article class=\"media\">\n                    <a class=\"pull-left thumb p-thumb\">\n                        <img src=\"img/avatar-mini.jpg\">\n                    </a>\n                    <div class=\"media-body\">\n                        <a href=\"#\" class=\"cmt-head\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a>\n                        <p> <i class=\"fa fa-time\"></i> 1 hours ago</p>\n                    </div>\n                </article>\n            </div>\n        </div>\n    </div>\n</section>\n\n<div class=\"row product-list\">\n    <div class=\"col-md-4\">\n        <section class=\"panel\">\n            <div class=\"pro-img-box\">\n                <img src=\"http://placehold.it/279x222\" alt=\"\"/>\n                <a href=\"#\" class=\"adtocart\">\n                	<i class=\"fa fa-shopping-cart\"></i>\n                </a>\n            </div>\n            <div class=\"panel-body text-center\">\n                <h4>\n                    <a href=\"#\" class=\"pro-title\">\n                        Leopard Shirt Dress\n                    </a>\n                </h4>\n                <p class=\"price\">$300.00</p>\n            </div>\n        </section>\n    </div>\n</div>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(shopfront_detailsView)
T_REGISTER_VIEW(shopfront_detailsView)

#include "shopfront_detailsView.moc"
