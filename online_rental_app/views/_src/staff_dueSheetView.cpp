#include <QtCore>
#include <TreeFrogView>
#include "duesheets.h" 
#include "QDate" 
#include "applicationhelper.h"

class T_VIEW_EXPORT staff_dueSheetView : public TActionView
{
  Q_OBJECT
public:
  staff_dueSheetView() : TActionView() { }
  staff_dueSheetView(const staff_dueSheetView &) : TActionView() { }
  QString toString();
};

QString staff_dueSheetView::toString()
{
  responsebody.reserve(8752);
      tfetch(QList<DueSheets>, dues);
  tfetch(QList<DueSheets>, cdues);
  tfetch(DueSheets, dueSheet);
  responsebody += tr("<section class=\"wrapper site-min-height\">\n    <!-- page start-->\n    <div class=\"row\">\n		");
  if(controller()->activeAction() == "viewDueSheet"){;
  responsebody += tr("			<div class=\"col-sm-12\">\n				<section class=\"panel\">\n					<header class=\"panel-heading\">\n						Today's Due items\n						<span class=\"tools pull-right\">\n							<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n						</span>\n					</header>\n					<div class=\"panel-body\">\n						<div class=\"adv-table\">\n							<table  class=\"display table table-bordered table-striped\" id=\"dynamic-table\">\n								<thead>\n									<tr>\n										<th>Due item</th>\n										<th>Ordered by</th>\n										<th>Processed date</th>\n										<th>Was due</th>\n										<th></th>\n									</tr>\n								</thead>\n								<tbody>\n									");
  foreach(DueSheets ds, dues){;
  responsebody += tr("										<tr class=\"gradeA\">\n											<td>");
  responsebody += THttpUtility::htmlEscape(ds.orderedItem());
  responsebody += tr("</td>\n											<td>");
  responsebody += THttpUtility::htmlEscape(ds.orderedBy());
  responsebody += tr("</td>\n											<td>");
  responsebody += THttpUtility::htmlEscape(ds.processedDate());
  responsebody += tr("</td>\n											");
  if(QDate::fromString(ds.dueDate(), "yyyy-MM-dd") == QDate::currentDate()){;
  responsebody += tr("												<td>today</td>\n											");
  } else{;
  responsebody += tr("												");
  if(QDate::fromString(ds.dueDate(), "yyyy-MM-dd").daysTo(QDate::currentDate()) == 1){;
  responsebody += tr("													<td>a day ago</td>\n												");
  } else{;
  responsebody += tr("													<td>");
  responsebody += THttpUtility::htmlEscape(QDate::fromString(ds.dueDate(), "yyyy-MM-dd").daysTo(QDate::currentDate()));
  responsebody += tr("days ago</td>\n												");
  };
  responsebody += tr("											");
  };
  responsebody += tr("											<td>\n												");
  if(QDate::fromString(ds.dueDate(), "yyyy-MM-dd") == QDate::currentDate()){;
  responsebody += tr("													<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("activateFine", ds.id()));
  responsebody += tr("\" class=\"btn btn-primary btn-xs disabled\" data-toggle=\"tooltip\" title=\"Activate fine\"><i class='fa fa-cogs'></i></a>\n												");
  } else{;
  responsebody += tr("													");
  if(QDate::fromString(ds.dayFineLastActivate(), "yyyy-MM-dd") == QDate::currentDate()){;
  responsebody += tr("														<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("activateFine", ds.id()));
  responsebody += tr("\" class=\"btn btn-primary btn-xs disabled\" data-toggle=\"tooltip\" title=\"Activate fine\"><i class='fa fa-cogs'></i></a>\n													");
  } else{;
  responsebody += tr("														<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("activateFine", ds.id()));
  responsebody += tr("\" class=\"btn btn-primary btn-xs\" data-toggle=\"tooltip\" title=\"Activate fine\"><i class='fa fa-cogs'></i></a>\n													");
  };
  responsebody += tr("												");
  };
  responsebody += tr("												<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("checkIn", ds.id()));
  responsebody += tr("\" class=\"btn btn-success btn-xs\" data-toggle=\"tooltip\" title=\"Check item in\"><i class='fa fa-share-square-o'></i></a>\n											</td>\n										</tr>\n									");
  };
  responsebody += tr("								</tbody>\n								<tfoot>\n									<tr>\n										<th>Due item</th>\n										<th>Ordered by</th>\n										<th>Processed date</th>\n										<th>Was due</th>\n										<th></th>\n									</tr>\n								</tfoot>\n							</table>\n						</div>\n					</div>\n				</section>\n			</div>\n		");
  } else{;
  responsebody += tr("			<div class=\"col-sm-6\">\n				<section class=\"panel\">\n					<header class=\"panel-heading\">\n						Today's Due items\n						<span class=\"tools pull-right\">\n							<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n						</span>\n					</header>\n					<div class=\"panel-body\">\n						<div class=\"adv-table\">\n							<table  class=\"display table table-bordered table-striped\" id=\"dynamic-table\">\n								<thead>\n									<tr>\n										<th>Due item</th>\n										<th>Ordered by</th>\n										<th>Was due</th>\n										<th></th>\n									</tr>\n								</thead>\n								<tbody>\n									");
  foreach(DueSheets ds, cdues){;
  responsebody += tr("										<tr class=\"gradeA\">\n											<td>");
  responsebody += THttpUtility::htmlEscape(ds.orderedItem());
  responsebody += tr("</td>\n											<td>");
  responsebody += THttpUtility::htmlEscape(ds.orderedBy());
  responsebody += tr("</td>\n											");
  if(QDate::fromString(ds.dueDate(), "yyyy-MM-dd") == QDate::currentDate()){;
  responsebody += tr("												<td>today</td>\n											");
  } else{;
  responsebody += tr("												");
  if(QDate::fromString(ds.dueDate(), "yyyy-MM-dd").daysTo(QDate::currentDate()) == 1){;
  responsebody += tr("													<td>a day ago</td>\n												");
  } else{;
  responsebody += tr("													<td>");
  responsebody += THttpUtility::htmlEscape(QDate::fromString(ds.dueDate(), "yyyy-MM-dd").daysTo(QDate::currentDate()));
  responsebody += tr("days ago</td>\n												");
  };
  responsebody += tr("											");
  };
  responsebody += tr("											<td>\n												");
  if(QDate::fromString(ds.dueDate(), "yyyy-MM-dd") == QDate::currentDate()){;
  responsebody += tr("													<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("activateFine", ds.id()));
  responsebody += tr("\" class=\"btn btn-primary btn-xs disabled\" data-toggle=\"tooltip\" title=\"Activate fine\"><i class='fa fa-cogs'></i></a>\n												");
  } else{;
  responsebody += tr("													");
  if(QDate::fromString(ds.dayFineLastActivate(), "yyyy-MM-dd") == QDate::currentDate()){;
  responsebody += tr("														<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("activateFine", ds.id()));
  responsebody += tr("\" class=\"btn btn-primary btn-xs disabled\" data-toggle=\"tooltip\" title=\"Activate fine\"><i class='fa fa-cogs'></i></a>\n													");
  } else{;
  responsebody += tr("														<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("activateFine", ds.id()));
  responsebody += tr("\" class=\"btn btn-primary btn-xs\" data-toggle=\"tooltip\" title=\"Activate fine\"><i class='fa fa-cogs'></i></a>\n													");
  };
  responsebody += tr("												");
  };
  responsebody += tr("												<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("checkIn", ds.id()));
  responsebody += tr("\" class=\"btn btn-success btn-xs\" data-toggle=\"tooltip\" title=\"Check item in\"><i class='fa fa-share-square-o'></i></a>\n											</td>\n										</tr>\n									");
  };
  responsebody += tr("								</tbody>\n								<tfoot>\n									<tr>\n										<th>Due item</th>\n										<th>Ordered by</th>\n										<th>Was due</th>\n										<th></th>\n									</tr>\n								</tfoot>\n							</table>\n						</div>\n					</div>\n				</section>\n			</div>\n			<div class=\"col-sm-6\">\n				<section class=\"panel\">\n					<header class=\"panel-heading\">\n						Checkin form\n						<span class=\"tools pull-right\">\n							<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n						</span>\n					</header>\n					<div class=\"panel-body\">\n						<div class=\"form\">\n							");
  responsebody += QVariant(formTag(urla("checkInProcess", dueSheet.id()), Tf::Post, "", a("class", "cmxform tasi-form"))).toString();
  responsebody += tr("\n								<div class=\"form-group\">\n									<label for=\"itemOrdered\">Item to Checkin</label>\n									<input class=\" form-control\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(dueSheet.orderedItem());
  responsebody += tr("\" readonly />\n								</div>\n								<div class=\"form-group\">\n									<label for=\"orderedBy\">User returning item</label>\n									<input class=\"form-control\" name=\"\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(dueSheet.orderedBy());
  responsebody += tr("\" readonly />\n								</div>\n								<div class=\"form-group\">\n									<div class=\"pull-right\">\n										<button class=\"btn btn-danger\" type=\"submit\">Check in</button>\n										<a class=\"btn btn-default\" type=\"button\" href=\"/Staff/viewDueSheet\">Cancel</a>\n									</div>\n								</div>\n							</form>\n						</div>\n					</div>\n				</section>\n			</div>\n		");
  };
  responsebody += tr("    </div>\n    <!-- page end-->\n</section>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(staff_dueSheetView)
T_REGISTER_VIEW(staff_dueSheetView)

#include "staff_dueSheetView.moc"
