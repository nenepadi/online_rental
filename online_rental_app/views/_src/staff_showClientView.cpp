#include <QtCore>
#include <TreeFrogView>
#include "client.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT staff_showClientView : public TActionView
{
  Q_OBJECT
public:
  staff_showClientView() : TActionView() { }
  staff_showClientView(const staff_showClientView &) : TActionView() { }
  QString toString();
};

QString staff_showClientView::toString()
{
  responsebody.reserve(1946);
  responsebody += tr("<!DOCTYPE html>\n");
    tfetch(Client, client);
  responsebody += tr("<html>\n<head>\n  <meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" />\n  <title>");
  responsebody += THttpUtility::htmlEscape(controller()->name() + ": " + controller()->activeAction());
  responsebody += tr("</title>\n</head>\n<body>\n<p style=\"color: red\">");
  tehex(error);
  responsebody += tr("</p>\n<p style=\"color: green\">");
  tehex(notice);
  responsebody += tr("</p>\n\n<h1>Showing Client</h1>\n<dt>Clientid</dt><dd>");
  responsebody += THttpUtility::htmlEscape(client.clientid());
  responsebody += tr("</dd><br />\n<dt>Firstname</dt><dd>");
  responsebody += THttpUtility::htmlEscape(client.firstname());
  responsebody += tr("</dd><br />\n<dt>Lastname</dt><dd>");
  responsebody += THttpUtility::htmlEscape(client.lastname());
  responsebody += tr("</dd><br />\n<dt>Username</dt><dd>");
  responsebody += THttpUtility::htmlEscape(client.username());
  responsebody += tr("</dd><br />\n<dt>Email</dt><dd>");
  responsebody += THttpUtility::htmlEscape(client.email());
  responsebody += tr("</dd><br />\n<dt>Password</dt><dd>");
  responsebody += THttpUtility::htmlEscape(client.password());
  responsebody += tr("</dd><br />\n<dt>Pick Location</dt><dd>");
  responsebody += THttpUtility::htmlEscape(client.pickLocation());
  responsebody += tr("</dd><br />\n<dt>Is Active</dt><dd>");
  responsebody += THttpUtility::htmlEscape(client.isActive());
  responsebody += tr("</dd><br />\n<dt>Entitled Number Movies</dt><dd>");
  responsebody += THttpUtility::htmlEscape(client.entitledNumberMovies());
  responsebody += tr("</dd><br />\n\n");
  responsebody += QVariant(linkTo("Edit", urla("edit_client", client.clientid()))).toString();
  responsebody += tr(" |\n");
  responsebody += QVariant(linkTo("Back", urla("client_listings", client.pickLocation()))).toString();
  responsebody += tr("\n\n</body>\n</html>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(staff_showClientView)
T_REGISTER_VIEW(staff_showClientView)

#include "staff_showClientView.moc"
