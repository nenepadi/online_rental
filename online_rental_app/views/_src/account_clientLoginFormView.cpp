#include <QtCore>
#include <TreeFrogView>
#include "applicationhelper.h"

class T_VIEW_EXPORT account_clientLoginFormView : public TActionView
{
  Q_OBJECT
public:
  account_clientLoginFormView() : TActionView() { }
  account_clientLoginFormView(const account_clientLoginFormView &) : TActionView() { }
  QString toString();
};

QString account_clientLoginFormView::toString()
{
  responsebody.reserve(3856);
  responsebody += tr("<!DOCTYPE html>\n<html lang=\"en\">\n  	<head>\n	    	<meta charset=\"utf-8\">\n	    	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\n	    	<title>Movie Rentals - User Login</title>\n\n	    	<link href=\"/worker/css/bootstrap.css\" rel=\"stylesheet\">\n	    	<link href=\"/common/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />\n\n		<!-- Custom styles for this template -->\n		<link href=\"/worker/css/style.css\" rel=\"stylesheet\">\n		<link href=\"/worker/css/style-responsive.css\" rel=\"stylesheet\">\n\n		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->\n		<!--[if lt IE 9]>\n			<script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>\n		      	<script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>\n		<![endif]-->\n  	</head>\n\n  	<body>\n	  	<div id=\"login-page\">\n	  		<div class=\"container\">\n	  			");
  responsebody += QVariant(formTag(urla("clientLogin"), Tf::Post, "", a("class", "form-login"))).toString();
  responsebody += tr("\n		        		<h2 class=\"form-login-heading\">user login</h2>\n		        		<div class=\"login-wrap\">\n		        			<div>");
  techoex(errorMessage);
  responsebody += tr("</div>\n		        			<input type=\"email\" id=\"email\" name=\"email\" value=\"\" placeholder=\"name@domain.com\" class=\"form-control\" autofocus>\n		            			<br>\n		            			<input type=\"password\" id=\"password\" name=\"password\" value=\"\" placeholder=\"Password\" class=\"form-control\">\n		            			<label class=\"checkbox\">\n		                			<span class=\"pull-right\">\n		                    			<a data-toggle=\"modal\" href=\"#myModal\"> Forgot Password?</a>\n		                			</span>\n		            			</label>\n		            			<button class=\"btn btn-theme btn-block\" type=\"submit\"><i class=\"fa fa-lock\"></i> SIGN IN</button>\n		            		</div>\n		      		</form>\n\n		      		<!-- Modal -->\n	          		<div aria-hidden=\"true\" aria-labelledby=\"myModalLabel\" role=\"dialog\" tabindex=\"-1\" id=\"myModal\" class=\"modal fade\">\n	              		<div class=\"modal-dialog\">\n	                  			<div class=\"modal-content\">\n	                      			<div class=\"modal-header\">\n	                          			<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n	                          			<h4 class=\"modal-title\">Forgot Password ?</h4>\n	                      			</div>\n	                      			<div class=\"modal-body\">\n	                          			<p>Enter your e-mail address below to reset your password.</p>\n	                          			<input type=\"text\" name=\"email\" placeholder=\"Email\" autocomplete=\"off\" class=\"form-control placeholder-no-fix\">\n	                      			</div>\n			                      	<div class=\"modal-footer\">\n			                          	<button data-dismiss=\"modal\" class=\"btn btn-default\" type=\"button\">Cancel</button>\n			                          	<button class=\"btn btn-theme\" type=\"button\">Submit</button>\n			                      	</div>\n	                  			</div>\n	              		</div>\n	          		</div>\n		    	</div>\n	  	</div>\n	    	<!-- js placed at the end of the document so the pages load faster -->\n	    	<script src=\"/worker/js/jquery.js\"></script>\n	    	<script src=\"/worker/js/bootstrap.min.js\"></script>\n	    	<!--BACKSTRETCH-->\n	    	<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->\n	    	<script type=\"text/javascript\" src=\"/worker/js/jquery.backstretch.min.js\"></script>\n	    	<script>\n	        	$.backstretch(\"/worker/img/login-bg.jpg\", {speed: 500});\n	    	</script>\n  	</body>\n</html>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(account_clientLoginFormView)
T_REGISTER_VIEW(account_clientLoginFormView)

#include "account_clientLoginFormView.moc"
