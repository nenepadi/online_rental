#include <QtCore>
#include <TreeFrogView>
#include "category.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT admin_categoryListingsView : public TActionView
{
  Q_OBJECT
public:
  admin_categoryListingsView() : TActionView() { }
  admin_categoryListingsView(const admin_categoryListingsView &) : TActionView() { }
  QString toString();
};

QString admin_categoryListingsView::toString()
{
  responsebody.reserve(2627);
    tfetch(QList<Category>, allCategories);
  tfetch(Category, catlog);
  responsebody += tr("<section class=\"wrapper\">\n	<!-- page start-->\n	<div class=\"row\">\n		<div class=\"col-sm-6 col-sm-offset-3\">\n			<div class=\"col-sm-12\">\n				<section class=\"panel\">\n					<header class=\"panel-heading\">\n						Create and Edit Categories\n					</header>\n					<div class=\"panel-body\">\n						");
  if(controller()->activeAction() == "categoryListings"){;
  responsebody += tr("							");
  responsebody += QVariant(formTag(urla("category_create"))).toString();
  responsebody += tr("\n								<div class=\"form-group col-sm-10\">\n									<input type=\"text\" class=\"form-control\" name=\"categoryName\" placeholder=\"Enter category name\" value=\"\" required>\n								</div>\n								<button type=\"submit\" class=\"btn btn-success col-sm-2\">Add</button>\n							</form>\n						");
  } else{;
  responsebody += tr("							");
  responsebody += QVariant(formTag(urla("category_save", catlog.catId()))).toString();
  responsebody += tr("\n								<div class=\"form-group col-sm-10\">\n									<input type=\"text\" class=\"form-control\" name=\"categoryName\" placeholder=\"Enter category name\" value=\"");
  responsebody += THttpUtility::htmlEscape(catlog.categoryName());
  responsebody += tr("\" required>\n								</div>\n								<button type=\"submit\" class=\"btn btn-success col-sm-2\">Update</button>\n							</form>\n						");
  };
  responsebody += tr("					</div>\n				</section>\n			</div>\n			<div class=\"col-sm-12\">\n				<section class=\"panel\">\n					<header class=\"panel-heading\">\n						All Catgories\n						<span class=\"tools pull-right\">\n							<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n							<a href=\"javascript:;\" class=\"fa fa-times\"></a>\n						</span>\n					</header>\n					<div class=\"panel-body\">\n						<ul class=\"list-group\">\n                            			");
  foreach(Category cat, allCategories){;
  responsebody += tr("								<li class=\"list-group-item\">\n									");
  responsebody += QVariant(linkTo(cat.categoryName(), urla("category_edit", cat.catId()))).toString();
  responsebody += tr("\n									");
  responsebody += QVariant(linkTo("<i class='fa fa-trash-o'></i>", urla("category_remove", cat.catId()), Tf::Post, "confirm('Are you sure you want to delete?')", a("class", "btn btn-danger btn-xs pull-right"))).toString();
  responsebody += tr("\n								</li>\n							");
  };
  responsebody += tr("                        			</ul>\n					</div>\n				</section>\n			</div>\n		</div>\n    	</div>\n    	<!-- page end-->\n</section>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(admin_categoryListingsView)
T_REGISTER_VIEW(admin_categoryListingsView)

#include "admin_categoryListingsView.moc"
