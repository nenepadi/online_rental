#include <QtCore>
#include <TreeFrogView>
#include "client.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT admin_allClientsView : public TActionView
{
  Q_OBJECT
public:
  admin_allClientsView() : TActionView() { }
  admin_allClientsView(const admin_allClientsView &) : TActionView() { }
  QString toString();
};

QString admin_allClientsView::toString()
{
  responsebody.reserve(2933);
    tfetch(QList<Client>, clients);
  responsebody += tr("<section class=\"wrapper\">\n    	<!-- page start-->\n    	<div class=\"row\">\n       	<div class=\"col-sm-12\">\n            		<section class=\"panel\">\n              		<header class=\"panel-heading\">\n                  			<b>All Clients</b>\n             			<span class=\"tools pull-right\">\n                				<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n                				<a href=\"javascript:;\" class=\"fa fa-times\"></a>\n             			</span>\n              		</header>\n              		<div class=\"panel-body\">\n              			<div class=\"adv-table\">\n              				<table  class=\"display table table-bordered table-striped\" id=\"dynamic-table\">\n              					<thead>\n              						<tr>\n                  							<th>Username</th>\n                  							<th>Email</th>\n                  							<th>Pick Location</th>\n                  							<th class=\"hidden-phone\">Is Active</th>\n                  							<th class=\"hidden-phone\">Entitled Number Movies</th>\n              						</tr>\n              					</thead>\n              					<tbody>\n              						");
  foreach(Client cl, clients){;
  responsebody += tr("	              					<tr class=\"gradeA\">\n	                  						<td>");
  responsebody += THttpUtility::htmlEscape(cl.username());
  responsebody += tr("</td>\n	                  						<td>");
  responsebody += THttpUtility::htmlEscape(cl.email());
  responsebody += tr("</td>\n	                  						<td>");
  responsebody += THttpUtility::htmlEscape(cl.pickLocation());
  responsebody += tr("</td>\n	                  						");
  if(cl.isActive()){;
  responsebody += tr("	                  							<td class=\"center hidden-phone\"><i class=\"fa fa-check-square-o\" style=\"color: green;\"></i></td>\n	                  						");
  } else{;
  responsebody += tr("	                  							<td class=\"center hidden-phone\"><i class=\"fa fa-square-o\" style=\"color: red;\"></i></td>\n	                  						");
  };
  responsebody += tr("	                  						<td class=\"center hidden-phone\">");
  responsebody += THttpUtility::htmlEscape(cl.entitledNumberMovies());
  responsebody += tr("</td>\n	              					</tr>\n              						");
  };
  responsebody += tr("              					</tbody>\n              					<tfoot>\n              						<tr>\n                  							<th>Username</th>\n                  							<th>Email</th>\n                  							<th>Pick Location</th>\n                  							<th class=\"hidden-phone\">Is Active</th>\n                  							<th class=\"hidden-phone\">Entitled Number Movies</th>\n              						</tr>\n              					</tfoot>\n              				</table>\n              			</div>\n              		</div>\n            		</section>\n        	</div>\n    	</div>\n    	<!-- page end-->\n</section>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(admin_allClientsView)
T_REGISTER_VIEW(admin_allClientsView)

#include "admin_allClientsView.moc"
