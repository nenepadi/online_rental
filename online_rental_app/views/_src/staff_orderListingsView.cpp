#include <QtCore>
#include <TreeFrogView>
#include "clientorder.h" 
#include "rentalagent.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT staff_orderListingsView : public TActionView
{
  Q_OBJECT
public:
  staff_orderListingsView() : TActionView() { }
  staff_orderListingsView(const staff_orderListingsView &) : TActionView() { }
  QString toString();
};

QString staff_orderListingsView::toString()
{
  responsebody.reserve(5859);
      tfetch(QList<ClientOrder>, ufOrderList);
  tfetch(QList<ClientOrder>, fOrderList);
  tfetch(RentalAgent, agent);
  tfetch(QVariantMap, clientOrder);
  responsebody += tr("<section class=\"wrapper\">\n    <!-- page start-->\n    <div class=\"row\">\n       	<div class=\"col-sm-6\">\n            <section class=\"panel\">\n              	<header class=\"panel-heading\">\n				<b>Unprocessed orders</b>\n             		<span class=\"tools pull-right\">\n                		<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n             		</span>\n              	</header>\n              	<div class=\"panel-body\">\n              		<div class=\"adv-table\">\n              			<table  class=\"display table table-bordered table-striped\" id=\"dynamic-table\">\n              				<thead>\n              					<tr>\n                  					<th>Item</th>\n                  					<th>Ordered by</th>\n                  					<th>Processed?</th>\n                  					<th></th>\n              					</tr>\n              				</thead>\n              				<tbody>\n              					");
  foreach(ClientOrder uo, ufOrderList){;
  responsebody += tr("	              					<tr class=\"gradeA\">\n		                  					<td>");
  responsebody += THttpUtility::htmlEscape(uo.movie());
  responsebody += tr("</td>\n		                  					<td>");
  responsebody += THttpUtility::htmlEscape(uo.user());
  responsebody += tr("</td>\n		                  					<td>");
  responsebody += THttpUtility::htmlEscape(uo.fulfilled());
  responsebody += tr("</td>\n		                  					<td>\n		                  						<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("orderProcessForm", uo.id()));
  responsebody += tr("\" class=\"btn btn-primary btn-xs\" data-toggle=\"tooltip\" title=\"Proccess this order\"><i class='fa fa-cogs'></i></a>\n	              					</tr>\n              					");
  };
  responsebody += tr("              				</tbody>\n              				<tfoot>\n              					<tr>\n                  					<th>Item</th>\n                  					<th>Ordered by</th>\n                  					<th>Processed?</th>\n                  					<th></th>\n              					</tr>\n              				</tfoot>\n              			</table>\n              		</div>\n              	</div>\n            </section>\n        </div>\n\n        ");
  if(controller()->activeAction() == "orderListings"){;
  responsebody += tr("			<div class=\"col-sm-6\">\n				<section class=\"panel\">\n					<header class=\"panel-heading\">\n						<b>Processed orders</b>\n						<span class=\"tools pull-right\">\n							<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n						</span>\n					</header>\n					<div class=\"panel-body\">\n						<div class=\"adv-table\">\n							<table  class=\"display table table-bordered table-striped\" id=\" \">\n								<thead>\n									<tr>\n										<th>Item</th>\n										<th>Ordered by</th>\n										<th>Processed?</th>\n									</tr>\n								</thead>\n								<tbody>\n									");
  foreach(ClientOrder fo, fOrderList){;
  responsebody += tr("										<tr class=\"gradeA\">\n											<td>");
  responsebody += THttpUtility::htmlEscape(fo.movie());
  responsebody += tr("</td>\n											<td>");
  responsebody += THttpUtility::htmlEscape(fo.user());
  responsebody += tr("</td>\n											<td>");
  responsebody += THttpUtility::htmlEscape(fo.fulfilled());
  responsebody += tr("</td>\n										</tr>\n									");
  };
  responsebody += tr("								</tbody>\n								<tfoot>\n									<tr>\n										<th>Item</th>\n										<th>Ordered by</th>\n										<th>Processed?</th>\n									</tr>\n								</tfoot>\n							</table>\n						</div>\n					</div>\n				</section>\n			</div>\n		");
  } else{;
  responsebody += tr("			<div class=\"col-sm-6\">\n				<section class=\"panel\">\n					<header class=\"panel-heading\">\n						<b>Order processing</b>\n						<span class=\"tools pull-right\">\n							<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n						</span>\n					</header>\n					<div class=\"panel-body\">\n						<div class=\"form\">\n							");
  responsebody += QVariant(formTag(urla("orderProcessed", clientOrder["id"]), Tf::Post, "", a("class", "cmxform tasi-form"))).toString();
  responsebody += tr("\n								<div class=\"form-group\">\n									<label for=\"itemOrdered\">Item Ordered</label>\n									<input class=\" form-control\" name=\"clientOrder[movie]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(clientOrder["movie"]);
  responsebody += tr("\" readonly />\n								</div>\n								<div class=\"form-group\">\n									<label for=\"orderedBy\">Ordered by</label>\n									<input class=\" form-control\" name=\"clientOrder[user]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(clientOrder["user"]);
  responsebody += tr("\" readonly />\n								</div>\n								<div class=\"form-group\">\n									<label for=\"processedDate\">Proccessed date</label>\n									<input class=\" form-control\" name=\"processedDate\" type=\"date\" value=\"");
  techoex(date);
  responsebody += tr("\" readonly />\n								</div>\n								<div class=\"form-group\">\n									<label for=\"dueDate\">Due date</label>\n									<input class=\" form-control\" name=\"dueDate\" type=\"date\" required/>\n								</div>\n								<div class=\"form-group\">\n									<div class=\"pull-right\">\n										<button class=\"btn btn-danger\" type=\"submit\">Process</button>\n										<a class=\"btn btn-default\" type=\"button\" \n										href=\"/Staff/orderListings/");
  responsebody += THttpUtility::htmlEscape(agent.location());
  responsebody += tr("\">Cancel</a>\n									</div>\n								</div>\n							</form>\n						</div>\n					</div>\n				</section>\n			</div>\n		");
  };
  responsebody += tr("    </div>\n    <!-- page end-->\n</section>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(staff_orderListingsView)
T_REGISTER_VIEW(staff_orderListingsView)

#include "staff_orderListingsView.moc"
