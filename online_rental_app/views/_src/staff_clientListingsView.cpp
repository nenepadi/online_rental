#include <QtCore>
#include <TreeFrogView>
#include "client.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT staff_clientListingsView : public TActionView
{
  Q_OBJECT
public:
  staff_clientListingsView() : TActionView() { }
  staff_clientListingsView(const staff_clientListingsView &) : TActionView() { }
  QString toString();
};

QString staff_clientListingsView::toString()
{
  responsebody.reserve(3728);
    tfetch(QList<Client>, clientByLocationList);
  responsebody += tr("<section class=\"wrapper\">\n    	<!-- page start-->\n    	<div class=\"row\">\n       	<div class=\"col-sm-12\">\n            		<section class=\"panel\">\n              		<header class=\"panel-heading\">\n                  			<b>Your clients</b>\n             			<span class=\"tools pull-right\">\n             				<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("client_entry"));
  responsebody += tr("\" class=\"btn btn-shadow btn-success btn-xs\" data-toggle=\"tooltip\" title=\"Register client\"><i class='fa fa-plus'></i></a>\n                				<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n                				<a href=\"javascript:;\" class=\"fa fa-times\"></a>\n             			</span>\n              		</header>\n              		<div class=\"panel-body\">\n              			<div class=\"adv-table\">\n              				<table  class=\"display table table-bordered table-striped\" id=\"dynamic-table\">\n              					<thead>\n              						<tr>\n                  							<th>Username</th>\n                  							<th>Email</th>\n                  							<th class=\"hidden-phone\">Is Active</th>\n                  							<th class=\"hidden-phone\">Entitled Number Movies</th>\n                  							<th></th>\n              						</tr>\n              					</thead>\n              					<tbody>\n              						");
  foreach(Client cl, clientByLocationList){;
  responsebody += tr("	              						<tr class=\"gradeA\">\n	                  							<td>");
  responsebody += THttpUtility::htmlEscape(cl.username());
  responsebody += tr("</td>\n	                  							<td>");
  responsebody += THttpUtility::htmlEscape(cl.email());
  responsebody += tr("</td>\n	                  							");
  if(cl.isActive()){;
  responsebody += tr("	                  								<td class=\"center hidden-phone\"><i class=\"fa fa-check-square-o\" style=\"color: green;\"></i></td>\n	                  							");
  } else{;
  responsebody += tr("	                  								<td class=\"center hidden-phone\"><i class=\"fa fa-square-o\" style=\"color: red;\"></i></td>\n	                  							");
  };
  responsebody += tr("	                  							<td class=\"center hidden-phone\">");
  responsebody += THttpUtility::htmlEscape(cl.entitledNumberMovies());
  responsebody += tr("</td>\n	                  							<td>\n	                  								<a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("edit_client", cl.clientid()));
  responsebody += tr("\" class=\"btn btn-primary btn-xs\" data-toggle=\"tooltip\" title=\"Edit client #");
  responsebody += THttpUtility::htmlEscape(cl.clientid());
  responsebody += tr("\"><i class='fa fa-pencil'></i></a>\n	                  								");
  responsebody += QVariant(linkTo("<i class='fa fa-trash-o'></i>", urla("remove_client", cl.clientid()), Tf::Post, "confirm('Are you sure you want to delete?')", a("class", "btn btn-danger btn-xs"))).toString();
  responsebody += tr("\n										</td>\n	              						</tr>\n              						");
  };
  responsebody += tr("              					</tbody>\n              					<tfoot>\n              						<tr>\n                  							<th>Username</th>\n                  							<th>Email</th>\n                  							<th class=\"hidden-phone\">Is Active</th>\n                  							<th class=\"hidden-phone\">Entitled Number Movies</th>\n                  							<th></th>\n              						</tr>\n              					</tfoot>\n              				</table>\n              			</div>\n              		</div>\n            		</section>\n        	</div>\n   	</div>\n    	<!-- page end-->\n</section>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(staff_clientListingsView)
T_REGISTER_VIEW(staff_clientListingsView)

#include "staff_clientListingsView.moc"
