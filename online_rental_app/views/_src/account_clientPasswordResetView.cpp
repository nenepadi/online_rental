#include <QtCore>
#include <TreeFrogView>
#include "user.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT account_clientPasswordResetView : public TActionView
{
  Q_OBJECT
public:
  account_clientPasswordResetView() : TActionView() { }
  account_clientPasswordResetView(const account_clientPasswordResetView &) : TActionView() { }
  QString toString();
};

QString account_clientPasswordResetView::toString()
{
  responsebody.reserve(8251);
  responsebody += tr("<!DOCTYPE html>\n");
    tfetch(User, user);
  responsebody += tr("<html lang=\"en\">\n	<head>\n	    <meta charset=\"utf-8\">\n	    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\n	    <title>Online Rentals:Shopfront - ");
  responsebody += THttpUtility::htmlEscape(controller()->name());
  responsebody += tr("</title>\n\n	    <!-- Bootstrap core CSS -->\n	    <link href=\"/flatlab/css/bootstrap.min.css\" rel=\"stylesheet\">\n	    <link href=\"/flatlab/css/bootstrap-reset.css\" rel=\"stylesheet\">\n	    <!--external css-->\n	    <link href=\"/flatlab/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />\n	    <!--right slidebar-->\n	    <link href=\"/flatlab/css/slidebars.css\" rel=\"stylesheet\">\n	    <!-- Custom styles for this template -->\n	    <link href=\"/flatlab/css/style.css\" rel=\"stylesheet\">\n	    <link href=\"/flatlab/css/style-responsive.css\" rel=\"stylesheet\" />\n\n	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->\n	    <!--[if lt IE 9]>\n	      <script src=\"/flatlab/js/html5shiv.js\"></script>\n	      <script src=\"/flatlab/js/respond.min.js\"></script>\n	    <![endif]-->\n	</head>\n\n  	<body class=\"full-width\">\n  		<section id=\"container\" class=\"\">\n      		<!--header start-->\n      		<header class=\"header white-bg\">\n          		<!--logo start-->\n          		<a href=\"\" class=\"logo\" >Online<span>Rentals</span></a>\n          		<!--logo end-->\n          		<!--nav start-->\n          		<div class=\"top-nav \">\n              		<ul class=\"nav pull-right top-menu\">\n                  		<li>\n                      		<input type=\"text\" class=\"form-control search\" placeholder=\"Search\">\n                  		</li>\n                  		<!-- user login dropdown start-->\n                  		<li class=\"dropdown\">\n                      		<a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">\n                          		<img alt=\"\" src=\"/flatlab/img/avatar1_small1.jpg\">\n                          		<span class=\"username\">");
  responsebody += THttpUtility::htmlEscape(user.username());
  responsebody += tr("</span>\n                          		<b class=\"caret\"></b>\n                      		</a>\n                      		<ul class=\"dropdown-menu extended logout\">\n                          		<div class=\"log-arrow-up\"></div>\n                          		<li><a href=\"#\"><i class=\" fa fa-suitcase\"></i>Profile</a></li>\n                          		<li>");
  responsebody += QVariant(linkTo("<i class='fa fa-cog'></i> Password", url("Account", "changePasswordForm"))).toString();
  responsebody += tr("</li>\n                          		<li><a href=\"#\"><i class=\"fa fa-map-marker\"></i>Location</a></li>\n                          		<li>");
  responsebody += QVariant(linkTo("<i class='fa fa-key'></i> Logout", url("Account", "clientLogout"))).toString();
  responsebody += tr("</li>\n                      		</ul>\n                  		</li>\n                  		<!-- user login dropdown end -->\n              		</ul>\n          		</div>\n          		<!--nav end-->\n      		</header>\n      		<!--header end-->\n\n      		<!--main content start-->\n      		<section id=\"main-content\">\n          		<section class=\"wrapper\">\n              		<!-- page start-->\n              		<div class=\"row\">\n              			<div class=\"col-lg-12\">&nbsp;</div>\n              			<div class=\"col-lg-12\">&nbsp;</div>\n              			<div class=\"col-lg-12\">&nbsp;</div>\n              			<div class=\"col-lg-8 col-lg-offset-2\">\n	              			<div class=\"col-lg-6 col-lg-offset-3\">\n							    <!--widget start-->\n							    <section class=\"panel\">\n							        <div class=\"twt-feed blue-bg\">\n							            <h1>");
  responsebody += THttpUtility::htmlEscape(user.firstname() + " " + user.lastname());
  responsebody += tr("</h1>\n							            <p>");
  responsebody += THttpUtility::htmlEscape(user.email());
  responsebody += tr("</p>\n							            <a href=\"#\">\n							                <img src=\"/flatlab/img/profile-avatar.jpg\" alt=\"\">\n							            </a>\n							        </div>\n							     	\n							     	<div class=\"weather-category twt-category\">\n	                                    <ul>\n	                                        <li class=\"active\">\n	                                            <h5>");
  responsebody += THttpUtility::htmlEscape(user.pickLocation());
  responsebody += tr("</h5>\n	                                            Pick location\n	                                        </li>\n	                                        <li>\n	                                            <h5>");
  responsebody += THttpUtility::htmlEscape(user.entitledNumberMovies());
  responsebody += tr("</h5>\n	                                            Movies left\n	                                        </li>\n	                                        <li>\n	                                            <h5>");
  responsebody += THttpUtility::htmlEscape(user.username());
  responsebody += tr("</h5>\n	                                            Username\n	                                        </li>\n	                                    </ul>\n	                                </div>\n							        <form>\n							        	<div class = \"panel col-lg-10 col-lg-offset-1\">\n							        		<div class=\"panel-body\">\n							        			<div class=\"form-group\">\n		                                      		<label for=\"oldpass\">Old password</label>\n		                                      		<input type=\"password\" class=\"form-control\" id=\"oldpass\" placeholder=\"Enter old password\">\n		                                  		</div>\n		                                  		<div class=\"form-group\">\n		                                      		<label for=\"newpass\">New password</label>\n		                                      		<input type=\"password\" class=\"form-control\" id=\"newpass\" placeholder=\"Enter new password\">\n		                                  		</div>\n							        		</div>\n							        	</div>\n							       		\n								        <footer class=\"twt-footer\">\n								            <button class=\"btn btn-space btn-white\" data-toggle=\"button\">\n								                <i class=\"fa fa-map-marker\"></i>\n								            </button>\n								            <button class=\"btn btn-space btn-white\" data-toggle=\"button\">\n								                <i class=\"fa fa-cog\"></i>\n								            </button>\n								            <button class=\"btn btn-space btn-info pull-right\" type=\"submit\">Save changes</button>\n								        </footer>\n							        </form>\n							    </section>\n							    <!--widget end-->\n							</div>\n						</div>\n              		</div>\n              		<!-- page end-->\n          		</section>\n      		</section>\n      		<!--main content end-->\n\n      		<!--footer start-->\n      		<footer class=\"site-footer\">\n          		<div class=\"text-center\">\n              		&copy;2015 Aseda Designs.\n              		<a href=\"#\" class=\"go-top\">\n                  		<i class=\"fa fa-angle-up\"></i>\n              		</a>\n          		</div>\n      		</footer>\n      		<!--footer end-->\n  		</section>\n\n    	<!-- js placed at the end of the document so the pages load faster -->\n    	<script src=\"/flatlab/js/jquery.js\"></script>\n	    <script src=\"/flatlab/js/bootstrap.min.js\"></script>\n	    <script class=\"include\" type=\"text/javascript\" src=\"/flatlab/js/jquery.dcjqaccordion.2.7.js\"></script>\n	    <script src=\"/flatlab/js/jquery.scrollTo.min.js\"></script>\n	    <script src=\"/flatlab/js/jquery.nicescroll.js\" type=\"text/javascript\"></script>\n	    <script src=\"/flatlab/assets/jquery-knob/js/jquery.knob.js\"></script>\n	    <script src=\"/flatlab/js/respond.min.js\" ></script>\n\n	  	<!--right slidebar-->\n	  	<script src=\"/flatlab/js/slidebars.min.js\"></script>\n\n	    <!--common script for all pages-->\n	    <script src=\"/flatlab/js/common-scripts.js\"></script>\n\n	  	<script>\n	      	//knob\n	      	$(\".knob\").knob();\n	  	</script>\n	</body>\n</html>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(account_clientPasswordResetView)
T_REGISTER_VIEW(account_clientPasswordResetView)

#include "account_clientPasswordResetView.moc"
