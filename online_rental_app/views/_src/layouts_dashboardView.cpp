#include <QtCore>
#include <TreeFrogView>
#include "applicationhelper.h"

class T_VIEW_EXPORT layouts_dashboardView : public TActionView
{
  Q_OBJECT
public:
  layouts_dashboardView() : TActionView() { }
  layouts_dashboardView(const layouts_dashboardView &) : TActionView() { }
  QString toString();
};

QString layouts_dashboardView::toString()
{
  responsebody.reserve(4554);
  responsebody += tr("<!DOCTYPE html>\n<html>\n	<head>\n    	<meta charset=\"utf-8\">\n    	<title>Movie Rentals -- ");
  responsebody += THttpUtility::htmlEscape(controller()->name() + ":" + controller()->activeAction());
  responsebody += tr("</title>\n\n		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\">\n    	<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\n\n		<link href=\"/admin/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n		<link href=\"/admin/css/bootstrap-responsive.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n		<link href=\"/admin/css/font-awesome.css\" rel=\"stylesheet\">\n		<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600\" rel=\"stylesheet\">\n\n		<link href=\"/admin/css/style.css\" rel=\"stylesheet\" type=\"text/css\">\n		<link href=\"/admin/css/pages/dashboard.css\" rel=\"stylesheet\">\n	</head>\n\n	<body>\n		<div class=\"navbar navbar-fixed-top\">\n			<div class=\"navbar-inner\">\n				<div class=\"container\">\n					<a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\">\n						<span class=\"icon-bar\"></span>\n						<span class=\"icon-bar\"></span>\n						<span class=\"icon-bar\"></span>\n					</a>\n\n					");
  responsebody += QVariant(linkTo("Online Rentals", url("Home", "index"), Tf::Get, "", a("class", "brand"))).toString();
  responsebody += tr("\n					\n					<div class=\"nav-collapse\">\n						<ul class=\"nav pull-right\">\n							<li class=\"dropdown\">\n								<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n									<i class=\"fa fa-user\"></i>&nbsp;Administrator <b class=\"caret\"></b>\n								</a>\n            					<ul class=\"dropdown-menu\">\n              						<li><a href=\"javascript:;\">Profile</a></li>\n              						<li>\n              							");
  responsebody += QVariant(linkTo("Logout", url("Account", "staffLogout"))).toString();
  responsebody += tr("\n              						</li>\n            					</ul>\n          					</li>\n						</ul>\n						<form class=\"navbar-search pull-right\">\n          					<input type=\"text\" class=\"search-query\" placeholder=\"Search\">\n        				</form>\n					</div>	\n				</div>\n			</div>\n		</div>\n\n		<div class=\"subnavbar\">\n  			<div class=\"subnavbar-inner\">\n    			<div class=\"container\">\n      				<ul class=\"mainnav\">\n        				<li class=\"active\"><a href=\"/Staff/admin_dash\"><i class=\"fa fa-dashboard\"></i><span>Dashboard</span> </a> </li>\n        				<li class=\"dropdown\"><a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"> <i class=\"fa fa-user-md\"></i><span>Staff ops</span> <b class=\"caret\"></b></a>\n          					<ul class=\"dropdown-menu\">\n            					<li>");
  responsebody += QVariant(linkTo("Create Staff", url("Staff", "staff_entry"))).toString();
  responsebody += tr("</li>\n            					<li>");
  responsebody += QVariant(linkTo("All Staff", url("Staff", "all_staff"))).toString();
  responsebody += tr("</li>\n          					</ul>\n          				</li>\n          				<li class=\"dropdown\"><a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"> <i class=\"fa fa-table\"></i><span>Item ops</span> <b class=\"caret\"></b></a>\n          					<ul class=\"dropdown-menu\">\n            					<li>");
  responsebody += QVariant(linkTo("Create Item", url("Item", "item_entry"))).toString();
  responsebody += tr("</li>\n            					<li>");
  responsebody += QVariant(linkTo("All Items", url("Item", "all_items"))).toString();
  responsebody += tr("</li>\n          					</ul>\n          				</li>\n        				<li><a href=\"#\"><i class=\"fa fa-bar-chart\"></i><span>Reports</span> </a> </li>\n      				</ul>\n    			</div> \n  			</div> \n		</div>\n\n		<div class=\"main\">\n  			<div class=\"main-inner\">\n    			");
  responsebody += QVariant(yield()).toString();
  responsebody += tr(" \n  			</div> \n		</div>\n\n		<div class=\"footer\">\n  			<div class=\"footer-inner\">\n    			<div class=\"container\">\n      				<div class=\"row\">\n        				<div class=\"span12\"> &copy;2015 - Movie Rentals </div>\n      				</div>\n    			</div> \n  			</div> \n		</div>\n\n		\n		<script src=\"/admin/js/jquery-1.7.2.min.js\"></script>\n		<script src=\"/admin/js/bootstrap.js\"></script>\n		<!--script src=\"/admin/js/full-calendar/fullcalendar.min.js\"></script>\n		<script src=\"/admin/js/script.js\"></script-->\n\n	</body>\n</html>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(layouts_dashboardView)
T_REGISTER_VIEW(layouts_dashboardView)

#include "layouts_dashboardView.moc"
