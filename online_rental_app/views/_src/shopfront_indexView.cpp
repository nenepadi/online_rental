#include <QtCore>
#include <TreeFrogView>
#include "item.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT shopfront_indexView : public TActionView
{
  Q_OBJECT
public:
  shopfront_indexView() : TActionView() { }
  shopfront_indexView(const shopfront_indexView &) : TActionView() { }
  QString toString();
};

QString shopfront_indexView::toString()
{
  responsebody.reserve(3178);
    tfetch(QList<Item>, itemList);
  tfetch(QList<Item>, itemByCategoryList);
  responsebody += tr("<section class=\"panel\">\n    	<div class=\"panel-body\">\n    		<div class=\"pro-sort\">\n            		<label class=\"pro-lab\">Sort By</label>\n            		<select class=\"styled\" >\n            			<option>Default Sorting</option>\n                		<option class=\"sort\" data-sort=\"pro-title\">Name</option>\n                		<option class=\"sort\" data-sort=\"price\">Category</option>\n            		</select>\n        	</div>\n        	<div class=\"pull-right\">\n            		<ul class=\"pagination pagination-sm pro-page-list\"></ul>\n        	</div>\n    	</div>\n</section>\n\n<div class=\"row product-list list\">\n	");
  if(controller()->activeAction() == "index"){;
  responsebody += tr("    		<div>");
  techoex(notice);
  responsebody += tr("</div>\n    		");
  foreach(Item i, itemList){;
  responsebody += tr("    			<div class=\"col-md-4\">\n	            		<section class=\"panel\">\n	                		<div class=\"pro-img-box\">\n	                    		<img src=\"http://placehold.it/279x222\" alt=\"\"/>\n	                    		<a href=\"#\" class=\"adtocart\"><i class=\"fa fa-shopping-cart\"></i></a>\n	                		</div>\n\n	                		<div class=\"panel-body text-center\">\n	                    		<h4>\n	                    			<a href=\"/Shopfront/itemDetails/");
  responsebody += THttpUtility::htmlEscape(i.id());
  responsebody += tr("\" class=\"pro-title\">");
  responsebody += THttpUtility::htmlEscape(i.itemTitle());
  responsebody += tr("</a>\n	                    		</h4>\n	                    		<p class=\"price\">");
  responsebody += THttpUtility::htmlEscape(i.category());
  responsebody += tr("</p>\n	                		</div>\n	            		</section>\n	        	</div>\n	    	");
  };
  responsebody += tr("    	");
  } else{;
  responsebody += tr("    		");
  foreach(Item i, itemByCategoryList){;
  responsebody += tr("    			<div class=\"col-md-4\">\n	            		<section class=\"panel\">\n	                		<div class=\"pro-img-box\">\n	                    		<img src=\"http://placehold.it/279x222\" alt=\"\"/>\n	                    		<a href=\"#\" class=\"adtocart\"><i class=\"fa fa-shopping-cart\"></i></a>\n	                		</div>\n\n	                		<div class=\"panel-body text-center\">\n	                    		<h4>\n	                    			<a href=\"/Shopfront/itemDetails/");
  responsebody += THttpUtility::htmlEscape(i.id());
  responsebody += tr("\" class=\"pro-title\">");
  responsebody += THttpUtility::htmlEscape(i.itemTitle());
  responsebody += tr("</a>\n	                    		</h4>\n	                    		<p class=\"price\">");
  responsebody += THttpUtility::htmlEscape(i.category());
  responsebody += tr("</p>\n	                		</div>\n	            		</section>\n	        	</div>\n	    	");
  };
  responsebody += tr("    	");
  };
  responsebody += tr("</div>\n\n<script type=\"text/javascript\">\n	var options = {\n		valueNames: [ 'pro-title', 'price' ],\n		page: 6,\n    		plugins: [ListPagination({})]\n	};\n\n	var productList = new List('movies', options);\n</script>");

  return responsebody;
}

Q_DECLARE_METATYPE(shopfront_indexView)
T_REGISTER_VIEW(shopfront_indexView)

#include "shopfront_indexView.moc"
