#include <QtCore>
#include <TreeFrogView>
#include "applicationhelper.h"

class T_VIEW_EXPORT layouts_homeView : public TActionView
{
  Q_OBJECT
public:
  layouts_homeView() : TActionView() { }
  layouts_homeView(const layouts_homeView &) : TActionView() { }
  QString toString();
};

QString layouts_homeView::toString()
{
  responsebody.reserve(6213);
  responsebody += tr("<!DOCTYPE html>\n<html class=\"home\" lang=\"\">\n	<head>\n		<meta charset=\"utf-8\">\n		<meta name=\"description\" content=\"flat, clean, responsive, application frontend template built with bootstrap 3\">\n		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n		<title>Online Rentals:Landing page - ");
  responsebody += THttpUtility::htmlEscape(controller()->name());
  responsebody += tr("</title>\n		<link rel=\"stylesheet\" href=\"/common/plugins/owl-carousel/owl.carousel.css\">\n		<link rel=\"stylesheet\" href=\"/common/plugins/owl-carousel/owl.theme.css\">\n		<link rel=\"stylesheet\" href=\"/common/plugins/owl-carousel/owl.transitions.css\">\n		<link rel=\"stylesheet\" href=\"/common/plugins/magnific-popup/magnific-popup.css\">\n		<link rel=\"stylesheet\" href=\"/common/bootstrap/css/bootstrap.min.css\">\n		<link rel=\"stylesheet\" href=\"/common/font-awesome/css/font-awesome.min.css\">\n		<link rel=\"stylesheet\" href=\"/common/themify-icons/themify-icons.css\">\n		<link rel=\"stylesheet\" href=\"/landing/css/animate.min.css\">\n		<link rel=\"stylesheet\" href=\"/landing/css/skin/blue.css\" id=\"skin\">\n		<link rel=\"stylesheet\" href=\"/landing/css/fonts/opensans.css\" id=\"font\">\n		<link rel=\"stylesheet\" href=\"/landing/css/main.css\">\n		<!--[if lt IE 9]><script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script><script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script><![endif]-->\n\n		<style type=\"text/css\">\n			#map-row{\n				height: 560px;\n				margin: 0;\n				padding: 2px;\n				border: solid 2px #ddd;\n				border-radius: 10px;\n			}\n			#map-canvas {\n				height: 550px;\n				margin: 0;\n				padding: 2px;\n				border: solid 2px #ddd;\n				border-radius: 10px;\n			}\n			p.ui-distance {\n				bottom:10px;\n				padding:5px 10px;\n				background:rgba(0,0,0,0.5);\n				color:#fff;\n				font-size:12px;\n				line-height:18px;\n				border-radius:3px;\n				word-wrap: break-word;\n			}\n		</style>\n\n		<link rel=\"stylesheet\" href=\"/landing/css/panel.css\">\n		<script src=\"/common/plugins/jquery-1.11.1.min.js\"></script>\n		<script src=\"/common/plugins/modernizr.js\"></script>\n		<!--script type=\"text/javascript\"\n		src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBZImRWLGyynQaujHrgyifDmjz-wnne3oI\"></script-->\n		<script src=\"/locations.json\"></script>\n		<!--script src=\"/flatlab/js/maps.js\"></script-->\n		<script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.4/mapbox.js'></script>\n		<link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.4/mapbox.css' rel='stylesheet' />\n	</head>\n	<body>\n		<header class=\"header\">\n			<div class=\"container\">\n				<nav class=\"heading-font\">\n					<div class=\"branding\">\n						<button type=\"button\" class=\"mobile-toggle\">\n							<span class=\"ti-menu\"></span>\n						</button>\n						<!-- <a href=\"/Home\" class=\"logo transition\"> Movie Rentals </a> -->\n						");
  responsebody += QVariant(linkTo("Online Rentals", url("Home", "index"), Tf::Get, "", a("class", "logo transition"))).toString();
  responsebody += tr("\n					</div>\n					<div class=\"navigation spy\">\n						<ul class=\"nav\">\n							<li class=\"hide\"><a href=\"#top\"></a></li>\n							<li><a href=\"#how\">How it works</a></li>\n							<li><a href=\"#pricing\">Pricing</a></li>\n							<li><a href=\"#contact\">Contact us</a></li>\n							<li class=\"dropdown show-on-hover\">\n								<a href=\"javascript:;\" class=\"ignore\" data-toggle=\"dropdown\">\n									<span>Terms & Conditions</span>\n								</a>\n								<ul class=\"dropdown-menu\">\n									<li><a href=\"#\" class=\"transition\" onclick=\"getLocations()\">Privacy Policy</a></li>\n									<li><a href=\"\" class=\"transition\">Pricing</a></li>\n								</ul>\n							</li>\n							<li>");
  responsebody += QVariant(linkTo("Login", url("Account", "clientLoginForm"), Tf::Get, "", a("class", "transition"))).toString();
  responsebody += tr("</li>\n							<li>");
  responsebody += QVariant(linkTo("Administrator", url("Account", "staffLoginForm"), Tf::Get, "", a("class", "nav-btn btn btn-success btn-rounded transition"))).toString();
  responsebody += tr("</li>\n						</ul>\n					</div>\n				</nav>\n			</div>\n		</header>\n\n		");
  responsebody += QVariant(yield()).toString();
  responsebody += tr("\n\n		<footer class=\"content-section\">\n			<div class=\"container\">\n				<div class=\"row text-center\">\n					<div class=\"col-sm-12 mb25\">\n						<a class=\"btn btn-primary btn-social-icon btn-rounded btn-outline btn-sm smooth-scroll mb25\" href=\"#top\"><i class=\"ti-angle-up\"></i></a><br>\n						<a class=\"btn btn-social-icon btn-facebook btn-rounded btn-sm ml5 mr5\" href=\"javascript:;\"><i class=\"fa fa-facebook\"></i></a>\n						<a class=\"btn btn-social-icon btn-twitter btn-rounded btn-sm ml5 mr5\" href=\"javascript:;\"><i class=\"fa fa-twitter\"></i></a>\n						<a class=\"btn btn-social-icon btn-google-plus btn-rounded btn-sm ml5 mr5\" href=\"javascript:;\"><i class=\"fa fa-google-plus\"></i></a>\n					</div>\n					<div class=\"col-sm-12 mb25\">\n						<small class=\"show\">&copy;&nbsp;Copyright Aseda<span class=\"color\">Designs</span>&nbsp;<span class=\"year\"></span>. All rights reserved</small>\n					</div>\n				</div>\n			</div>\n		</footer>\n\n		<script src=\"/common/bootstrap/js/bootstrap.js\"></script>\n		<script src=\"/common/plugins/appear/jquery.appear.js\"></script>\n		<script src=\"/common/plugins/nav/jquery.nav.js\"></script>\n		<script src=\"/common/plugins/jquery.easing.min.js\"></script>\n		<script src=\"/common/plugins/jquery.parallax.js\"></script>\n		<script src=\"/common/plugins/isotope/isotope.pkgd.min.js\"></script>\n		<script src=\"/common/plugins/count-to/jquery.countTo.js\"></script>\n		<script src=\"/common/plugins/owl-carousel/owl.carousel.min.js\"></script>\n		<script src=\"/common/plugins/magnific-popup/jquery.magnific-popup.min.js\"></script>\n		<script src=\"/landing/js/main.js\"></script>\n		<script src=\"/landing/js/subscribe.js\"></script>\n		<script src=\"/landing/js/demo_home.js\"></script>\n		<script src=\"/landing/js/panel.js\"></script>\n	</body>\n</html>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(layouts_homeView)
T_REGISTER_VIEW(layouts_homeView)

#include "layouts_homeView.moc"
