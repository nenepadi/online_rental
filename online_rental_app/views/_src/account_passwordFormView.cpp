#include <QtCore>
#include <TreeFrogView>
#include "rentalagent.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT account_passwordFormView : public TActionView
{
  Q_OBJECT
public:
  account_passwordFormView() : TActionView() { }
  account_passwordFormView(const account_passwordFormView &) : TActionView() { }
  QString toString();
};

QString account_passwordFormView::toString()
{
  responsebody.reserve(3256);
  responsebody += tr("<!DOCTYPE html>\n");
    tfetch(RentalAgent, agent);
  responsebody += tr("<html lang=\"en\">\n  	<head>\n    	<meta charset=\"utf-8\">\n    	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n    	<meta name=\"description\" content=\"\">\n    	<meta name=\"author\" content=\"Dashboard\">\n    	<meta name=\"keyword\" content=\"Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina\">\n\n    	<title>Online Rentals - Change Password</title>\n\n    	<link href=\"/worker/css/bootstrap.css\" rel=\"stylesheet\">\n    	<link href=\"/common/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />\n        \n	    <!-- Custom styles for this template -->\n	    <link href=\"/worker/css/style.css\" rel=\"stylesheet\">\n	    <link href=\"/worker/css/style-responsive.css\" rel=\"stylesheet\">\n\n	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->\n	    <!--[if lt IE 9]>\n	      <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>\n	      <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>\n	    <![endif]-->\n  	</head>\n\n  	<body>\n	  	<div id=\"login-page\">\n	  		<div class=\"container\">\n	  			");
  responsebody += QVariant(formTag(urla("passwordChange"), Tf::Post, "", a("class", "form-login"))).toString();
  responsebody += tr("\n			        	<h2 class=\"form-login-heading\">change password</h2>\n			        	<div class=\"login-wrap\">\n			        		<div>");
  techoex(error);
  responsebody += tr("</div>\n			        		<input type=\"password\" id=\"opass\" name=\"opass\" class=\"form-control\" autofocus placeholder=\"Type old password\"  required>\n			            		<br>\n			            		<input type=\"password\" id=\"npass\" name=\"npass\" placeholder=\"Type new password\" class=\"form-control\">\n			            		<br>\n			            		<input type=\"password\" id=\"rpass\" name=\"rpass\" placeholder=\"Re-type new password\" class=\"form-control\">\n			            		<br>\n			            		");
  if(agent.role() == "administrator"){;
  responsebody += tr("			            			<a class=\"btn btn-danger btn-block\" href=\"/Admin/index\"><i class=\"fa fa-arrow-circle-o-left\"></i> CANCEL</a>\n			            		");
  } else{;
  responsebody += tr("			            			<a class=\"btn btn-danger btn-block\" href=\"/Staff/staff_dash\"><i class=\"fa fa-arrow-circle-o-left\"></i> CANCEL</a>\n			            		");
  };
  responsebody += tr("			            		<button class=\"btn btn-theme btn-block\" type=\"submit\"><i class=\"fa fa-lock\"></i> UPDATE</button>\n			            </div>\n		      		</form>\n		    	</div>\n	  	</div>\n\n	    	<!-- js placed at the end of the document so the pages load faster -->\n	    	<script src=\"/worker/js/jquery.js\"></script>\n	    	<script src=\"/worker/js/bootstrap.min.js\"></script>\n\n	    	<!--BACKSTRETCH-->\n	    	<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->\n	    	<script type=\"text/javascript\" src=\"/worker/js/jquery.backstretch.min.js\"></script>\n	    	<script>\n	       	$.backstretch(\"/worker/img/login-bg.jpg\", {speed: 500});\n	    	</script>\n  	</body>\n</html>");

  return responsebody;
}

Q_DECLARE_METATYPE(account_passwordFormView)
T_REGISTER_VIEW(account_passwordFormView)

#include "account_passwordFormView.moc"
