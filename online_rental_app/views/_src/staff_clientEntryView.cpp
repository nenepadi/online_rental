#include <QtCore>
#include <TreeFrogView>
#include "client.h" 
#include "rentalagent.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT staff_clientEntryView : public TActionView
{
  Q_OBJECT
public:
  staff_clientEntryView() : TActionView() { }
  staff_clientEntryView(const staff_clientEntryView &) : TActionView() { }
  QString toString();
};

QString staff_clientEntryView::toString()
{
  responsebody.reserve(10131);
      tfetch(RentalAgent, agent);
  tfetch(QVariantMap, client);
  responsebody += tr("<section class=\"wrapper\">\n    	<!-- page start-->\n    	<div class=\"row\">\n        	<div class=\"col-lg-12\">\n            		<section class=\"panel\">\n                		<header class=\"panel-heading\">\n                			");
  if(controller()->activeAction() == "client_entry"){;
  responsebody += tr("                    			<b>User Registration form</b>\n                    		");
  } else{;
  responsebody += tr("                    			<b>Edit user #");
  responsebody += QVariant(client["clientid"]).toString();
  responsebody += tr("</b>\n                    		");
  };
  responsebody += tr("                    		<span class=\"tools pull-right\">\n                        			<a href=\"javascript:;\" class=\"fa fa-chevron-down\"></a>\n                        			<a href=\"javascript:;\" class=\"fa fa-times\"></a>\n                    		</span>\n                		</header>\n                		<div class=\"panel-body\">\n                    		<div>");
  techoex(error);
  responsebody += tr("</div>\n                    		<div class=\"form\">\n                    			");
  if(controller()->activeAction() == "client_entry"){;
  responsebody += tr("		                  			");
  responsebody += QVariant(formTag(urla("create_client"), Tf::Post, "", a("class", "cmxform form-horizontal tasi-form registerUser"))).toString();
  responsebody += tr("\n		                  		");
  } else{;
  responsebody += tr("		                  			");
  responsebody += QVariant(formTag(urla("save_client", client["clientid"]), Tf::Post, "", a("class", "cmxform form-horizontal tasi-form registerUser"))).toString();
  responsebody += tr("\n		                  		");
  };
  responsebody += tr("                            			<div class=\"form-group \">\n                                			<label for=\"firstname\" class=\"control-label col-lg-2\">First name</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"firstname\" name=\"client[firstname]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(client["firstname"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"lastname\" class=\"control-label col-lg-2\">Last name</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"lastname\" name=\"client[lastname]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(client["lastname"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"username\" class=\"control-label col-lg-2\">Username</label>\n                                			<div class=\"col-lg-10\">                                    				\n                                    				");
  if(controller()->activeAction() == "client_entry"){;
  responsebody += tr("					                  			<input class=\" form-control\" id=\"username\" name=\"client[username]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(client["username"]);
  responsebody += tr("\" />\n					                  		");
  } else{;
  responsebody += tr("					                  			<input class=\" form-control\" id=\"username\" name=\"client[username]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(client["username"]);
  responsebody += tr("\" readonly/>\n					                  		");
  };
  responsebody += tr("                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"email\" class=\"control-label col-lg-2\">Email</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"email\" name=\"client[email]\" type=\"email\" value=\"");
  responsebody += THttpUtility::htmlEscape(client["email"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"password\" class=\"control-label col-lg-2\">Password</label>\n                                			<div class=\"col-lg-10\">\n                                    				");
  if(controller()->activeAction() == "client_entry"){;
  responsebody += tr("					                  			<input class=\" form-control\" id=\"password\" name=\"client[password]\" type=\"password\" value=\"");
  responsebody += THttpUtility::htmlEscape(client["password"]);
  responsebody += tr("\" />\n					                  		");
  } else{;
  responsebody += tr("					                  			<input class=\" form-control\" id=\"password\" name=\"client[password]\" type=\"password\" value=\"");
  responsebody += THttpUtility::htmlEscape(client["password"]);
  responsebody += tr("\" readonly/>\n					                  		");
  };
  responsebody += tr("                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"picklocation\" class=\"control-label col-lg-2\">Pick location</label>\n                                			<div class=\"col-lg-10\">\n                                				");
  if(controller()->activeAction() == "client_entry"){;
  responsebody += tr("					                  			<input class=\" form-control\" id=\"picklocation\" name=\"client[pickLocation]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(agent.location());
  responsebody += tr("\"  readonly />\n					                  		");
  } else{;
  responsebody += tr("					                  			<input class=\" form-control\" id=\"picklocation\" name=\"client[pickLocation]\" type=\"text\" value=\"");
  responsebody += THttpUtility::htmlEscape(client["pickLocation"]);
  responsebody += tr("\" required/>\n					                  		");
  };
  responsebody += tr("                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"entitlednumbermovies\" class=\"control-label col-lg-2\">Entitlement</label>\n                                			<div class=\"col-lg-10\">\n                                    				<input class=\" form-control\" id=\"entitlednumbermovies\" name=\"client[entitledNumberMovies]\" type=\"number\" value=\"");
  responsebody += THttpUtility::htmlEscape(client["entitledNumberMovies"]);
  responsebody += tr("\" required/>\n                                    				<span class=\"help-block\">Number of movies entitled to user based on premium</span>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group \">\n                                			<label for=\"isactive\" class=\"control-label col-lg-2\">Active?</label>\n                                			<div class=\"col-lg-4\">\n                                				");
  if(client["isActive"].toBool() == true){;
  responsebody += tr("	                                				<label class=\"radio-inline\">\n	  										<input type=\"radio\" name=\"client[isActive]\" value=\"1\" onclick=\"OnSelect(this)\" checked> True\n										</label>\n										<label class=\"radio-inline\">\n	  										<input type=\"radio\" name=\"client[isActive]\" value=\"0\" onclick=\"OnSelect(this)\"> False\n										</label>\n									");
  } else{;
  responsebody += tr("										<label class=\"radio-inline\">\n	  										<input type=\"radio\" name=\"client[isActive]\" value=\"1\" onclick=\"OnSelect(this)\"> True\n										</label>\n										<label class=\"radio-inline\">\n	  										<input type=\"radio\" name=\"client[isActive]\" value=\"0\" onclick=\"OnSelect(this)\" checked> False\n										</label>\n									");
  };
  responsebody += tr("                                    				<input class=\" form-control\" id=\"isactive\" name=\"client[isActive]\" type=\"hidden\" value=\"");
  responsebody += THttpUtility::htmlEscape(client["isActive"]);
  responsebody += tr("\" required/>\n                                			</div>\n                            			</div>\n\n                            			<div class=\"form-group\">\n                                			<div class=\"col-lg-offset-2 col-lg-10\">\n                                				");
  if(controller()->activeAction() == "client_entry"){;
  responsebody += tr("				                    			<button class=\"btn btn-danger\" type=\"submit\">Save</button>\n                                    					<a class=\"btn btn-default\" href=\"/Staff/client_listings\">Cancel</a>\n				                    		");
  } else{;
  responsebody += tr("				                    			<button class=\"btn btn-danger\" type=\"submit\">Update</button>\n                                    					<a class=\"btn btn-default\" href=\"/Staff/client_listings\">Cancel</a>\n				                    		");
  };
  responsebody += tr("                                			</div>\n                            			</div>\n                        			</form>\n                    		</div>\n                		</div>\n            		</section>\n        	</div>\n    	</div>\n    	<!-- page end-->\n</section>\n\n<script src=\"/flatlab/js/form-validation-script.js\"></script>\n<script src=\"/flatlab/js/advanced-form-components.js\"></script>\n<script type=\"text/javascript\" src=\"/flatlab/js/ga.js\"></script>\n\n<script>\n	function OnSelect(radio){\n		var selectedValue = radio.value;\n		document.getElementById(\"isactive\").value = selectedValue;\n	} \n</script>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(staff_clientEntryView)
T_REGISTER_VIEW(staff_clientEntryView)

#include "staff_clientEntryView.moc"
