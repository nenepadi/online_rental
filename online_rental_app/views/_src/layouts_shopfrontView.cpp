#include <QtCore>
#include <TreeFrogView>
#include "category.h" 
#include "client.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT layouts_shopfrontView : public TActionView
{
  Q_OBJECT
public:
  layouts_shopfrontView() : TActionView() { }
  layouts_shopfrontView(const layouts_shopfrontView &) : TActionView() { }
  QString toString();
};

QString layouts_shopfrontView::toString()
{
  responsebody.reserve(7405);
  responsebody += tr("<!DOCTYPE html>\n");
      tfetch(Client, client);
  tfetch(QList<Category>, categoryList);
  responsebody += tr("<html lang=\"en\">\n	<head>\n		<meta charset=\"utf-8\">\n	    	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\n	    	<title>Online Rentals:Shopfront - ");
  responsebody += THttpUtility::htmlEscape(controller()->name());
  responsebody += tr("</title>\n\n	    	<!-- Bootstrap core CSS -->\n	    	<link href=\"/flatlab/css/bootstrap.min.css\" rel=\"stylesheet\">\n	    	<link href=\"/flatlab/css/bootstrap-reset.css\" rel=\"stylesheet\">\n	    	<!--external css-->\n	    	<link href=\"/flatlab/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />\n	    	<link href=\"/flatlab/assets/jquery-ui/jquery-ui-1.10.1.custom.min.css\" rel=\"stylesheet\"/>\n	      <!--right slidebar-->\n	      <link href=\"/flatlab/css/slidebars.css\" rel=\"stylesheet\">\n	    	<!-- Custom styles for this template -->\n	    	<link href=\"/flatlab/css/style.css\" rel=\"stylesheet\">\n	    	<link href=\"/flatlab/css/style-responsive.css\" rel=\"stylesheet\" />\n\n	    	<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->\n	    	<!--[if lt IE 9]>\n	      		<script src=\"/flatlab/js/html5shiv.js\"></script>\n	      		<script src=\"/flatlab/js/respond.min.js\"></script>\n	    	<![endif]-->\n      		<script src=\"//cdnjs.cloudflare.com/ajax/libs/list.js/1.1.1/list.min.js\"></script>\n      		<script src=\"https://raw.githubusercontent.com/javve/list.pagination.js/v0.1.1/dist/list.pagination.min.js\"></script>\n	</head>\n\n  	<body class=\"full-width\" id=\"movies\">\n  		<section id=\"container\" class=\"\">\n      			<!--header start-->\n      			<header class=\"header white-bg\">\n          			<!--logo start-->\n          			<a href=\"\" class=\"logo\" >Online<span>Rentals</span></a>\n          			<!--logo end-->\n          			<!--nav start-->\n          			<div class=\"top-nav \">\n              			<ul class=\"nav pull-right top-menu\">\n	                  			<li><input type=\"text\" class=\"form-control search\" placeholder=\"Search\"></li>\n	                  			<!-- user login dropdown start-->\n	                  			<li class=\"dropdown\">\n	                      			<a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">\n	                          			<img alt=\"\" src=\"/flatlab/img/avatar1_small1.jpg\">\n	                          			<span class=\"username\">");
  responsebody += THttpUtility::htmlEscape(client.username());
  responsebody += tr("</span>\n	                          			<b class=\"caret\"></b>\n	                      			</a>\n	                      			<ul class=\"dropdown-menu\">\n	                          			<div class=\"log-arrow-up\"></div>\n	                          			<li>");
  responsebody += QVariant(linkTo("<i class='fa fa-suitcase'></i> Profile", url("Account", "clientProfile"))).toString();
  responsebody += tr("</li>\n	                          			<li>");
  responsebody += QVariant(linkTo("<i class='fa fa-sign-out'></i> Logout", url("Account", "clientLogout"))).toString();
  responsebody += tr("</li>\n	                      			</ul>\n	                  			</li>\n	                  			<!-- user login dropdown end -->\n	              		</ul>\n          			</div>\n          			<!--nav end-->\n      			</header>\n      			<!--header end-->\n\n      			<!--main content start-->\n      			<section id=\"main-content\">\n          			<section class=\"wrapper\">\n              			<!-- page start-->\n              			<div class=\"row\">\n              				<!--sidebar start-->\n                  				<div class=\"col-md-3\">\n                      				<!--category start-->\n                      				<section class=\"panel\">\n                          				<header class=\"panel-heading\">\n                              					Category\n                          				</header>\n                          				<div class=\"panel-body\">\n                              					<ul class=\"nav prod-cat\">\n                              						");
  if(controller()->activeAction() == "index"){;
  responsebody += tr("                              							<li class=\"active\"><a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("index"));
  responsebody += tr("\"><i class='fa fa-angle-right'></i> All Movies</a></li>\n                              						");
  } else{;
  responsebody += tr("                              							<li><a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("index"));
  responsebody += tr("\"><i class='fa fa-angle-right'></i> All Movies</a></li>\n                              						");
  };
  responsebody += tr("                              							\n                              						");
  foreach(Category c, categoryList){;
  responsebody += tr("                                  						<li>");
  responsebody += QVariant(linkTo("<i class='fa fa-angle-right'></i> " + c.categoryName(), urla("getByCategory", c.categoryName()))).toString();
  responsebody += tr("</li>\n                                  					");
  };
  responsebody += tr("                              					</ul>\n                          				</div>\n                      				</section>\n                      				<!--category end-->\n                  				</div>\n                  				<!--sidebar end-->\n                  				<!--content start-->\n                  				<div class=\"col-md-9\">\n                      				");
  responsebody += QVariant(yield()).toString();
  responsebody += tr("\n                  				</div>\n                  				<!--content end-->\n              			</div>\n              			<!-- page end-->\n          			</section>\n      			</section>\n      			<!--main content end-->\n\n      			<!--footer start-->\n      			<footer class=\"site-footer\">\n          			<div class=\"text-center\">\n              			&copy;2015 Aseda Designs.\n              			<a href=\"#\" class=\"go-top\">\n                  				<i class=\"fa fa-angle-up\"></i>\n              			</a>\n          			</div>\n      			</footer>\n      			<!--footer end-->\n  		</section>\n\n    		<!-- js placed at the end of the document so the pages load faster -->\n    		<script src=\"/flatlab/js/jquery.js\"></script>\n    		<script src=\"/flatlab/js/bootstrap.min.js\"></script>\n    		<script class=\"include\" type=\"text/javascript\" src=\"/flatlab/js/jquery.dcjqaccordion.2.7.js\"></script>\n    		<script src=\"/flatlab/js/jquery.scrollTo.min.js\"></script>\n    		<script src=\"/flatlab/js/jquery.nicescroll.js\" type=\"text/javascript\"></script>\n    		<script src=\"/flatlab/assets/jquery-ui/jquery-ui-1.10.1.custom.min.js\" type=\"text/javascript\"></script>\n    		<script src=\"/flatlab/js/jquery.ui.touch-punch.min.js\"></script>\n    		<script src=\"/flatlab/js/jquery.customSelect.min.js\" ></script>\n    		<script src=\"/flatlab/js/respond.min.js\" ></script>\n  		<!--right slidebar-->\n  		<script src=\"/flatlab/js/slidebars.min.js\"></script>\n    		<!--common script for all pages-->\n    		<script src=\"/flatlab/js/common-scripts.js\"></script>\n\n      		<script type=\"text/javascript\">\n          		$(document).ready(function() {\n              		$(function(){\n                  			$('select').customSelect();\n              		});\n          		});\n      		</script>\n	</body>\n</html>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(layouts_shopfrontView)
T_REGISTER_VIEW(layouts_shopfrontView)

#include "layouts_shopfrontView.moc"
