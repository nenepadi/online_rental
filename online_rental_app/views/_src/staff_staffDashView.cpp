#include <QtCore>
#include <TreeFrogView>
#include "rentalagent.h" 
#include "clientorder.h" 
#include "duesheets.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT staff_staffDashView : public TActionView
{
  Q_OBJECT
public:
  staff_staffDashView() : TActionView() { }
  staff_staffDashView(const staff_staffDashView &) : TActionView() { }
  QString toString();
};

QString staff_staffDashView::toString()
{
  responsebody.reserve(13521);
        tfetch(QList<DueSheets>, dues);
  tfetch(QList<ClientOrder>, ufOrderList);
  tfetch(RentalAgent, agent);
  tfetch(int, clientCount);
  tfetch(int, orderCount);
  tfetch(int, salesCount);
  responsebody += tr("<section class=\"wrapper\">\n	<!--state overview start-->\n      	<div class=\"row state-overview\">\n             <div class=\"col-lg-4 col-sm-6\">\n                   <section class=\"panel\">\n                         	<div class=\"symbol terques\"><i class=\"fa fa-user\"></i></div>\n                         	<div class=\"value\">\n                              	<h1 class=\"count\">0</h1>\n                              	<p>Users</p>\n                              	<p style=\"display: none;\" id=\"clientCount\">");
  responsebody += THttpUtility::htmlEscape(clientCount);
  responsebody += tr("</p>\n                          </div>\n                   	</section>\n            	</div>\n            	<div class=\"col-lg-4 col-sm-6\">\n                  	<section class=\"panel\">\n                         	<div class=\"symbol red\"><i class=\"fa fa-tags\"></i></div>\n                         	<div class=\"value\">\n                         		<h1 class=\" count2\">0</h1>\n                             	<p>Sales</p>\n                             	<p style=\"display: none;\" id=\"salesCount\">");
  responsebody += THttpUtility::htmlEscape(salesCount);
  responsebody += tr("</p>\n                         	</div>\n                   	</section>\n            	</div>\n             <div class=\"col-lg-4 col-sm-6\">\n                  	<section class=\"panel\">\n                         	<div class=\"symbol yellow\"><i class=\"fa fa-shopping-cart\"></i></div>\n                         	<div class=\"value\">\n                              	<h1 class=\" count3\">0</h1>\n                              	<p>Orders</p>\n                              	<p style=\"display: none;\" id=\"orderCount\">");
  responsebody += THttpUtility::htmlEscape(orderCount);
  responsebody += tr("</p>\n                         	</div>\n                   	</section>\n            	</div>\n            	<!-- <div class=\"col-lg-3 col-sm-6\">\n                   	<section class=\"panel\">\n                         	<div class=\"symbol blue\"><i class=\"fa fa-bar-chart-o\"></i></div>\n                         	<div class=\"value\">\n                         		<h1 class=\" count4\">0</h1>\n                         		<p>Total Profit</p>\n                         	</div>\n                  	</section>\n            	</div> -->\n      	</div>\n      	<!--state overview end-->\n\n      	<div class=\"row\">\n      		<div class=\"col-lg-6\">\n      			<!--user info table start-->\n      			<section class=\"panel\">\n      				<div class=\"panel-body\">\n      					<a href=\"#\" class=\"task-thumb\"><img src=\"img/avatar1.jpg\" alt=\"\"></a>\n                              	<div class=\"task-thumb-details\">\n                                	<h1><a href=\"#\">");
  responsebody += THttpUtility::htmlEscape(agent.firstname() + " " + agent.lastname());
  responsebody += tr("</a></h1>\n                                  	<p>Duesheets</p>\n                              	</div>\n                         	</div>\n                         	<table class=\"table personal-task\">\n                         		<tbody>\n                         			");
  foreach(DueSheets ds, dues){;
  responsebody += tr("                         				<tr>\n                         					<td>");
  responsebody += THttpUtility::htmlEscape(ds.orderedItem());
  responsebody += tr("</td>\n                                    			<td>");
  responsebody += THttpUtility::htmlEscape(ds.orderedBy());
  responsebody += tr("</td>\n                                    			<td>\n                                    				");
  if(QDate::fromString(ds.dueDate(), "yyyy-MM-dd") == QDate::currentDate()){;
  responsebody += tr("											");
  responsebody += QVariant(linkTo("<i class='fa fa-cogs'></i>", urla("activateFine", ds.id()), Tf::Post, "", a("class", "btn btn-primary btn-xs disabled"))).toString();
  responsebody += tr("\n									");
  } else{;
  responsebody += tr("										");
  if(QDate::fromString(ds.dayFineLastActivate(), "yyyy-MM-dd") == QDate::currentDate()){;
  responsebody += tr("											");
  responsebody += QVariant(linkTo("<i class='fa fa-cogs'></i>", urla("activateFine", ds.id()), Tf::Post, "", a("class", "btn btn-primary btn-xs disabled"))).toString();
  responsebody += tr("\n										");
  } else{;
  responsebody += tr("											");
  responsebody += QVariant(linkTo("<i class='fa fa-cogs fa-spin'></i>", urla("activateFine", ds.id()), Tf::Post, "", a("class", "btn btn-primary btn-xs"))).toString();
  responsebody += tr("\n													");
  };
  responsebody += tr("										");
  };
  responsebody += tr("									");
  responsebody += QVariant(linkTo("<i class='fa fa-share-square-o'></i>", urla("checkIn", ds.id()), Tf::Post, "", a("class", "btn btn-success btn-xs"))).toString();
  responsebody += tr("\n                                    			</td>\n                                		</tr>\n                                	");
  };
  responsebody += tr("                              	</tbody>\n                         	</table>\n                  	</section>\n                   <!--user info table end-->\n            	</div>\n            	<div class=\"col-lg-6\">\n             	<!--work progress start-->\n                  	<section class=\"panel\">\n                   		<div class=\"panel-body progress-panel\">\n                         		<div class=\"task-progress\">\n                               		<h1>Work Progress</h1>\n                                  	<p>Orders to process</p>\n                              	</div>\n                         	</div>\n                         	<table class=\"table personal-task\">\n                         		<tbody>\n                         			");
  foreach(ClientOrder co, ufOrderList){;
  responsebody += tr("	                              		<tr>\n	                                  		<td>");
  responsebody += THttpUtility::htmlEscape(co.movie());
  responsebody += tr("</td>\n	                                  		<td>");
  responsebody += THttpUtility::htmlEscape(co.user());
  responsebody += tr("</td>\n	                                 		<td>");
  responsebody += QVariant(linkTo("<i class='fa fa-cogs'></i>", urla("orderProcessForm", co.id()), Tf::Get, "", a("class", "btn btn-primary btn-xs"))).toString();
  responsebody += tr("</div></td>\n	                              		</tr>\n	                              	");
  };
  responsebody += tr("                              	</tbody>\n                         	</table>\n                   	</section>\n                  	<!--work progress end-->\n            	</div>\n      	</div>\n\n      	<div class=\"row\">\n      		<div class=\"col-lg-8\">\n             	<!--custom chart start-->\n                   	<div class=\"border-head\"><h3>Earning Graph</h3></div>\n                   	<div class=\"custom-bar-chart\">\n                         	<ul class=\"y-axis\">\n                         		<li><span>100</span></li>\n                         		<li><span>80</span></li>\n                              	<li><span>60</span></li>\n                              	<li><span>40</span></li>\n                              	<li><span>20</span></li>\n                              	<li><span>0</span></li>\n                         	</ul>\n                         	<div class=\"bar\">\n                              	<div class=\"title\">JAN</div>\n                              	<div class=\"value tooltips\" data-original-title=\"80%\" data-toggle=\"tooltip\" data-placement=\"top\">80%</div>\n                         	</div>\n                         	<div class=\"bar \">\n                         		<div class=\"title\">FEB</div>\n                              	<div class=\"value tooltips\" data-original-title=\"50%\" data-toggle=\"tooltip\" data-placement=\"top\">50%</div>\n                         	</div>\n                         	<div class=\"bar \">\n                         		<div class=\"title\">MAR</div>\n                              	<div class=\"value tooltips\" data-original-title=\"40%\" data-toggle=\"tooltip\" data-placement=\"top\">40%</div>\n                         	</div>\n                         	<div class=\"bar \">\n                         		<div class=\"title\">APR</div>\n                              	<div class=\"value tooltips\" data-original-title=\"55%\" data-toggle=\"tooltip\" data-placement=\"top\">55%</div>\n                         	</div>\n                         	<div class=\"bar\">\n                              	<div class=\"title\">MAY</div>\n                              	<div class=\"value tooltips\" data-original-title=\"20%\" data-toggle=\"tooltip\" data-placement=\"top\">20%</div>\n                         	</div>\n                         	<div class=\"bar \">\n                              	<div class=\"title\">JUN</div>\n                              	<div class=\"value tooltips\" data-original-title=\"39%\" data-toggle=\"tooltip\" data-placement=\"top\">39%</div>\n                         	</div>\n                         	<div class=\"bar\">\n                         		<div class=\"title\">JUL</div>\n                              	<div class=\"value tooltips\" data-original-title=\"75%\" data-toggle=\"tooltip\" data-placement=\"top\">75%</div>\n                         	</div>\n                         	<div class=\"bar \">\n                              	<div class=\"title\">AUG</div>\n                              	<div class=\"value tooltips\" data-original-title=\"45%\" data-toggle=\"tooltip\" data-placement=\"top\">45%</div>\n                         	</div>\n                         	<div class=\"bar \">\n                              	<div class=\"title\">SEP</div>\n                              	<div class=\"value tooltips\" data-original-title=\"50%\" data-toggle=\"tooltip\" data-placement=\"top\">50%</div>\n                         	</div>\n                         	<div class=\"bar \">\n                              	<div class=\"title\">OCT</div>\n                              	<div class=\"value tooltips\" data-original-title=\"42%\" data-toggle=\"tooltip\" data-placement=\"top\">42%</div>\n                         	</div>\n                         	<div class=\"bar \">\n                              	<div class=\"title\">NOV</div>\n                              	<div class=\"value tooltips\" data-original-title=\"60%\" data-toggle=\"tooltip\" data-placement=\"top\">60%</div>\n                         	</div>\n                         	<div class=\"bar \">\n                              	<div class=\"title\">DEC</div>\n                              	<div class=\"value tooltips\" data-original-title=\"90%\" data-toggle=\"tooltip\" data-placement=\"top\">90%</div>\n                         	</div>\n                   	</div>\n                  	<!--custom chart end-->\n            	</div>\n            	<div class=\"col-lg-4\">\n                  	<!--new earning start-->\n                  	<div class=\"panel terques-chart\">\n                         	<div class=\"panel-body chart-texture\">\n                          	<div class=\"chart\">\n                                 	<div class=\"heading\">\n                                      		<span>Friday</span>\n                                      		<strong>$ 57,00 | 15%</strong>\n                                  	</div>\n                                  	<div class=\"sparkline\" data-type=\"line\" data-resize=\"true\" data-height=\"75\" data-width=\"90%\" data-line-width=\"1\" data-line-color=\"#fff\" data-spot-color=\"#fff\" data-fill-color=\"\" data-highlight-line-color=\"#fff\" data-spot-radius=\"4\" data-data=\"[200,135,667,333,526,996,564,123,890,564,455]\"></div>\n                              	</div>\n                         	</div>\n                         	<div class=\"chart-tittle\">\n                              	<span class=\"title\">New Earning</span>\n                              	<span class=\"value\">\n                                  	<a href=\"#\" class=\"active\">Market</a>\n                                  	|\n                                  	<a href=\"#\">Referal</a>\n                                  	|\n                                  	<a href=\"#\">Online</a>\n                              	</span>\n                         	</div>\n                  	</div>\n                  	<!--new earning end-->\n\n                  	<!--total earning start-->\n                   	<div class=\"panel green-chart\">\n                         	<div class=\"panel-body\">\n                             	<div class=\"chart\">\n                                  	<div class=\"heading\">\n                                      		<span>June</span>\n                                      		<strong>23 Days | 65%</strong>\n                                  	</div>\n                               		<div id=\"barchart\"></div>\n                              	</div>\n                         	</div>\n                         	<div class=\"chart-tittle\">\n                              	<span class=\"title\">Total Earning</span>\n                              	<span class=\"value\">$, 76,54,678</span>\n                         	</div>\n                  	</div>\n                  	<!--total earning end-->\n            	</div>\n     	</div>\n</section>");

  return responsebody;
}

Q_DECLARE_METATYPE(staff_staffDashView)
T_REGISTER_VIEW(staff_staffDashView)

#include "staff_staffDashView.moc"
