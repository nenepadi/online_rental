#include <QtCore>
#include <TreeFrogView>
#include "applicationhelper.h"

class T_VIEW_EXPORT partial_admin_navigationView : public TActionView
{
  Q_OBJECT
public:
  partial_admin_navigationView() : TActionView() { }
  partial_admin_navigationView(const partial_admin_navigationView &) : TActionView() { }
  QString toString();
};

QString partial_admin_navigationView::toString()
{
  responsebody.reserve(1690);
  responsebody += tr("<aside>\n	<div id=\"sidebar\"  class=\"nav-collapse \">\n		<!-- sidebar menu start-->\n		<ul class=\"sidebar-menu\" id=\"nav-accordion\">\n	  		<li>\n	      		<a href=\"/Admin/index\">\n	          		<i class=\"fa fa-dashboard\"></i>\n	          		<span>Dashboard</span>\n	      		</a>\n	  		</li>\n\n	  		<li class=\"sub-menu\">\n	      		<a href=\"javascript:;\" >\n	          		<i class=\"fa fa-users\"></i>\n	          		<span>Users</span>\n	      		</a>\n	      		<ul class=\"sub\">\n	          		<li>");
  responsebody += QVariant(linkTo("Staff", urla("all_staff"))).toString();
  responsebody += tr("</li>\n	          		<li>");
  responsebody += QVariant(linkTo("Clients", urla("all_clients"))).toString();
  responsebody += tr("</li>\n	      		</ul>\n	  		</li>\n\n	  		<li class=\"sub-menu\">\n	      		<a href=\"javascript:;\" >\n	          		<i class=\"fa fa-film\"></i>\n	          		<span>Products</span>\n	      		</a>\n	      		<ul class=\"sub\">\n	          		<li>");
  responsebody += QVariant(linkTo("Categories", urla("categoryListings"))).toString();
  responsebody += tr("</li>\n	          		<li>");
  responsebody += QVariant(linkTo("Movies", urla("all_items"))).toString();
  responsebody += tr("</li>\n	      		</ul>\n	  		</li>\n\n	  		<li class=\"sub-menu\">\n	      		<a href=\"javascript:;\" >\n	          		<i class=\"fa fa-bar-chart-o\"></i>\n	          		<span>Reports</span>\n	      		</a>\n	      		<ul class=\"sub\">\n	          		<li>");
  responsebody += QVariant(linkTo("All Reports", urla("reports"))).toString();
  responsebody += tr("</li>\n	      		</ul>\n	  		</li>\n		</ul>\n		<!-- sidebar menu end-->\n	</div>\n</aside>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(partial_admin_navigationView)
T_REGISTER_VIEW(partial_admin_navigationView)

#include "partial_admin_navigationView.moc"
