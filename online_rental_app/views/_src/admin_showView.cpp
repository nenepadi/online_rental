#include <QtCore>
#include <TreeFrogView>
#include "item.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT admin_showView : public TActionView
{
  Q_OBJECT
public:
  admin_showView() : TActionView() { }
  admin_showView(const admin_showView &) : TActionView() { }
  QString toString();
};

QString admin_showView::toString()
{
  responsebody.reserve(1503);
  responsebody += tr("<!DOCTYPE html>\n");
    tfetch(Item, item);
  responsebody += tr("<html>\n<head>\n  <meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" />\n  <title>");
  responsebody += THttpUtility::htmlEscape(controller()->name() + ": " + controller()->activeAction());
  responsebody += tr("</title>\n</head>\n<body>\n<p style=\"color: red\">");
  tehex(error);
  responsebody += tr("</p>\n<p style=\"color: green\">");
  tehex(notice);
  responsebody += tr("</p>\n\n<h1>Showing Item</h1>\n<dt>ID</dt><dd>");
  responsebody += THttpUtility::htmlEscape(item.id());
  responsebody += tr("</dd><br />\n<dt>Item Title</dt><dd>");
  responsebody += THttpUtility::htmlEscape(item.itemTitle());
  responsebody += tr("</dd><br />\n<dt>Category</dt><dd>");
  responsebody += THttpUtility::htmlEscape(item.category());
  responsebody += tr("</dd><br />\n<dt>Year of Production</dt><dd>");
  responsebody += THttpUtility::htmlEscape(item.yearOfProduction());
  responsebody += tr("</dd><br />\n<dt>Description</dt><dd>");
  responsebody += THttpUtility::htmlEscape(item.description());
  responsebody += tr("</dd><br />\n<dt>Quantity</dt><dd>");
  responsebody += THttpUtility::htmlEscape(item.quantity());
  responsebody += tr("</dd><br />\n\n");
  responsebody += QVariant(linkTo("Edit", urla("edit_item", item.id()))).toString();
  responsebody += tr(" |\n");
  responsebody += QVariant(linkTo("Back", urla("all_items"))).toString();
  responsebody += tr("\n\n</body>\n</html>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(admin_showView)
T_REGISTER_VIEW(admin_showView)

#include "admin_showView.moc"
