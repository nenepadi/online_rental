#include <QtCore>
#include <TreeFrogView>
#include "client.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT layouts_clientSettingsView : public TActionView
{
  Q_OBJECT
public:
  layouts_clientSettingsView() : TActionView() { }
  layouts_clientSettingsView(const layouts_clientSettingsView &) : TActionView() { }
  QString toString();
};

QString layouts_clientSettingsView::toString()
{
  responsebody.reserve(8120);
  responsebody += tr("<!DOCTYPE html>\n");
    tfetch(Client, loggedUser);
  responsebody += tr("<html lang=\"en\">\n	<head>\n		<meta charset=\"utf-8\">\n		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\n    		<title>Online Rentals:Shopfront - ");
  responsebody += THttpUtility::htmlEscape(controller()->name());
  responsebody += tr("</title>\n\n		<!-- Bootstrap core CSS -->\n	    	<link href=\"/flatlab/css/bootstrap.min.css\" rel=\"stylesheet\">\n	    	<link href=\"/flatlab/css/bootstrap-reset.css\" rel=\"stylesheet\">\n	    	<!--external css-->\n	    	<link href=\"/flatlab/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />\n	    	<!--right slidebar-->\n	    	<link href=\"/flatlab/css/slidebars.css\" rel=\"stylesheet\">\n	    	<!-- Custom styles for this template -->\n	    	<link href=\"/flatlab/css/style.css\" rel=\"stylesheet\">\n	    	<link href=\"/flatlab/css/style-responsive.css\" rel=\"stylesheet\" />\n\n	    	<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->\n	    	<!--[if lt IE 9]>\n	      		<script src=\"/flatlab/js/html5shiv.js\"></script>\n	      		<script src=\"/flatlab/js/respond.min.js\"></script>\n	    	<![endif]-->\n	</head>\n\n  	<body class=\"full-width\">\n  		<section id=\"container\" class=\"\">\n  			<!--header start-->\n      			<header class=\"header white-bg\">\n          			<!--logo start-->\n          			<a href=\"\" class=\"logo\" >Online<span>Rentals</span></a>\n          			<!--logo end-->\n          			<!--nav start-->\n          			<div class=\"top-nav \">\n              			<ul class=\"nav pull-right top-menu\">\n                  				<li>\n                      				<input type=\"text\" class=\"form-control search\" placeholder=\"Search\">\n                  				</li>\n                  				<!-- user login dropdown start-->\n                  				<li class=\"dropdown\">\n                      				<a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">\n                          				<img alt=\"\" src=\"/flatlab/img/avatar1_small1.jpg\">\n                          				<span class=\"username\">");
  responsebody += THttpUtility::htmlEscape(loggedUser.username());
  responsebody += tr("</span>\n                          				<b class=\"caret\"></b>\n                      				</a>\n                      				<ul class=\"dropdown-menu\">\n                          				<div class=\"log-arrow-up\"></div>\n                          				<li>");
  responsebody += QVariant(linkTo("<i class='fa fa-suitcase'></i> Profile", urla("clientProfile"))).toString();
  responsebody += tr("</li>\n                          				<li>");
  responsebody += QVariant(linkTo("<i class='fa fa-key'></i> Logout", urla("clientLogout"))).toString();
  responsebody += tr("</li>\n                      				</ul>\n                  				</li>\n                  				<!-- user login dropdown end -->\n              			</ul>\n          			</div>\n          			<!--nav end-->\n      			</header>\n      			<!--header end-->\n      			\n      			<!--main content start-->\n      			<section id=\"main-content\">\n          			<section class=\"wrapper\">\n              			<!-- page start-->\n              			<div class=\"row\">\n                  				<aside class=\"profile-nav col-lg-3\">\n                      				<section class=\"panel\">\n                          				<div class=\"user-heading round\">\n                              					<a href=\"#\">\n                                  					<img src=\"/flatlab/img/profile-avatar.jpg\" alt=\"\">\n                              					</a>\n                              					<h1>");
  responsebody += THttpUtility::htmlEscape(loggedUser.firstname() + " " + loggedUser.lastname());
  responsebody += tr("</h1>\n                              					<p>");
  responsebody += THttpUtility::htmlEscape(loggedUser.email());
  responsebody += tr("</p>\n                          				</div>\n\n                          				<ul class=\"nav nav-pills nav-stacked\">\n                          					");
  if(controller()->activeAction() == "clientProfile"){;
  responsebody += tr("	                              					<li class=\"active\"><a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("clientProfile"));
  responsebody += tr("\"> <i class=\"fa fa-user\"></i> Profile</a></li>\n	                              					<li><a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("changeProfileForm", loggedUser.clientid()));
  responsebody += tr("\"> <i class=\"fa fa-edit\"></i> Edit profile</a></li>\n	                              					<li><a href=\"");
  responsebody += THttpUtility::htmlEscape(url("Account", "changePasswordForm"));
  responsebody += tr("\"> <i class=\"fa fa-map-marker\"></i> Change Pick Location</a></li>\n	                              				");
  } else if(controller()->activeAction() == "changeProfileForm"){;
  responsebody += tr("	                              					<li><a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("clientProfile"));
  responsebody += tr("\"> <i class=\"fa fa-user\"></i> Profile</a></li>\n	                              					<li class=\"active\"><a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("changeProfileForm", loggedUser.clientid()));
  responsebody += tr("\"> <i class=\"fa fa-edit\"></i> Edit profile</a></li>\n	                              					<li><a href=\"");
  responsebody += THttpUtility::htmlEscape(url("Account", "changePasswordForm"));
  responsebody += tr("\"> <i class=\"fa fa-map-marker\"></i> Change Pick Location</a></li>\n	                              				");
  }else{;
  responsebody += tr("	                              					<li><a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("clientProfile"));
  responsebody += tr("\"> <i class=\"fa fa-user\"></i> Profile</a></li>\n	                              					<li><a href=\"");
  responsebody += THttpUtility::htmlEscape(urla("changeProfileForm", loggedUser.clientid()));
  responsebody += tr("\"> <i class=\"fa fa-edit\"></i> Edit profile</a></li>\n	                              					<li class=\"active\"><a href=\"");
  responsebody += THttpUtility::htmlEscape(url("Account", "changePasswordForm"));
  responsebody += tr("\"> <i class=\"fa fa-map-marker\"></i> Change Pick Location</a></li>\n	                              				");
  };
  responsebody += tr("                          				</ul>\n							</section>\n                  				</aside>\n                  				<aside class=\"profile-info col-lg-9\">\n                      				");
  responsebody += QVariant(yield()).toString();
  responsebody += tr("\n                  				</aside>\n              			</div>\n              			<!-- page end-->\n          			</section>\n      			</section>\n      			<!--main content end-->\n\n      			<!--footer start-->\n      			<footer class=\"site-footer\">\n          			<div class=\"text-center\">\n              			&copy;2015 Aseda Designs.\n              			<a href=\"#\" class=\"go-top\">\n                  				<i class=\"fa fa-angle-up\"></i>\n              			</a>\n          			</div>\n      			</footer>\n      			<!--footer end-->\n  		</section>\n\n    		<!-- js placed at the end of the document so the pages load faster -->\n    		<script src=\"/flatlab/js/jquery.js\"></script>\n	    	<script src=\"/flatlab/js/bootstrap.min.js\"></script>\n	    	<script class=\"include\" type=\"text/javascript\" src=\"/flatlab/js/jquery.dcjqaccordion.2.7.js\"></script>\n	    	<script src=\"/flatlab/js/jquery.scrollTo.min.js\"></script>\n	    	<script src=\"/flatlab/js/jquery.nicescroll.js\" type=\"text/javascript\"></script>\n	    	<script src=\"/flatlab/assets/jquery-knob/js/jquery.knob.js\"></script>\n	    	<script src=\"/flatlab/js/respond.min.js\" ></script>\n\n	  	<!--right slidebar-->\n	  	<script src=\"/flatlab/js/slidebars.min.js\"></script>\n\n	    	<!--common script for all pages-->\n	    	<script src=\"/flatlab/js/common-scripts.js\"></script>\n\n	  	<script>\n	      		//knob\n	      		$(\".knob\").knob();\n	  	</script>\n  	</body>\n</html>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(layouts_clientSettingsView)
T_REGISTER_VIEW(layouts_clientSettingsView)

#include "layouts_clientSettingsView.moc"
