#include <QtCore>
#include <TreeFrogView>
#include "rentalagent.h" 
#include "QDate" 
#include "applicationhelper.h"

class T_VIEW_EXPORT partial_staff_navigationView : public TActionView
{
  Q_OBJECT
public:
  partial_staff_navigationView() : TActionView() { }
  partial_staff_navigationView(const partial_staff_navigationView &) : TActionView() { }
  QString toString();
};

QString partial_staff_navigationView::toString()
{
  responsebody.reserve(1597);
      tfetch(RentalAgent, agent);
  responsebody += tr("<aside>\n	<div id=\"sidebar\"  class=\"nav-collapse \">\n		<!-- sidebar menu start-->\n		<ul class=\"sidebar-menu\" id=\"nav-accordion\">\n	  		<li>\n	      		<a href=\"/Staff/staff_dash\">\n	          		<i class=\"fa fa-dashboard\"></i>\n	          		<span>Dashboard</span>\n	      		</a>\n	  		</li>\n\n	  		<li class=\"sub-menu\">\n	      		<a href=\"javascript:;\" >\n	          		<i class=\"fa fa-users\"></i>\n	          		<span>Users</span>\n	      		</a>\n	      		<ul class=\"sub\">\n	          		<li><a href=\"/Staff/client_listings/");
  responsebody += THttpUtility::htmlEscape(agent.location());
  responsebody += tr("\">Clients</a></li>\n	      		</ul>\n	  		</li>\n\n	  		<li class=\"sub-menu\">\n	      		<a href=\"javascript:;\" >\n	          		<i class=\"fa fa-credit-card\"></i>\n	          		<span>Transactions</span>\n	      		</a>\n	      		<ul class=\"sub\">\n	          		<li><a  href=\"/Staff/orderListings/");
  responsebody += THttpUtility::htmlEscape(agent.location());
  responsebody += tr("\">Orders</a></li>\n	          		<li>");
  responsebody += QVariant(linkTo("Due sheets", urla("viewDueSheet"))).toString();
  responsebody += tr("</li>\n	      		</ul>\n	  		</li>\n\n	  		<li class=\"sub-menu\">\n	      		<a href=\"javascript:;\" >\n	          		<i class=\"fa fa-gift\"></i>\n	          		<span>Miscellaneous</span>\n	      		</a>\n	      		<ul class=\"sub\">\n	          		<li><a  href=\"#\">Compose mail</a></li>\n	      		</ul>\n	  		</li>\n		</ul>\n		<!-- sidebar menu end-->\n	</div>\n</aside>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(partial_staff_navigationView)
T_REGISTER_VIEW(partial_staff_navigationView)

#include "partial_staff_navigationView.moc"
