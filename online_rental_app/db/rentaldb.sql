CREATE TABLE rental_agent(
	agentid INTEGER PRIMARY KEY AUTOINCREMENT,
	firstname VARCHAR(50) NOT NULL,
	lastname VARCHAR(70) NOT NULL,
	email VARCHAR(70) NOT NULL UNIQUE,
	password VARCHAR(128) NOT NULL DEFAULT "staff2014",
	location VARCHAR(255) NOT NULL,
	role VARCHAR(20) NOT NULL DEFAULT "staff",
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	lock_revision INTEGER
);

CREATE TABLE item(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	item_title VARCHAR(255) NOT NULL,
	category VARCHAR(255) NOT NULL,
	year_of_production DATE NOT NULL,
	description TEXT NOT NULL,
	quantity INTEGER NOT NULL DEFAULT 1, 
	lead_actor VARCHAR NULL,
	lead_actress VARCHAR NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	lock_revision INTEGER
);

CREATE TABLE client(
	clientid INTEGER PRIMARY KEY AUTOINCREMENT,
	firstname VARCHAR(50) NOT NULL,
	lastname VARCHAR(70) NOT NULL,
	username VARCHAR(50) NOT NULL UNIQUE,
	email VARCHAR(70) NOT NULL UNIQUE,
	password VARCHAR(128) NOT NULL,
	pick_location VARCHAR(250) NOT NULL,
	is_active BOOL NOT NULL,
	entitled_number_movies INTEGER NOT NULL DEFAULT 0,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	lock_revision INTEGER
);

CREATE TABLE client_order(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	movie VARCHAR(255) NOT NULL,
	user VARCHAR(255) NOT NULL,
	location VARCHAR(255),
	fulfilled BOOL NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	lock_revision INTEGER
);

CREATE TABLE category(
	cat_id INTEGER PRIMARY KEY AUTOINCREMENT,
	category_name VARCHAR(255) NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	lock_revision INTEGER
);

CREATE TABLE due_sheets(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	ordered_item VARCHAR(255) NOT NULL,
	ordered_by VARCHAR(255) NOT NULL,
	processed_date DATE NOT NULL,
	due_date DATE NOT NULL,
	check_in BOOL NOT NULL,
	processor VARCHAR(255) NOT NULL,
	day_fine_last_activate DATE,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	lock_revision INTEGER
);

CREATE TABLE possible_locations(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	location_name VARCHAR,
	latitude REAL,
	longitude REAL
);

INSERT INTO rental_agent(firstname, lastname, email, password, location, role, lock_revision) 
VALUES("Ernestina", "Mensah", "estymens@admin.com", "estymens12", "Bolzano", "administrator", 1);

INSERT INTO rental_agent(firstname, lastname, email, password, location, role, lock_revision) 
VALUES("Obed", "Ademang", "oby@staff.com", "staff2014", "Saltau", "staff", 1);